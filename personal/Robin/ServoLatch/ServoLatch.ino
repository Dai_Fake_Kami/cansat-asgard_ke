#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "ServoControl.h"

ServoLatch myServo;

void setup() {
  Serial.begin (115200);
  myServo.begin (9);
}

void loop() {
  myServo.unlock();
  delay(3000);
  myServo.lock();
  delay(3000);
}
