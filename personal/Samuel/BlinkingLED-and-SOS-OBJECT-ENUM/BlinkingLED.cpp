#include "blinkingLED.h"


BlinkingLED::BlinkingLED(byte thePinNumber, uint16_t theDuration) {   //Declares a cosntructor for class BlinkingLed with parameters fo the pin number the blink will be ran on
  duration = theDuration;                                             //Transfers the value of public "theDuration" to the private "duration"
  pinNumber = thePinNumber;                                           //Transfers the value of public "thePinNumber" to the private "pinNumer"
  pinMode(pinNumber, OUTPUT);                                         //Sets the pinMode of pinNumber to OUTPUT
  ts = millis();                                                      //Saves the current value of millis() in the timestamp variable
}

void BlinkingLED::run() {                                             //Defines the method run()
  Serial.println("DURATION");                                         //DEBUG: Writes duration
  Serial.println(duration);                                           //DEBUG: Writes the value of duration
  Serial.println("TS");                                               //DEBUG: Wirtes TS
  Serial.println(ts);                                                 //Writes the value of time stamp (ts)
  if ((millis() - ts) >= duration) {                                  //Checks if the user inputted time in duration has elapsed
    digitalWrite(pinNumber, !digitalRead(pinNumber));                 //Writes the oposite state of pinNumber to pinNumber  
    ts = millis();                                                    //Saves the current value of millis() in the Time Stamp
  }
}
