#include "BlinkLED.h"
bool BlinkLED::begin( const uint8_t TheLedPinNbr, uint16_t APeriodInMsec) {
  pinMode(TheLedPinNbr , OUTPUT);
  lastChangeTimestamp = 0;
  return true ;
}




void BlinkLED::run() {
  unsigned long now = millis();
  if ((now - lastChangeTimestamp) > periodInMsec) {
    if (digitalRead(ledPinNbr) == HIGH) {
      digitalWrite(ledPinNbr , LOW);
    }
    else {
      digitalWrite (ledPinNbr , HIGH);

    }
    lastChangeTimestamp = now;
  }
}
