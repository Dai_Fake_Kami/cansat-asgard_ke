// --- Commande d'un StepStick/Driver A4988 ----------------------
// DRV8834_Test.ino
//
// Commande d'un moteur pas-à-pas à l'aide d'un pilote DRV8834 avec
//     Arduino.
//

/* Connexions: nSLEEP à VCC
    step et dir comme ci-dessous
    enable est optionnel
*/

#define pinSleep 9 // Activation du driver/pilote avec nSLEEP
#define pinStep   6 // Signal de PAS (avancement)
#define pinDir    12 // Direction 

#include "CSPU_Debug.h"

#define STEPS_PER_REVOLUTION 20
#define MICRO_STEPS     4   // 4 microsteps per step
#define STEPS_PER_TURN  STEPS_PER_REVOLUTION*MICRO_STEPS
#define CM              (int) (23.57*STEPS_PER_TURN)

const uint16_t DurationHigh = 1; // Duration for holding step pin HIGH, in ms
const uint16_t DurationLow = 3; // Duration for holding step pin LOW, in ms

void setup() {
  DINIT(115200)
  Serial.println("Test DRV8834");

  pinMode( pinSleep, OUTPUT );
  pinMode( pinDir   , OUTPUT );
  pinMode( pinStep  , OUTPUT );
  Serial << "Pins: sleep=" << pinSleep << ", dir=" << pinDir << ", step=" << pinStep << ENDL;
  /*Serial << "1 revolution, dir = HIGH" << ENDL;
    oneRevolution(HIGH);
    delay(5000);
    Serial << "1 revolution, dir = LOW" << ENDL;
    oneRevolution(LOW);
  */delay(5000);
  moveCm(3);
  delay(5000);
  moveCm(-3);
  delay(5000);
  /* moveCm(2);
    delay(5000);
    moveCm(-2);
  */Serial.println("End of job");
}

/* Move numCm centimers. Positive is toward motor */
void moveCm(int numCm) {
  uint16_t NumSteps = abs(numCm) * CM;
  digitalWrite( pinDir   , numCm >= 0 ? HIGH : LOW); // Direction avant
  digitalWrite( pinStep  , LOW);  // Initialisation de la broche step
  Serial << "  Moving " << numCm << " cm, i.e. " << NumSteps << " steps..." << ENDL;
  digitalWrite( pinSleep, HIGH);
  delay(100);
  for (int i = 1; i <= NumSteps ; i++) {
    digitalWrite( pinStep, HIGH );
    delay( DurationHigh ); //  1-2 us devrait suffire
    digitalWrite( pinStep, LOW );
    delay( DurationLow );
  }
  digitalWrite( pinSleep, LOW);
}

void oneRevolution(bool direction) {
  digitalWrite( pinDir   , direction ? HIGH : LOW); // Direction avant
  digitalWrite( pinStep  , LOW);  // Initialisation de la broche step
  Serial << "  Triggering " << STEPS_PER_TURN << " steps..." << ENDL;
  digitalWrite( pinSleep, HIGH);
  delay(100);
  for (int i = 1; i <= STEPS_PER_TURN ; i++) {
    Serial.println( i );
    digitalWrite( pinStep, HIGH );
    delay( DurationHigh ); //  1-2 us devrait suffire
    digitalWrite( pinStep, LOW );
    delay( DurationLow );
  }
  digitalWrite( pinSleep, LOW);
}

void loop() {

  /*
    int i = 0;

    digitalWrite( pinDir   , HIGH); // Direction avant
    digitalWrite( pinStep  , LOW);  // Initialisation de la broche step

    // Avance de 200 pas
    for( i=0; i<200; i++){
    Serial.println( i );
    digitalWrite( pinStep, HIGH );
    delay( 10 ); // 10 ms est énorme 1-2 us devrait suffire
    digitalWrite( pinStep, LOW );
    delay( 10 );
    }

    // Changer de direction
    digitalWrite( pinDir   , LOW); // Direction avant

    // Refaire 200 pas dans l'autre sens
    for( i=0; i<200; i++){
    Serial.println( i );
    digitalWrite( pinStep, HIGH );
    delay( 1 );
    digitalWrite( pinStep, LOW );
    delay( 1 );
    }

    // Pas de step et pas d'ordre...
    //   l'axe du moteur est donc bloqué
    Serial.println("Axe bloqué + attendre 5 sec");
    delay( 5000 );

    // déblocage de l'axe moteur
    Serial.println("Deblocage axe");
    digitalWrite( pinEnable, HIGH ); // logique inversée

    // Fin et blocage du programme
    // Presser reset pour recommencer
    Serial.println("Fin de programme");
    while( true );
  */
}
