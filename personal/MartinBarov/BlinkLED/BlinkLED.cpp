
#include "BlinkLED.h"

bool BlinkLED::begin(const uint8_t theLED_PinNBR, unsigned long APeriodInMSec) {
  pinMode(theLED_PinNBR, OUTPUT);
  LED_PinNBR = theLED_PinNBR;
  periodInMsec = APeriodInMSec ;
  lastChangeTimestamp = 0;
  return true;
}

void BlinkLED::run() {
  unsigned long currentTime = millis();
  if ((currentTime - lastChangeTimestamp) > periodInMsec) {
    if (digitalRead(LED_PinNBR) == HIGH) {
      digitalWrite(LED_PinNBR, LOW);
    }
    else {
      digitalWrite(LED_PinNBR, HIGH);
    }
    lastChangeTimestamp = currentTime;
  }
}
