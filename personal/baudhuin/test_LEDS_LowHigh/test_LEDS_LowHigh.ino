/*
    Test whether LED are pulled up or down.

*/
#include "DebugCSPU.h"

void setup() {
  DINIT(115200);
  Serial << "Serial OK" << ENDL;
  pinMode(13, OUTPUT);
  pinMode(8, OUTPUT);
  while (1) {
    Serial << "writing HIGH" << ENDL;
    digitalWrite(13, HIGH);
    digitalWrite(8, HIGH);
    delay(3000);
    Serial << "writing LOW" << ENDL;
    digitalWrite(13, LOW);
    digitalWrite(8, LOW);
    delay(3000);
  }
  // put your setup code here, to run once:

}

void loop() {
  // put your main code here, to run repeatedly:

}
