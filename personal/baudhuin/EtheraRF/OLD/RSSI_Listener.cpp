/*
 * RSSI_Listener.cpp
 */

#include "RSSI_Listener.h"
#include "DebugCSPU.h"

#define DBG_GET_RSSI 1

bool RSSI_Listener::begin(RH_RF69 &rf69, float frequency, bool useEncryption, const unsigned char* key,
		uint16_t timeoutInMsec, uint8_t beaconID) {
	this->beaconID = beaconID;
	this->timeout = timeoutInMsec;
	this->frequency = frequency;
	this->useEncryption = useEncryption;
	if (useEncryption) {
	  memcpy(this->key, key, BeaconEncryptionKeySize); 
	}
	this->rf69 = &rf69;
  this->status = ListenerStatus::ReadyForRSSI;
	return  true;
}

bool RSSI_Listener::mesureRSSI0(float x0, uint16_t timeoutInMsec) {
	float rssi;
	if (getRSSI(rssi, timeoutInMsec)) {
		this->rssi0 = rssi;
		this->x0=x0;
		status = ListenerStatus::ReadyForDistance;
		return true;
	}
	else return false;
}

bool RSSI_Listener::getRSSI(float &rssi, uint16_t timeoutInMsec) {
	if (!checkStatus(ListenerStatus::ReadyForRSSI)) return false; // begin was not called.
	uint16_t actualTimeout = (timeoutInMsec ? timeoutInMsec : timeout);

	if (!rf69->setFrequency(frequency)) {
		 DPRINTS(DBG_GET_RSSI,"Cannot set frequency to ");
		 DPRINTLN(DBG_GET_RSSI, frequency);
		return false;
	} else {
		DPRINTS(DBG_GET_RSSI,"Frequency set to ");
		DPRINTLN(DBG_GET_RSSI, frequency);
	}

	if (useEncryption == true) {
	    rf69->setEncryptionKey(key);
	    DPRINTSLN(DBG_GET_RSSI,"Encryption key set.");
	} else {
		rf69->setEncryptionKey(nullptr);
		DPRINTSLN(DBG_GET_RSSI,"Encryption disabled");
	}

	// Now wait for a message
	uint8_t buf[RH_RF69_MAX_MESSAGE_LEN];
	uint8_t len = sizeof(buf);

	if (rf69->waitAvailableTimeout(actualTimeout))  {
	    if (rf69->recv(buf, &len)) {
	    	DPRINTS(DBG_GET_RSSI, "Got a msg: ");
	    	DPRINTLN(DBG_GET_RSSI, (char*) buf);
	    	if (beaconID != -1) {
	    		Serial << "** Check origin beacon (id=" << beaconID << " to be implemented" << ENDL;
	    	}
	    	rssi=rf69->lastRssi();
	    	DPRINTS(DBG_GET_RSSI, "Got RSSI: ");
	    	DPRINTLN(DBG_GET_RSSI, rssi);
	    	return true;
	    } else {
	      return false;
	    }
	  } else {
		  DPRINTSLN(DBG_GET_RSSI,"No message received");
		  return false;
	  }
}

bool RSSI_Listener::measureDistance(float &rssi, float &distance) {
	if (getRSSI(rssi)) {
		return convertRSSI_ToDistance(rssi, distance);
	} else return false;

}

bool RSSI_Listener::convertRSSI_ToDistance(float rssi, float &distance) {
	if (!checkStatus(ListenerStatus::ReadyForDistance)) return false; // rssi0 / x0 not available.
	Serial << "rssi to distance conversion to be implemented " << ENDL;
	return false;
}

bool RSSI_Listener::checkStatus(ListenerStatus minStatus) {
	if (status < minStatus) {
		DPRINTS(DBG_DIAGNOSTIC, "RSSI_Listener status KO: expected=");
		DPRINT(DBG_DIAGNOSTIC, (int) minStatus);
		DPRINTS(DBG_DIAGNOSTIC, ", actual= ");
		DPRINTLN(DBG_DIAGNOSTIC, (int) status);
		return false;
	} else return true;
}
