
#include "SD_Logger.h"
#include "EtheraConfig.h"
#include "RH_RF69.h"

constexpr float frequency = 434.0;

RH_RF69 rf69(RFM69_ChipSelectPin,RFM69_InterruptPin);
SD_Logger logger;

void setup()
{
  Serial.begin(115200);
  while (!Serial) {
    delay(1);  // wait until serial console is open, remove if not tethered to computer
  }

  // Initialize the radio module
  pinMode(RFM69_ResetPin, OUTPUT);
  digitalWrite(RFM69_ResetPin, LOW);
  // manual reset
  digitalWrite(RFM69_ResetPin, HIGH);
  delay(10);
  digitalWrite(RFM69_ResetPin, LOW);
  delay(10);

  if (!rf69.init()) {
    Serial.println("*** RFM69 radio init failed.");
    exit(-1);
  }

  if (!rf69.setFrequency(frequency)) {
    Serial.println("Cannot set frequency");
    exit(-1);
  } else {
    Serial.print("Frequency set to ");
    Serial.print(frequency);
    Serial.println(" MHz");
  }

  rf69.setEncryptionKey(nullptr);
  Serial.println("Encryption disabled");
  Serial.println("RFM69 radio init OK.");

  DPRINTSLN(DBG_DIAGNOSTIC, "Initialising SD logger...");
  logger.setChipSelect( SD_CardChipSelect);
  if (!logger.init("First line for test")) {
    Serial.println("** Error intiailising.loger");
  } else {
    Serial.println("SD_Logger initialized.");
  }
  Serial.println("Setup over.");
}

void readOneMsg() {
  uint8_t buf[RH_RF69_MAX_MESSAGE_LEN + 1];
  uint8_t len = sizeof(buf);
  if (rf69.waitAvailableTimeout(CansatAcquisitionPeriod / 2))  {
    if (rf69.recv(buf, &len)) {
      buf[len] = 0;
      Serial.print("Got a msg: ");
      Serial.println((char*) buf);
      Serial.print("RSSI: ");
      Serial.println(rf69.lastRssi());
    }
  } else {
    Serial.println("No message");
  }
}

void storeOneString() {
logger.log("this is string sent to the SD card");
Serial.println("written to SD card");
}

void loop() {
  readOneMsg();
  storeOneString();
  delay(CansatAcquisitionPeriod);
}
