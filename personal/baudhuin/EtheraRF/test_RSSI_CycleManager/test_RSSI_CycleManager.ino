/**
 *  A program just running the RSSI Cycle Manager with the period configured in the CansatConfig.h.
 *  It runs only the RF secondary mission of Ethera and displays the result on the Serial interface. 
 */
#include "RSSI_CycleManager.h"
#include "elapsedMillis.h"
#include "EtheraRecord.h"


constexpr bool DisplayAllRecords=true;  // If true ALL records are displayed, if false, only the record with an 
                                        // an actual RSSI measurement are displayed. 

RSSI_CycleManager cycleManager;
EtheraRecord record;
elapsedMillis elapsed;

void setup() {
  DINIT(115200);
  Serial << "Test of RSSI_CycleManager with period=" << CansatAcquisitionPeriod << " msec." << ENDL;
  
  if (!cycleManager.begin()) {
    Serial << "*** Error initializing the RSSI_CycleManager" << ENDL;
    exit(-1);
  }
  record.clear();
  Serial << "Initialisation OK." << ENDL;
}

void loop() {
  if (elapsed > CansatAcquisitionPeriod) {
    elapsed=0;
    record.clear();
    cycleManager.run(record);
    if (DisplayAllRecords || (record.hasRSSI_Data())) {
      Serial << "Freq=" << record.getFrequency() << " MHz, RSSI=" << record.getRSSI() 
             << " dBm, distance=" << record.distance << "m" << ENDL;
    }
  }
}
