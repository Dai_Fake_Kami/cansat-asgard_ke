/*
  Program to send a SOS (Version 4)
*/

#include "BlinkingLed.h"
#include "MorseSOS.h"

constexpr unsigned long SOS_Period = 5000; // msec
constexpr byte LedSOS = LED_BUILTIN;
constexpr byte BlinkLed = 8;
constexpr unsigned long LedPeriod=600; // msec

BlinkingLed myBlinkingLed(BlinkLed,LedPeriod);
//MorseSOS myBlinkingLed(BlinkLed, SOS_Period+1000);
MorseSOS mySOS(LedSOS, SOS_Period);

void setup()
{
  Serial.begin(19200);
  while (!Serial) ;
  pinMode(LedSOS, OUTPUT);
  Serial.println("Setup ok.");
}

void loop()
{
  mySOS.run();
  myBlinkingLed.run();
}
