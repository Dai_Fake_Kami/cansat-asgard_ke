/*
 * CansatRecordTestFlow.h
 *
 */

#pragma once
#include "CansatRecord.h"
#include <SdFat.h>

/** @ingroup CSPU_CansatData
 *  This is a utility class for use during testing only: it provides a flow of
 *  CansatRecords filled with data found in a data file on the SD card.  It is used to simulated
 *  a flow of record obtained during a flight.
 *  @usage
 *  @code
 *  	CansatRecordTestFlow myFlow();
 *  	CansatRecord record;
 *
 *  	if (!myflow.openInputFile("myfile.txt")) {
 *  		...handle error...
 *  	} else {
 *  		while (getRecord(record)) {
 *  			... make use of record ...
 *  		}
 *  	}
 *  @endcode
 *  - The input file is assumed to contain comma-separated values (one line per record).
 *    Columns must be in the same order as in the CSV output produced by the CansatRecord.
 *    Lines starting with character '#' are ignored.
 *  - It is assumed that this class is the only user of the SD-Card, and that it is
 *    instantiated only once (although this is not enforced).
 *  - This class can be used with subclasses of CansatRecord. If secondary mission
 *    data must be populated in the record, implement the populateSecondaryMissionData() method.
 *  - The data file is expected to contain at least all possible columns for the
 *    CansatRecord columns (including GPS velocity, descente velocity and reference altitude)
 *    Those columns are skipped according to the value of symbols INCLUDE_GPS_VELOCITY,
 *    INCLUDE_DESCENT_VELOCITY and INCLUDE_REF_ALTITUDE (defined in CansatConfig.h)
 */
class CansatRecordTestFlow {
public:
	/** Constructor
	 * @param theBufferSize Number of characters in the internal line buffer. It should
	 *        be more than the longest line in the input file.
	 */
	CansatRecordTestFlow(uint16_t theBufferSize=256);
	virtual ~CansatRecordTestFlow();
	/** Initialize the flow from a particular data file.
	 *  @param fileName The name of the input file, expected to be at the root level
	 *  			    of the SD-Card.
	 *  @param SD_ChipSelect The chip-select pin number for the SD-Card reader
	 *  				     (default value is the internal SD-card reader of
	 *  				      Feather M0 Adalogger)
	 *  @return true if initialisation successful, false otherwise.
	 */
	bool openInputFile(const char* fileName,const byte SD_ChipSelect=4);

	/** Populate a record with the next data found in the file.
	 *  @param record The record to populate. Existing content is erased.
	 *  @return true if a record was read from file,
	 *  false otherwise (file error or end-of-file reached).
	 */
	bool getRecord(CansatRecord& record);

	/** Close open input file, if any. */
	void closeInputFile();

protected:
	/** Populate the secondary mission part of the record. This method should be overridden in a
	 *  subclass. It is is called for each record after the CansatRecord fields have been
	 *  successfully populated. It should
	 *  1. cast the record to the subclass of CansatRecord provided to getRecord(),
	 *  2. call strtok(nullptr, ",") to retrieve the value from each value, convert it as required
	 *     (using atoi, atof etc.) and store the result in the record.
	 *  3. define the return value.
	 *  See class CansatRecordExampleTestFlow for an example.
	 *  @param record The record to populate. Primary mission and GPS data should not be modified.
 *  @param lineBuffer The buffer containing the line currenly parsed.
	 *  @param ptr A point to a character within the buffer to use when calling strtok to obtain
	 *  		   the first secondary mission token.
	 *  @return true if there is a next record to fetch after this one,
	 *  false otherwise.
	 */
	virtual bool populateSecondaryMissionData(CansatRecord& /* record */, char* /* ptr */) {return true;};

	/** Process the content of the buffer to populate the record
	 *  @param record The record to populate.
	 *  @return true if the buffer was successfully analyzed, false otherwise.
	 */
	bool parseBufferIntoRecord(CansatRecord& record);
private:
	SdFat mySD;
    File	dataFile; /** The currently open file */
    char*  buffer;	  /** The buffer for the current line */
    uint16_t bufferSize; /** Size of buffer (in characters) */
    uint32_t currentLine; /** The line currently being read in the file */
};

