/*
    CansatInterface.h
    This file contains definitions shared by the Ground and the Space segments.
*/

#pragma once

/** @ingroup CSPU_CansatConfig

    Identifiers to define the source of a CansatRecord, in project where
    Records can be sent from several sources.
*/
enum class CansatCanID {
  Undefined = 0xFF,
  // G-Mini values
  MainCan = 0,              /**< Value denoting the main can */
  SubCan1 = 1,              /**< Value denoting the first subcan */
  SubCan2 = 2,              /**< Value denoting the second subcan */
  SubCan3 = 3,              /**< Value denoting the third subcan */
  SubCan4 = 4               /**< Value denoting the fourth subcan */
};

/** State of the secondary mission controller. Coded as uint8_t in record
 */
enum class FlightControlCode {
	NoData=0,			 /**< Controller not active */
	StartingUp=1,
	GroundConditions=2,  /**< Altitude lower than minimum: ground conditions
							 assumed (before take-off or after landing) */
	NotDescending=3,		 /** Can is either ascending, not yet ejected or too
							 recently ejected for the descent phase to be in progress */

	NominalDescent=4,	 /**< Can is descending with velocity in nominal range:  this
							  is the situation where the various flight phases
							  are managed. */
	LowDescentVelocityAlert=10,	/**< Can is descending with too low velocity:
									 correction to be made to increase velocity */
	HighDescentVelocityAlert=11 /**< Can is descending with escessive velocity:
									 correction to be made to decrease velocity */
};

/** @ingroup CSPU_CansatConfig

   Identifiers to define the type of content of a frame transferred between
   the can and the ground.
    - In API mode, this value is byte 0 in any frame and the command string
      starts at byte 3.
    - In transparent mode, this is value is the first CSV field.
*/
enum class CansatFrameType {
  // DO NOT USE VALUE 0 (value returned when string to int conversion fails).
  CmdRequest = 1,              /**< Value denoting a command request  */
  CmdResponse = 2,             /**< Value denoting a command response   */
  StatusMsg = 3,               /**< Value denoting an unsollicitated status
                                    message from the can (byte 0 = frame type,
                                    byte 1 = optional sequence number,
                                    string from byte 2. */
  StringPart = 4,        /**< Value denoting a part of a string. The receiver
                          should not include any end-of-line between string
                          parts (byte 0 = frame type, byte 1 = optional
                          sequence number, string from byte 2.*/
  DataRecord = 5,              /**< Value denoting a measurement record */
  GroundDataRecord = 100       /**< Value denoting a ground record */
};

/**  @ingroup CSPU_CansatAsgard
      Constants used to identify command requests sent to the can (applicable
      when CansatFrameType is CmdRequest.
      - In API mode, this is the first CSV value of the command string.
      - In transparent mode, this is the second CSV value of the command string
        (first is the frame type, see above).
      The possible parameters for each request are documented with each value.
      numerical values 0 to 99 are reserved for generic commands.
      project-specific command requests should be in range 100-255
*/
enum class CansatCmdRequestType {
  InvalidRequestType = 0,	/**< value returned when string to int conversion fails. */
  // --- Cross-project command requests
  InitiateCmdMode = 10, 	/**< Request switch to command mode.
								 Parameters: none. */
  TerminateCmdMode = 11,  /**< Request switch to acquisition mode.
								 Parameters: none.*/
  DigitalWrite = 13, 		/**< Request a write on digital pin.
								 Parameters: pinNumber, state (0=LOW, 1=HIGH)*/
  ListFiles = 14, 		/**< Request a list of files present on the SD Card
	 	 	 	 	 	 	 	 Parameters: directory to list,
	 	 	 	 	 	 	 	 (optional) start seq. number (start list from
	 	 	 	 	 	 	 	 n-th file. First file is 1),
	 	 	 	 	 	 	 	 (optional)number of files to list (defaults to 1000) */
  GetFile = 15, 			/**< Request the transfer of a particular file
	 	 	 	 	 	 	 	 Parameters: file name (mandatory. not leading or trailing blanks!)),
	 	 	 	 	 	 	 	 start byte (optional, default to 0),
	 	 	 	 	 	 	 	 numBytes (optional, defaults to 1000). */
  GetCampaignStatus = 16,	/**< Request the status of the measurement campaign
								 (started or not). */
  StartBuzzer = 17, 		/**< Request the operation of the buzzer to locate
								 the can.
	 	 	 	 	 	 	 	 Parameter: duration in seconds */
  StartCampaign = 18, 	/**< Request the immediate start of the measurement
								 campaign */
  StopCampaign = 19, 		/**< Request the immediate stop of the measurement
								 campaign */
  PrepareShutdown = 20,	/**< Request the can to prepare for shutdown: this
								 command orders the can to take any action require
								 before the power is shutdown, e.g. move mechanical
								 parts in a standard storage position. It does not
								 stop the data acquisition/storage/transmission (use
								 Start/StopCampaign to control it). It could include
								 powering down some power consuming equipment, provided
								 it does not prevent sensor data acquisition.
								 Sending this command more than once should not
								 have any additional effect.
								 Parameters: none. */
  CancelShutdown = 21, 	/**< Cancel a previous PrepareShutdown command, in order
								 to resume normal operation of the can without powering
								 down, then up. If no previous PrepareShutdown command
								 was sent, this command should have no effect.
								 Parameters: none. */
  SetParameterValue = 22,		/**< Set a parameter stored on the SD card.
								 Parameters: parameter name, parameter value */
  GetParameterValue = 23,		/**< Get the value of a parameter stored on the SD card.
								 Parameters: parameter name, parameter type (0 for integer, 1 for float) */
  // --- Project-specific commands
  // Torus project commands.
  GetWinchPosition = 100,	/**< Request the current position of the Winch.
	 	 	 	 	 	 	 	 No parameter */
  SetWinchPosition = 101,    /**< Set new Winch position. Parameter: length of
	 	 	 	 	 	 	 	 rope unrolled (in mm) */
  ResetWinchPosition = 102,	/**< Reset winch position to the initial setting.
								No parameter */
  // GMini project commands.
  Latch_Lock = 200, 		/**< lock the subcans latch, No parameter*/
  Latch_Unlock = 201, 		/**< unlock the subcans latch, No parameter*/
  Latch_SetNeutral = 202, 	/**< set the subcans latch to neutral, No parameter. */
  Latch_GetStatus = 203,      /**<  Query the subcans latch status, No parameter. */
  //Ethera project commands.
  AirBrakes_Set = 250,  /**< set the airbrakes. Parameter : int(0-100) = position. */
  AirBrakes_Get = 251,  /**< set the airbrakes. No parameter. */
  AirBrakes_SetNumSteps = 252,  /**< gives the number of steps after a change of position. Parameter : positive integer, num of steps. */
  AirBrakes_GetNumSteps = 253, /**< gives the number of steps. No parameter. */
  AirBrakes_ForceZero = 254,  /**< set to position 0 and force additional steps towards 0. Parameter : positive integer, num of steps. */
};

/**  @ingroup CSPU_CansatAsgard
     Constants used to identify command responses (applicable when
     CansatFrameType is CmdResponse).
     Every response can include an additional parameters which is a free,
     human-readable message.

     Numerical values 0 to 99 are reserved for generic commands.
     Project-specific command responses should be in range 100-255 */
enum class CansatCmdResponseType {
  InvalidResponseType = 0,	/**< value returned when string to int conversion fails. */
  // --- Cross-project command responses
  CmdModeInitiated = 10,    /**< Response to successful InitiateCmdMode.
							   	 Parameters: none. */
  CmdModeTerminated = 11,   /**< Response to successful TerminateCmdMode.
								 Parameters: none. */
  DigitalWriteOK = 12,      /**< Response to a successful DigitalWrite.
							   	 Parameters: pinNumber,
							   	 state (0=LOW, 1=HIGH). */
  FileListEntry = 13,       /**< Response to a ListFiles command. This
								 message is repeated for each file.
								 Parameters: file sequence number (from 1),
								 file name, file size (0 if directory) */
  FileListComplete = 14,    /**< Conclusion of a successful ListFiles command,
								 sent after the last FileListEntry response to
								 denote the end of list of file names.
								 Parameters: path = the listed path,
								 the number of files listed,
								 1 if the end of the listing was reached or
								 0 if there are more files to list. */
  CampaignStatus = 15,	    /**< The current status of the campaign.
								 Parameters: status 1=started, 0=not started */
  FileContent = 16,	        /**< Announces the transmission of the content of a
								 file, surrounded by content markers
								 (StartFileContent and BeginFileContent)
								 Parameters: path = the transmitted file's path
								 Content of file is transmitted as one string
								 in several StringPart messages which must be
								 concatenated back together by the receiver. */
  InvalidPath = 17,		   /**<  Response to ListFile or GetFile command, when
								 provided path is invalid (inexistent,
								 inaccessible, not a directory when a file is
								 expected, or not a file when a directory is
								 expected...). */
  MissingCmdParameters = 18,/**< Response to request missing mandatory
								 parameters */
  MalformedRequest = 19,	/**< Response to a malformed request (no request
								 type, request type is not a number...) */
  FileError = 20,			/**< Response to a file operation request failing
								 for a generic file issue */
  BeginFileContent = 21,	/**< Response sent before starting the transmission
								 of the file content requested with FileContent
*/
  EndFileContent = 22,		/**< Response sent after transmitting the last part
	 	 	 	 	 	 	 	 of the file content requested with FileContent
*/
  ReadyForShutdown = 23,	/**< Response sent after successfully processing a
								 PrepareShutdown command */
  ShutdownCancelled = 24,	/**< Response sent after successfully cancelling a
								 PrepareShutdown command, in response to a
								 CancelShutdown command */
  ShutdownPreparationFailed = 25,
  /**< Response to an unsuccessful attempt to prepare the
   	 can for shutdown.	 */
  ShutdownCancellationFailed = 26,
  /**< Response to an unsuccessful attempt to cancel the
  	 can preparation for shutdown.	 */
  ParameterOperationFailed = 27, /**< Response to SetParameterValue or GetParameterValue
								      when operation failed.
								      Parameters: none. */
  ParameterValue = 28,		/**< Response to a successful GetParameterValue command.
								 Parameters: parameter name, parameter value. */
  ParameterValueSet = 29,	/**< Response to a successful SetParameterValue command.
								 Parameters: parameter name, parameter value. */
  UnsupportedRequestType = 99,	 /**< Request type was not recognized.
									  Parameters: the request type */
  // --- Project-specific commands
  // Torus project commands.
  CurrentWinchPosition = 100,	/**< Response to GetWinchPosition.
	 	 	 	 	 	 	 	 Parameters: length of rope (in mm),
	 	 	 	 	 	 	 	 	 	 	 1 if target position is reached
	 	 	 	 	 	 	 	 	 	 	 0 if current position is an
	 	 	 	 	 	 	 	 	 	 	 intermediate value toward
	 	 	 	 	 	 	 	 	 	 	 target.  */
  WinchTargetSet = 101,    /**< Response to SetWinchPosition when the new.
								target has been set.
								 Parameter: length of
	 	 	 	 	 	 	 	 rope unrolled (in mm) */
  WinchTargetReached = 102, /**< Second response to SetWinchPosition, sent
								as soon as the target is reached.
								Parameters: same as CurrentWinchPosition. */
  InvalidRopeLength = 103,	/**< Response to SetWinchPosition when rope length
								is not in range [0-65535] */
  WinchTargetNotReached = 104,  /**< Response to SetWinchPosition when target is not
	 	 	 	 	 	 	 	 reached after maximum delay. Parameters:
	 	 	 	 	 	 	 	 target rope length (mm),
	 	 	 	 	 	 	 	 timeout delay (msec) */
  NoServoConfigured = 105,	/**< Response to any Winch-related command when no
								 servoWinch has been configured */
  ServoNotRunning = 106,		/**< Response to any Winch-related command requiring
	 	 	 	 	 	 	 	 the servowinch to run, when the command is issued
	 	 	 	 	 	 	 	 while the servo is not running. */
  OutOfBoundsWinchTarget = 107,  /**< Response to SetTarget command with out of bound
	 	 	 	 	 	 	 	 target value.
	 	 	 	 	 	 	 	 Parameter: the rejected target value.  */
  // GMini project commands.
  Latch_Status = 200, 		/**< Response to LatchGetStatus. Give the status of the subcans
   	   	   	   	   	   	   	   	 latch. Parameter: the status:  0=neutral, 1=locked,
   	   	   	   	   	   	   	   	 2=unlocked, 3=undefined (before any lock/unlock was requested. */
  // Ethera project commands.
  AirBrakes_Status = 250,  /**< return the airbrakes positions. Response to get and set.
                                  Parameter : int(0-100) = position.  */
  AirBrakes_InvalidPosition = 251,  /**< "Invalid position" response, sent if the position
                                  is wrong in AirBrakes_Set command. Parameter None */
  AirBrakes_NumSteps = 252,  /**< return number of steps. Parameter : positive integer, num of steps. */
  AirBrakes_ForcedToZero = 253,  /**< confirm zero has been forced. No parameter. */

};
