/*
 * XBeeTypes.h
 *
 *  Created on: 18 févr. 2022
 *      Author: Alain
 */

#pragma once

/** A couple of types used in the interface and configuration of XBee modules */
class XBeeTypes  {
public:
	/** The three types of XBee devices in a network */
	enum class DeviceType {
		Coordinator=0, Router=1, EndDevice=2, Unknown=3
	};
	/** The 4 possible power level for transmission (0=lowest, 4 = highest */
	enum class TransmissionPowerLevel {
		level0=0, level1=1, level2=2, level3=3, level4=4} ;
};


