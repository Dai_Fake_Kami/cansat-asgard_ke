/*
 * SSC_HW_Scanner.h
 *
 *  Created on: 31 janv. 2020
 *
 */

#pragma once
#include "HardwareScanner.h"
#include "SSC_Config.h"
#ifdef RF_ACTIVATE_API_MODE
#include "SSC_XbeeClient.h"
#endif

#define DBG_DIAGNOSTIC 1

// Macros to ease use of API or transparent modes with XBee modules.
// See IsaTwoAcquisitionProcess for usage examples.
#ifdef RF_ACTIVATE_API_MODE
#  define GET_RF_STREAM(theSSC_HW_Scanner) theSSC_HW_Scanner.getRF_XBee();
#  define RF_OPEN_STRING(object) object->openStringMessage((uint8_t)SSC_RecordType::StatusMsg);
#  define RF_OPEN_STRING_PART(object) object->openStringMessage((uint8_t)SSC_RecordType::StringPart);
#  define RF_OPEN_CMD_REQUEST(object) object->openStringMessage((uint8_t)SSC_RecordType::CmdRequest);
#  define RF_CLOSE_STRING(object) object->closeStringMessage();
#  define RF_CLOSE_STRING_PART(object) object->closeStringMessage();
#  define RF_CLOSE_CMD_REQUEST(object) object->closeStringMessage();
#else
#  define GET_RF_STREAM(theSSC_HW_Scanner) theSSC_HW_Scanner.getRF_SerialObject();
#  define RF_OPEN_STRING(object)
#  define RF_OPEN_STRING_PART(object)
#  define RF_OPEN_CMD_REQUEST(object)
#  define RF_CLOSE_CMD_REQUEST(object)
#  define RF_CLOSE_STRING(object)
#  define RF_CLOSE_STRING_PART(object)
#endif

/**
 * 	@ingroup SpySeeCanCSPU
 * 	@brief The class used to interact with the Hardware of SpySeeCan.
 *  This class should not be operationally used for any other controller, since it
 *  performs configurations (e.g. input and output pins) which could conflict with
 *  the hardware allocation on the controller.
 *  It can nevertheless be used as stand-alone utility to detect the available
 *  hardware capabilities.
 */
class SSC_HW_Scanner: public HardwareScanner {
  public:
    typedef struct Flags {
      unsigned int SD_CardReaderAvailable:1;
      unsigned int BMP_SensorAvailable:1;
      unsigned int SI4432_SensorsAvailable:NumSI4432_Cards;
      // NB: no flag for the sensors: we cannot possibly detect tehm.
    } Flags;
    
    SSC_HW_Scanner(const byte unusedAnalogInPin=0);
    virtual ~SSC_HW_Scanner() {};
    virtual void SSC_Init();
    virtual void checkAllSPI_Devices();
    /** Print the complete diagnostic
     *  This method completes the diagnostic provided by the HardwareScanner
     *  @param stream The stream on which the diagnostic must be printed.
     */
    virtual void printFullDiagnostic(Stream& stream) const;
    virtual void printSPI_Diagnostic(Stream& stream) const;
    virtual void printI2C_Diagnostic(Stream& stream) const;
    virtual byte getLED_PinNbr(LED_Type type);
    const Flags& getFlags() const { return flags;};
    Stream* getGPS_SerialObject() const {
    	return getSerialObject(GPS_SerialPortNumber);
    }
#ifdef RF_ACTIVATE_API_MODE
    virtual SSC_XBeeClient *getRF_XBee() {
    	if (xbClientAvailable) {
    		return &xbClient;
    	}
    	else return nullptr;
    }
#endif
#ifdef IGNORE_EEPROMS  // This is just used to avoid consuming write cycles during tests.
    virtual byte getNumExternalEEPROM() const;
#endif

  private:
    Flags flags;  //  The flags to indicate the availability of the various devices. 
#ifdef RF_ACTIVATE_API_MODE
    bool	xbClientAvailable;	/**< True if the xbClient was succesfully initialized */
    SSC_XBeeClient xbClient;  /**< The interface to communicate with the XBee in API mode */
#endif
    
};
