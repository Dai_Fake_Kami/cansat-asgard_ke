/*
 * SSC_BMP280_Client.cpp
 */
#include "SSC_BMP280_Client.h"

bool SSC_BMP280_Client::readData(SSC_Record& record){
  record.pressure = getPressure();
  record.altitude = getAltitude();
  record.temperatureBMP = getTemperature();
  return true;
}
