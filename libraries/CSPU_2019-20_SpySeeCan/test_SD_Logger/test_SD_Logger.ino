/*
    Test for SD_Logger
    
    Tested here:
      Writing SSC_Record to sd card
      
    Other tests for SD logger:
      cansatAsgardCSPU/test_SD_Logger2
      cansatAsgardCSPU/test_SD-Flash_Logger

    Wiring:
      Uno: CS to D4, DI (MOSI) = D11, DO (MISO) = D12, SCK = 13, GND to GND, 5V to 5V
      Feather M0 Express: CS to 10, DI to MO (MOSI), DO to MI (MISO), CLK to SCK, GND to GND, 3V to 3V.
      Feather M0 Adalogger: no wiring required, CS is 4 (internal pin).

    Expected results:
      If everyting is correctly wired and SD card is inserted: all test should succeed
      If the device is not connected or the card is not inserted: tests 10 and 12 should succeed
*/

#include "SD_Logger.h"
#include "SSC_Record.h"
#include "StringStream.h"


const byte CS_Pin = 4; // change to 10 if using Feather M0 express

SD_Logger theLogger(CS_Pin);


#define USE_ASSERTIONS
#define DEBUG_CSPU

#include "DebugCSPU.h"


int succeededTests = 0;


void test1() {
  Serial << "Initialising new file..." << ENDL;
  auto initresult = theLogger.init("test", "", 0, "csv");
  bool logresult;
  if (initresult != 0) {
    Serial << "Init failed.";
    Serial << "Test failed." << ENDL; return;
  }
  Serial << "Init OK." << ENDL;

  Serial << "File name: " << theLogger.fileName() << ENDL;
  int fileSize = theLogger.fileSize();
  int expectedFileSize = 0;
  Serial << "File size (bytes) = " << fileSize << ENDL;
  Serial << "Expected file size (bytes) = " << expectedFileSize << ENDL;
  if (fileSize != expectedFileSize) {
    Serial << "File size is not as expected." << ENDL;
    Serial << "Test failed." << ENDL; return;
  }

  Serial << "Initializing SSC_Record..." << ENDL;
  SSC_Record record;
  String header;
  header.reserve(200);
  String data;
  data.reserve(200);

  Serial << "Initializing StringStreams..." << ENDL;
  StringStream headerStream(header);
  StringStream dataStream(data);

  Serial << "Getting header and data from SSC_Record..." << ENDL;
  record.printCSV_Header(headerStream);
  record.printCSV(dataStream);

  Serial << "Header: " << ENDL << header << ENDL;
  Serial << "Data: " << ENDL << data << ENDL;
  Serial << "Writing on SD Card..." << ENDL;
  logresult = theLogger.log(header);
  if (!logresult) {
    Serial << "Logging failed. \n Test failed. \n";
    return;
  }
  logresult = theLogger.log(data);
  if (!logresult) {
    Serial << "Logging failed. \n Test failed. \n";
    return;
  }
  fileSize = theLogger.fileSize();
  expectedFileSize = header.length() + data.length() + 4; // +4 for the endlines
  Serial << "File size (bytes) = " << fileSize << ENDL;
  Serial << "Expected file size (bytes) = " << expectedFileSize << ENDL;

  if (fileSize != expectedFileSize) {
    Serial << "File size is not as expected." << ENDL;
    Serial << "Test failed." << ENDL; return;
  }

  Serial << "End of test: all OK" << ENDL;
  succeededTests++;
}



void setup() {
  DINIT(115200);
  pinMode(CS_Pin, OUTPUT);
  SPI.begin();


  Serial << "Begin of tests." << ENDL << ENDL;

  Serial << "Test 1: writing an SSC_Record object on the SD Card" << ENDL;
  test1();
  Serial << ENDL;


  Serial << "End of all tests." << ENDL << "Result: " << succeededTests << "/" << "1" << ENDL;

}

void loop() {
  ;
}
