/*
    This sketch tests the initSerialWithTimeout() function.

*/

#include "DebugCSPU.h"

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  DINIT_WITH_TIMEOUT(115200);
  Serial << "*** Line sent just after init *** " << ENDL;
#ifndef ARDUINO_ARCH_SAMD
  Serial << "WARNING: this test is not significant on boards with an external UART (such as Genuino Uno") << ENDL;
  Serial << "         On such board, the test will always succeed because the Serial will always be ready." << ENDL;
  Serial << "         Run this test on an SAMD board (Feather M0 Express, ItsyBity M0 or M4...)" << ENDL;
  Serial << "Aborted." << ENDL;
  Serial.flush();
  exit(-1); 
#endif
}

void loop() {
  Serial << "Did you see the line sent just after init ? If so, you didn't miss any input" << ENDL;
  Serial << "Also run the test without connecting the Serial, and check the onboard LED is blinking every 5 secs" << ENDL;
  Serial << "If so, the program is indeed running even without a serial line connected" << ENDL << ENDL;
  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  delay(5000);

}
