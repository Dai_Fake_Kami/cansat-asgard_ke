/*
    Timer.cpp (see .h for info)
*/
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "Timer.h"
#include "SerialStream.h"

byte Timer::level = 1;

Timer::Timer(__FlashStringHelper* msg) {
  location = msg;

  for (int i = 1; i < level; i++) {
    Serial.print(F("  "));
  }
  Serial.print(level);
  Serial.print(F(". In "));
  Serial.println(location);
  level++;
  internalTimer = 0; // Set to 0 here, to avoid including the Serial output overhead. 
}

Timer::~Timer() {
  long frozenTimer=internalTimer; // stop timer before adding output overhead. 
  level--;
  for (int i = 1; i < level; i++) {
    Serial.print(F("  "));
  }
  
  Serial.print(level);
  Serial.print(F(". Out "));
  Serial.print(location);
  Serial.print(F(": "));
  Serial.print(frozenTimer);
  Serial.println(F(" msec"));
}

