/*
 * CSPU_Test.h
 */

#pragma once
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

class CSPU_Test {
public:
	/** Ask the user to inspect results and define whether they are ok or not.
	 *  @param msg The message presented to the user (it should not include a final
	 *         period or question mark and will be appended with ". Is this ok (y/n)?"
	 *  @param numErrors An error counter which will be incremented in case the test
	 *  	   result is not validated by the user.
	 *  @param separatorFirst If true, an end-of-line is output before asking the user.
	 *  @return true if the user confirmed the test is ok, false otherwise.
	 */
	static void requestCheck(const char* msg, uint16_t &numErrors, bool separatorFirst=false);                /**<Method used to request a visual check after an action.*/

	/** Ask a question to the user and expect a yes-or-no answer.
	 *  @param question The question presented to the user (it should not include a final
	 *         period or question mark and will be appended with " (y/n) ?"
	 *  @param separatorFirst If true, an end-of-line is output before asking the user.
	 *  @return true if the user answer with YES, false otherwise.
	 */	static bool askYN(const char* question, bool separatorFirst=false);

	 /** Pause until the user sends a character on Serial.
	  *  @return The character sent by the user. */
	 static char pressAnyKey();

	 /** Pause until the user sends a character on Serial.
	  *  @param msg The message to print before requesting a key to be pressed.
	  *  @return The character sent by the user. */
	 static char pressAnyKey(const char* msg);

	 /** Check whether a character was sent on Serial.
	  *  @return The character, if any sent, 0 if none.
	  */
	 static char keyPressed();

	 /** Blink a LED twice a second. Call this method in your main loop
	  *  @param LED_Pin  The pin connected to the LED to blink. It is
	  *  				 expected to a digital pin, configured as OUTPUT.
	  */
	 static void heartBeat(uint8_t LED_Pin=LED_BUILTIN);
};




