/*
    This program tests the ThermistorClient class with the following 4 thermistors
     - NTCLE100E3
     - NTCLE305E4103SB
     - NTCLE305E4502SB
     - NTCLG100E2104JB
     - VMA320 (break-out)

    Wiring:
      Vcc - NTCLE100E3 - A0 - 33kohm - GND
      VCC - NTCLE305E4103SB - A4-  10kOhm - GND
      VCC - NTCLG100E22
      104JB - A1 - 10kOhm - GND
      VMA320, looking from components side, thermistor facing up:
      - A2 to left
      - VCC to middle
      - Gnd to right.
      VCC - NTCLE300E3502SB - A5 - 10kOhm - GND


     Usage: use csvFormat constant to select between human-readable output and csv output (for use in Excel sheet);
*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

constexpr bool csvFormat = true;// SEt to true to generate CSV formatted output.

// (Constants describing wiring and thermistor models are now in there classes).

#include "CansatConfig.h"
#include "ThermistorSteinhartHart.h"
#include "ThermistorNTCLE100E3.h"
#include "ThermistorVMA320.h"
#include "ThermistorNTCLE305E4103SB.h"
#include "ThermistorNTCLE300E3502SB.h"
#include "ThermistorNTCLG100E2104JB.h"

#ifdef ARDUINO_AVR_UNO
constexpr float Vcc=5.0;
#elif defined(ARDUINO_ARCH_SAMD)
constexpr float Vcc=3.3;
#else
#error "Board not recognised"
#endif

ThermistorNTCLE100E3 NTCLE100E3(Vcc, A0, 33000.0);
ThermistorNTCLE305E4103SB NTCLE305E4103SB(Vcc, A4, 10000.0);
ThermistorNTCLE300E3502SB NTCLE300E3502SB(Vcc, A5, 4700.0);
ThermistorNTCLG100E2104JB NTCLG100E2104JB(Vcc, A1, 180000.0); // c'est pas 180000ohms ici? à vérifier !
ThermistorVMA320 VMA320(Vcc, A2, 10000.0);
ThermistorSteinhartHart VMA320_Simple(Vcc, A2, 10000.0, 1 / 298.15, 1 / 3950.0, 0, 0, 10000);

void setup() {
  // to know wich Vmax and Nsteps we have to use;

  DINIT(115200);
  Serial << "Testing Thermistor model classes for 5 thermistors" << ENDL;
  Serial << "  Vcc = "<< Vcc << "V" <<ENDL;
  if (csvFormat) {
    Serial << "time,NTCLE100E3, NTCLG100E2, VMA320, VMA320_Simple, NTCLE305E4103SB, NTCLE305E4502SB " << ENDL;
  }
}

void loop() {
  float temp_X = NTCLE100E3.readTemperature();
  float temp_A = NTCLE305E4103SB.readTemperature();
  float temp_D = NTCLE300E3502SB.readTemperature();
  float temp_B = NTCLG100E2104JB.readTemperature();
  float temp_C = VMA320.readTemperature();
  float temp_C_simple = VMA320_Simple.readTemperature(); //not always necessary (last year we decided not to use this formula)

  if (csvFormat) {
    Serial <<  millis() << ", " << temp_X << ", " << ", " << temp_B << ", " << temp_C << ", " /*<< temp_C_simple*/ << temp_A  << ", " << temp_D <<  ENDL;

  } else {
    Serial <<  millis() << "NTCLE100E3: " << temp_X << "°C, NTCLG100E2: " << temp_B  << "°C, VMA320: " << temp_C << "°C (simplifié: " << temp_C_simple << "°C) ,NTCLE305E4103SB: " << temp_A << "°C ,NTCLE305E4502SB: " << temp_D <<  ENDL;
  }
  delay(1000);
}
