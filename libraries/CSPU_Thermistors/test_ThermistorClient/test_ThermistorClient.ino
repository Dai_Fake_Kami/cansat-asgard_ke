/*
   test_ThermistorClient
*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatConfig.h"
#include "ThermistorClient.h"
#include "CansatRecord.h"
#include "BMP_Client.h"
#include "ThermistorGT103J1K.h"
#include "ThermistorNTCLE300E3502SB.h"
#include "ThermistorVMA320.h"

constexpr bool CsvFormat = true;
constexpr float Vcc = 3.3;
constexpr byte GT103_pin = A0;
constexpr byte NTCLE_pin = A1;
constexpr byte VMA_pin = A2;
constexpr uint32_t GT103_Resistor = 10000;
constexpr uint32_t NTCLE_Resistor = 10000;
constexpr uint32_t VMA_Resistor = 10000;

ThermistorGT103J1K 		thermistor1(Vcc, GT103_pin, GT103_Resistor);
ThermistorNTCLE300E3502SB 	thermistor2(Vcc, NTCLE_pin, NTCLE_Resistor);
ThermistorVMA320			thermistor3(Vcc, VMA_pin, VMA_Resistor);
ThermistorClient 			therm;
uint32_t counter = 0;
uint8_t numberOfThermistors = 1; // Valid values: 1, 2, 3

BMP_Client bmp;
CansatRecord record;

void setup() {
  DINIT(115200);
  if (CsvFormat) {
    Serial << "time (seconds),BMP280, GT103J1K, NTCLE300E3, VMA320" << ENDL;
  }
  if (bmp.begin(1037.6)) {
    Serial << "BMP OK" << ENDL;
  } else {
    Serial << "BMP FAILED" << ENDL;
  }
  if (therm.readData(record) != 0) {
    Serial << "Error: readData with no thermistor configured. It should return 0! " << ENDL;
    while (1);
  } else Serial << "Testing without Thermistor: OK!" << ENDL;
  therm.setThermistors(&thermistor1);
}  

void loop() {
  record.clear();
  counter++;
  switch (numberOfThermistors) {
    case 0:
      Serial << "Error: 0 thermistor configured (in loop). Aborted" << ENDL;
      exit(1);
      break;
    case 1:
      if (counter > 3) {
        therm.setThermistors(&thermistor1, &thermistor2);
        numberOfThermistors = 2;
        Serial << "Configured second thermistor..." << ENDL;
      }
      break;
    case 2:
      if (counter > 6) {
        therm.setThermistors(&thermistor1, &thermistor2, &thermistor3);
        numberOfThermistors = 3;
        Serial << "Configured third thermistor..." << ENDL;
      }
      break;
    default:
      break;
  }

  bool result = therm.readData(record);
  if (!result) {
    Serial << "*** error while reading temperature from the thermistors" << ENDL;
  }
  result = bmp.readData(record);
  if (!result) {
    Serial << "*** error while reading temperature from the bmp" << ENDL;
  }

  if (CsvFormat) {
    Serial << millis() / 1000.0 << "," << record.temperatureBMP << " ," << record.temperatureThermistor1 ;
#ifdef INCLUDE_THERMISTOR2
    Serial << " ," << record.temperatureThermistor2 ;
#endif
#ifdef INCLUDE_THERMISTOR3
    Serial << " , " << record.temperatureThermistor3;
#endif
    Serial << ENDL;
  }
  else
    record.print(Serial);

  delay(1000);

}
