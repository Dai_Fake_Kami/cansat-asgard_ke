/*
   ThermistorClient.cpp
*/
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "ThermistorClient.h"

// silence warnings about unused parameters in not all thermistors are used.
#if !defined(INCLUDE_THERMISTOR2) || !defined(INCLUDE_THERMISTOR3)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#endif

ThermistorClient::ThermistorClient(	Thermistor *therm1,
		Thermistor *therm2,
		Thermistor *therm3 ):
  thermistor1(therm1)
#ifdef INCLUDE_THERMISTOR2
, thermistor2(therm2)
#endif
#ifdef INCLUDE_THERMISTOR3
, thermistor3(therm3)
#endif
{
}

void ThermistorClient::setThermistors(	Thermistor *therm1,
		Thermistor *therm2,
		Thermistor *therm3 )
{
  thermistor1=therm1;
#ifdef INCLUDE_THERMISTOR2
  thermistor2=therm2;
#endif
#ifdef INCLUDE_THERMISTOR3
  thermistor3=therm3;
#endif
}
// restore warnings about unused silenced above
#if !defined(INCLUDE_THERMISTOR2) || !defined(INCLUDE_THERMISTOR3)
#pragma GCC diagnostic pop
#endif

bool ThermistorClient::readData(CansatRecord& record) const {
  bool anyThermistor = false;
  if (thermistor1) {
	  record.temperatureThermistor1 = thermistor1->readTemperature();
   anyThermistor = true;
  }
#ifdef INCLUDE_THERMISTOR2
  if (thermistor2) {
	  record.temperatureThermistor2 = thermistor2->readTemperature();
   anyThermistor = true;
  }
#endif
#ifdef INCLUDE_THERMISTOR3
  if (thermistor3) {
	  record.temperatureThermistor3 = thermistor3->readTemperature();
   anyThermistor = true;
  }
#endif
  return anyThermistor;
}
