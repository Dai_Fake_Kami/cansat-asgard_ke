/*
    This program tests the ThermistorClient class with the following thermistors
     - NTCLE100E3
     - NTCLE305E4103SB
     - NTCLE305E4502SB
     - NTCLG100E2
     - VMA320 (break-out)
     - Littelfuse GT103J1K
     and with the BMP, using a 3.3V regulator for the thermistors. 

    Wiring (VCC is the output of the voltage regulator):
      VCC - NTCLE305E4103SB - A4-  10kOhm - GND
      VCC - NTCLG100E2 - A1 - 10kOhm - GND
      Vcc - GT103J1K - A0 - 10kohms - GND
      VMA320, looking from components side, thermistor facing up:
      - A2 to left
      - VCC to middle
      - Gnd to right.
      VCC - NTCLE300E3502SB - A5 - 10kOhm - GND
      VCC - ARF

      µC    BMP280
      3V    Vin
      GND   GND
      SCL   SCK (with pull-up resistor to Vin)
      SDA   SDI (with pull-up resistor to Vin)

      µC   Regulator
      GND   GND
      USB   IN
            OUT = VCC
            EN to IN
     Usage: use csvFormat constant to select between human-readable output and csv output (for use in Excel sheet);

     and measurement of the tension 
*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

constexpr bool csvFormat = true;// SEt to true to generate CSV formatted output.

// (Constants describing wiring and thermistor models are now in there classes).

#define DEBUG
#include "DebugCSPU.h"
#include "ThermistorSteinhartHart.h"
//#include "ThermistorNTCLE100E3.h"
#include "ThermistorVMA320.h"
#include "ThermistorNTCLE305E4103SB.h"
#include "ThermistorNTCLE300E3502SB.h"
#include "ThermistorNTCLG100E2104JB.h"
#include "BMP_Client.h"
#include "ThermistorGT103J1K.h"

#ifdef ARDUINO_AVR_UNO
constexpr float Vcc=5.0;
#elif defined(ARDUINO_ARCH_SAMD)
constexpr float Vcc=3.3;
#else
#error "Board not recognised"
#endif

//ThermistorNTCLE100E3 NTCLE100E3(Vcc, A0, 33000.0);
ThermistorNTCLE305E4103SB NTCLE305E4103SB(Vcc, A4, 10000.0);
ThermistorNTCLE300E3502SB NTCLE300E3502SB(Vcc, A5, 4640.0);
ThermistorNTCLG100E2104JB NTCLG100E2(Vcc, A1, 178100.0); 
ThermistorVMA320 VMA320(Vcc, A2, 10000.0);
ThermistorSteinhartHart VMA320_Simple(Vcc, A2, 10000.0, 1 / 298.15, 1 / 3950.0, 0, 0, 10000);
ThermistorGT103J1K GT103J1K(Vcc, A0, 10000.0);

CansatRecord record;
constexpr float mySeaLevelPressure = 1034.6;
BMP_Client bmp;

void setup() {
  // to know wich Vmax and Nsteps we have to use;

  DINIT(115200);
  Serial << "Testing Thermistor model classes for 5 thermistors and the BMP280" << ENDL;
  Serial << "  Vcc = "<< Vcc << "V" <<ENDL;
  
  Wire.begin();
  if (!bmp.begin(mySeaLevelPressure)) {
    Serial.print("Could not find a valid BMP280 sensor, check wiring!");
    exit(-1);
  }
  analogReference(AR_EXTERNAL); // AR_EXTERNAL, no external for Feather M0 Express
  Serial << "Set reference tension to AR_EXTERNAL" << ENDL;
  if (csvFormat) {
    Serial << "time, tension de référence, NTCLG100E2, VMA320, VMA320_Simple, NTCLE305E4103SB, NTCLE305E4502SB, GT103J1K, BMP280 " << ENDL;
  }
}

void loop() {
  float temp_B = NTCLG100E2.readTemperature();
  float temp_C = VMA320.readTemperature();
  float temp_C_simple = VMA320_Simple.readTemperature(); //not always necessary (last year we decided not to use this formula)
  float temp_A = NTCLE305E4103SB.readTemperature();
  float temp_D = NTCLE300E3502SB.readTemperature();
  float temp_E = GT103J1K.readTemperature();
  //float res = GT103J1K.readResistance();
  int sensorValue = analogRead(A3);
  float V = sensorValue * (Vcc / 4095);

  if (csvFormat) {
    Serial <<  millis() << ", " << V << "V, " << temp_B << ", " << temp_C << ", " << temp_C_simple << ", " << temp_A  << ", " << temp_D << ", " << temp_E ;
    bool result = bmp.readData(record);
    if (result == true) {
    Serial << ", " << record.temperatureBMP << ENDL;
    } if (result == false) {
    Serial << ENDL; 
    }
   
  } else {
    Serial <<  millis() << ", tension: " << V << "°C, NTCLG100E2: " << temp_B  << "°C, VMA320: " << temp_C << "°C (simplifié: " << temp_C_simple <<
    "°C) ,NTCLE305E4103SB: " << temp_A << "°C ,NTCLE305E4502SB: " << temp_D << "°C, " << temp_E << "°C";
    bool result = bmp.readData(record);
    if (result == true) {
    Serial << ", temp BMP: " << record.temperatureBMP << "°C, pressure:" << record.pressure << " hPa, " << "altitude:" << record.altitude << " m" << ENDL;
    }
    if (result == false){
    Serial << "0 " << ENDL; 
    }
  }
  delay(1000);
}
