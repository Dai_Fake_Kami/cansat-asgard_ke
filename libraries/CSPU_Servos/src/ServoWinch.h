#pragma once
#if defined(ARDUINO_SAMD_FEATHER_M0_EXPRESS) || defined(ARDUINO_AVR_UNO) || defined(ARDUINO_AVR_MICRO)
    /* v1.1.8 of the Servo library does not support the SAMD51 processor. See note in class comment */
#include <Servo.h>
#else
#error "ServoWinch class not supported on current board."
#endif

/** @ingroup CSPU_Servos
    @brief Class used for controlling the servo-motor
    @note As of Aug. 2021, the Servo library does not support SAMD51 processors. As a consequence
    	  this class compiles, but is not operational (with a cpp warning) when compiled for
    	  the ItsyBitsy M4 board. There is nevertheless no compilation error, to allow for
    	  the use of other classes of the CansatAsgard library.
*/

class ServoWinch {
  public:
    /**
      @param maxRopeLenToUse the rope length, when fully extended
	  @param minPulseWidth the pulse width, in microseconds, corresponding to the minimum extension of the rope
      @param maxPulseWidth the pulse width, in microseconds, corresponding to the maximum extension of the rope
    */
    ServoWinch(uint16_t maxRopeLenToUse, uint16_t minPulseWidthToUse, uint16_t maxPulseWidthToUse);

    /** What value types can be used when changing servo-motor position */
    enum class ValueType {
      pulseWidth,	/**< When a value represents the pulse width of the servo */
      ropeLength	/**< When a value represents the extension of the rope */
    };

    /** Start the servo-motor and set it to the initial length. Call this before calls to any other method.
      @param PWM_Pin The PWM pin used by the motor
      @param initPositionType the type of the initial position: angle (in degree; 0 is fully retracted, 180 is fully extended) or ropeLength (in mm)
      @param initPosition the desired initial position. In degrees if initPositionType is angle, in mm if initPositionType is ropeLength

      @return true if servo was successfully initialized and set to initPosition, false otherwise
    */
    bool begin(byte PWM_Pin, ValueType initPositionType, uint16_t initPosition);

    /** Power off the motor. If you want to use the motor later, you will have to call begin again.
      @return true if the motor was successfully powered off, false otherwise
    */
    bool end();

    /** Set the servo-motor to the desired angle or set the rope extension to the desired value
      @param type the type of the value: angle (in degree; 0 is fully retracted, 180 is fully extended) or ropeLength (in mm)
      @param value The desired value to be set. In degrees if type is angle, in mm if type is ropeLength

      @return true if servo was successfully set to the desired position, false otherwise
    */
    bool set(ValueType type, uint16_t value);

    /** Get the current rope length
      @return the current rope length (mm)
    */
    uint16_t getCurrRopeLen() const;

    /** Get the current pulse width
      @return the current pulse width (microseconds)
    */
    uint16_t getCurrPulseWidth() const;

    /** Query the servo to check whether it is running */
    bool isRunning() const { return running;};

  protected:
    /** Calculate rope length for a given pulse width (maps the values linearly)
      @param pulseWidth the given pulse width of the motor (microseconds)
      @return the rope length (mm)
    */
    virtual uint16_t getRopeLenFromPulseWidth(uint16_t pulseWidth) const;

    /** Calculate pulse width for a given rope length (maps the values linearly)
      @param ropeLen the given rope length (mm)
      @return the pulse width (microseconds)
    */
    virtual uint16_t getPulseWidthFromRopeLen(uint16_t ropeLen) const;


    uint16_t currRopeLen = 0;		/**< By how much is the rope currently extended (mm) */
    uint16_t currPulseWidth = 0;	/**< What is the current pulse width (microseconds) */
    uint16_t maxRopeLen;			/**< By how much can the rope be extended (mm) */
    uint16_t minPulseWidth;			/**< The minimum pulse width for the servo (microseconds) */
    uint16_t maxPulseWidth;			/**< The maximum pulse width for the servo (microseconds) */
    bool running = false;			/**< True if the motor is powered on, false otherwise */


  private:
#if defined(ARDUINO_SAMD_FEATHER_M0_EXPRESS) || defined(ARDUINO_AVR_UNO) || defined(ARDUINO_AVR_MICRO)
    Servo servo;					/**< The arduino servo-motor object. v1.1.8 of the library does not
     	 	 	 	 	 	 	 	 *   support the SAMD51 processor. */
#else
#warning "ServoWinch class not supported on current board."
#endif

    friend class ServoWinch_Test;	/**< Friend class used for testing */
};

