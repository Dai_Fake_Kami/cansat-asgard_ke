// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatConfig.h"
#include "ServoWinch.h"
#include "elapsedMillis.h"

const uint16_t maxRopeLen = 500;

const byte PWM_Pin = 9;
const uint16_t minPulseWidth = 1000;
const uint16_t maxPulseWidth = 2000;

ServoWinch servo (maxRopeLen, minPulseWidth, maxPulseWidth);

class ServoWinch_Test {
  public:
    uint16_t getRopeLenFromPulseWidth(uint16_t pulseWidth){
      return servo.getRopeLenFromPulseWidth(pulseWidth);
    }
    uint16_t getPulseWidthFromRopeLen(uint16_t ropeLen){
      return servo.getPulseWidthFromRopeLen(ropeLen);
    }
};

ServoWinch_Test testHelper;

void waitForUser(char msg[] = "Press any key to continue") {
  while (Serial.available() > 0) {
    Serial.read();
  }
  Serial.println(msg);
  Serial.flush();
  while (Serial.available() == 0) {
      delay(300);
  }
  Serial.read();
}


void setup() {
  DINIT(115200);

  waitForUser("Press any key to start motor and fully extend");
  servo.begin(PWM_Pin, ServoWinch::ValueType::ropeLength, maxRopeLen);

  waitForUser("Press any key to go to middle (using pulse width)");
  servo.set(ServoWinch::ValueType::pulseWidth, (minPulseWidth+maxPulseWidth)/2);

  waitForUser("Press any key to fully retract (using pulse width)");
  servo.set(ServoWinch::ValueType::pulseWidth, minPulseWidth);


  waitForUser("Press any key to go to middle (using rope length)");
  servo.set(ServoWinch::ValueType::ropeLength, maxRopeLen/2);

  waitForUser("Press any key to fully retract (using rope length)");
  servo.set(ServoWinch::ValueType::ropeLength, 0);

  waitForUser("Press any key to test precision of conversion angle/rope length");
  uint16_t doubleInverseOperation; uint16_t delta; bool ok;

  ok = false; delta = 0;
  while(!ok) {
    delta++; ok = true;
    for(int i = minPulseWidth; i <= maxPulseWidth; i++) {
      doubleInverseOperation = testHelper.getPulseWidthFromRopeLen(testHelper.getRopeLenFromPulseWidth(i));
      ok = i-delta <= doubleInverseOperation && doubleInverseOperation <= i+delta; if(!ok) break;
    }
  }
  Serial.print("Conversion pulseWidth->ropeLen->pulseWidth precise with delta = +-"); Serial.print(delta); Serial.println(" microseconds.");

  ok = false; delta = 0;
  while(!ok) {
    delta++; ok = true;
    for(int i = 0; i <= maxRopeLen; i++) {
      doubleInverseOperation = testHelper.getRopeLenFromPulseWidth(testHelper.getPulseWidthFromRopeLen(i));
      ok = i-delta <= doubleInverseOperation && doubleInverseOperation <= i+delta; if(!ok) break;
    }
  }
  Serial.print("Conversion ropeLen->pulseWidth->ropeLen precise with delta = +-"); Serial.print(delta); Serial.println(" mm.");

  waitForUser("Press any key to test speed of conversion");
  int tmp;
  float timeForOneConversion;
  elapsedMillis timeForAllConversions = 0;
  for(int i = minPulseWidth; i <= maxPulseWidth; i++) {
    tmp = testHelper.getRopeLenFromPulseWidth(i);
  }
  timeForOneConversion = timeForAllConversions*1000/(maxPulseWidth - minPulseWidth + 1);
  Serial.print("Conversion time pulseWidth->ropeLength averages "); Serial.print(timeForOneConversion, 2); Serial.println(" microseconds.");

  timeForAllConversions = 0;
  for(int i = 0; i <= maxRopeLen; i++) {
    tmp = testHelper.getPulseWidthFromRopeLen(i);
  }
  timeForOneConversion = timeForAllConversions*1000/(maxRopeLen + 1);
  Serial.print("Conversion time ropeLength->pulseWidth averages "); Serial.print(timeForOneConversion, 2); Serial.println(" microseconds.");

  waitForUser("Press any key to turn off motor");
  servo.end();

  Serial.print("Motor off.");
  
}

void loop() {
  // put your main code here, to run repeatedly:

}
