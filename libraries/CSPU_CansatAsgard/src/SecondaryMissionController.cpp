/*
 * SecondaryMissionController.cpp
 */

#include "CansatConfig.h"
#include "SecondaryMissionController.h"

 void SecondaryMissionController::run(CansatRecord& record) {
	if (elapsed >= CansatSecondaryMissionPeriod) {
		elapsed=0;
		manageSecondaryMission(record);
	}
}

#ifdef RF_ACTIVATE_API_MODE
bool SecondaryMissionController::begin(CansatXBeeClient* xbeeClient) {
	 xbee=xbeeClient;
	 return true;
};
#else
bool SecondaryMissionController::begin(Stream* RF_Stream) {
	 xbee=RF_Stream;
	 return true;
};
#endif


