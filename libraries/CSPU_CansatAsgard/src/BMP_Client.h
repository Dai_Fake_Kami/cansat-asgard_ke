/*
    BMP_Client.h
*/
#pragma once
#include "CansatConfig.h"
#ifdef BMP_USE_BMP3XX_MODEL
#  include "Adafruit_BMP3XX.h"
#else
#  include "Adafruit_BMP280.h"
#  include "Adafruit_Sensor.h"
#endif

#include "Wire.h"
#include "CansatRecord.h"

/** @ingroup CSPU_CansatAsgard
    @brief Interface class to an Adafruit BMP sensor (connected to the I2C bus),
    providing the required methods to access individual
    data and to fill the relevant fields of a CansatRecord.
    @par Support of BMP280 and BMP3XX flavours
    This class supports the Adafruit BMP280 and BMP3XX sensors (depending on the
    symbol BMP_USE_BMP3XX_MODEL defined in CansatConfig.h).
    @par Descent velocity support
    Computation of descent velocity is conditional to symbol INCLUDE_DESCENT_VELOCITY
    and is configured by constants BMP_NumPeriodsForDescentVelocityCalculation
    (number of acquisition periods over which the velocity must be calculated) and
    BMP_NumDescentVelocitySamplesToAverage (number of velocity samples to use
    for the moving average).
	@note In the original architecture, each project needed to subclass this client
	to populate the right class of record. Since the 2020 archtecture upgrade, this
	is no longer required, and any project can use the client and feed it the right
	subclass of CansatRecord.
*/
class BMP_Client {
  public:
    BMP_Client();
    /**
        @brief Initialize the BMP280 sensor before use.
        @param theSeaLevelPressure The pressure at sea level in hPa (required
        	   to compute altitude).
        @return True if initialization was successful, false otherwise.
    */
    bool begin(float theSeaLevelPressure);

    /** Obtain the atmospheric pressure in hPa
        @param numSamples The number of sensor readings to average.
        @return The current pressure, or InvalidPressure if reading failed */
    float getPressure(uint8_t numSamples = BMP_NumSamplesForPressureReading);

    /** Obtain the altitude (from sea level) in m as computed using the current pressure and the
        sea level pressure provide during initialization.
        It is obtained from a _single_ pressure reading. For a better
        stability, read pressure first with getPressure(), using the appropriate number
        of samples and derive altitude afterwards using convertPressureToAltitude();
        @return The current altitude. InvalidAltitude if the calculation fails for any reason.*/
    float getAltitude();

    /** Obtain the atmospheric temperature in °C
        @return The current temperature, or InvalidTemperature is an error
        		occured while reading. */
    float getTemperature() ;

    /** Convert a pressure value in altitude (from sea level) in m as computed using the current pressure and the
        sea level pressure provide during initialization.
        @param pressureInHPa The pressure to convert.
        @return The current altitude. InvalidAltitude if the calculation fails for any reason.*/
    float convertPressureToAltitude(float pressureInHPa);

    /** @brief Read sensor data and populate data record.
       @param record The data record to populate (fields BMPtemperature, altitude, pressure,
       	   	   referenceAltitude (if INCLUDE_REF_ALTITUDE is defined), descentVelocity (if
       	   	   INCLUDE_DESCENT_VELOCITY is defined). descentVelocity is averaged on the
       	   	   BMP_NumDescentVelocitySamplesToAverage last samples.
       	   	   @warning Be sure to test descentVelocity, pressure and altitude against
       	   	   BMP_Client::InvalidDescentVelocity, BMP_Client::InvalidPressure and
       	   	   BMP_Client::InvalidAltitude respectively before using the data.
       @return True if reading was successful. If individual values cannot be
       	   	   calculated, the InvalidXXX constants are used. Be sure to check
       	   	   values are different from them.
    */
    bool readData(CansatRecord& record);

    /* Definition of invalid values to denote error in processing */
    static constexpr float InvalidPressure = 0.0f;
    			/**< The value return as pressure when sensor reading fails */
    static constexpr float InvalidAltitude = -1.0f; 
    			/**< The value return as altitude when pressure sensor reading fails */
    static constexpr float InvalidDescentVelocity = 9999.9f;
    			/**< The value return as descent velocity when pressure sensor reading fails */
    static constexpr float InvalidTemperature = -9999.9f;
    			/**< The value return as temperature when sensor reading fails */
    static constexpr float MinRealisticPressure = 370.0f;
    		    /**< Lowest pressure ever recorded was 870hPa (1979 during typhoon, source wikipedia).
  	  	  	  	  	 Pressure decrease by about 100hPa/1000m, so minimum realistic pressure
  	  	  	  	  	 under 5000 m is about 370 hPa. Any pressure under this value
  	  	  	  	  	 will be reported as InvalidPressure. */

  private:
#ifdef BMP_USE_BMP3XX_MODEL
    Adafruit_BMP3XX bmp;  //**< The driver class.
#else
    Adafruit_BMP280 bmp;  //**< The driver class.
#endif

    float seaLevelPressure; //**< Atmospheric pressure at sea level, in hPA

#ifdef INCLUDE_DESCENT_VELOCITY
  protected:
    /** @brief Compute descent velocity using the provided record as input and comparing
     * 		   it with the time and altitude provided BMP_NumPeriodsForDescentVelocityCalculation
     * 		   records earlier.
     *  @param record The record from which the altitude and timestamp must be used.
     *  	   It is assumed that the record already contains the pressure and altitude
     *  	   information. The record is not modified.
     *  @return the current descent Velocity. InvalidDescentVelocity if the calculation fails for any reason
     */
    float computeDescentVelocity(const CansatRecord& record);

    /** @brief Compute a movingAverage on the last BMP_NumDescentVelocitySamplesToAverage
     * 		   values of the velocity.
     *  @return The resulting average value.
     */
    float computeMovingAverage(float velocity);

    /** @brief Compute the descent velocity from the data of the provided record,
     *  applying a moving average (this method calls computeDescentVelocity() and
     *  computeMovingAverage() to get the work done.
     *  @param record The record from which the altitude and timestamp must be used.
     *  	   It is assumed that the record already contains the pressure and altitude
     *  	   information. The record is not modified.
     *  @return The average descent velocity
     */
    float computeAveragedDescentVelocity(const CansatRecord& record);

#ifdef INCLUDE_REFERENCE_ALTITUDE
    /** Update the reference altitude if necessary.
     *	@param record The record containing the absolute altitude.
     */
    void updateReferenceAltitude(CansatRecord& record);
#endif

  private:
    /**  Table storing the previous altitudes which must be available to
     *   compute the descent velocity. */
    float previousAltitudes[BMP_NumPeriodsForDescentVelocityCalculation];

    /**  Table storing the timestamps for the previous altitudes stored
     *   in previousAltitudes[] (required to compute the descent velocity. */
    uint32_t previousTimestamp[BMP_NumPeriodsForDescentVelocityCalculation];

    /**  The index to the oldest altitude and timestamps stored in tables
     *   previousAltitudes[] and previousTimestamp[].    */
    uint8_t currentIdx; 
    
    /**  Table storing the previous descent velocities which must be available to
     *   compute the moving average. */
    float previousDescentVelocity[BMP_NumDescentVelocitySamplesToAverage];

    /**  The index to the oldest velocity value stored in table
     *   previousDescentVelocity[]   */
    uint8_t currentVelocityIdx;

    /** The current sum of all descent velocity values stored in table
     *  previousDescentVelocity[]. It is updated whenever a new value is stored
     *  by substracting the removed value and adding the new one. */
    float velocitySum;

#  ifdef INCLUDE_REFERENCE_ALTITUDE
	unsigned long refAltitudeCycleBegin = 0; /**< When did the reference altitude measurement cycle begin. */
	unsigned long lastCallToUpdateReferenceAltitude = 0; /**< When was updateReferenceAltitude last called. */

	float refAltitude = DefaultReferenceAltitude;	 /**< The reference altitude of the cansat. */
	float minAltitudeInLastCycle = DefaultReferenceAltitude; /**< The minimum recorded altitude in the last reference altitude measurement cycle. */
	float maxAltitudeInLastCycle = DefaultReferenceAltitude; /**< The maximum recorded altitude in the last reference altitude measurement cycle. */
	friend class RefAltitude_Test;		/**< Friend class used for testing */
#  endif
#endif
};
