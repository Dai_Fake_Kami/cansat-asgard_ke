/*
   SD_Logger.h
 */

#pragma once
#include "Logger.h"
#include <SdFat.h>
#include "elapsedMillis.h"
#include "CansatRecord.h"

/** @ingroup CSPU_CansatAsgard
 *  @brief  A class providing the required SD Services to the StorageManager

   @par Design issues
   The SD library cause some issues:
     - It includes an out-dated version of SdFat, and does not expose any method to access free space (confirmed in doc)
     - It is incompatible with a recent version of SdFat.
   On the other hand, recent versions of SdFat expose all interfaces of SD, AND methods to access free space AND have a 
   lower memory consumption (https://forum.arduino.cc/index.php?topic=274784.0).
   As a consequence, we do not use the SD library but the SdFat. 

   Initialization of library: this class initializes the SdFat library transparently (as part of the initStorage()) but NOT the SPI library.
   
   @par Design options:
   1. do we close the file after each write or just flush it ? Is there any problem removing
   the SD_Card with open & flushed files? Performance is excellent with both options (<30 msec / write, for about 60 bytes of data).
   2. would it be better to flush or close/reopen in doIdle() rather than in log?  Implemented but no significant improvement. 
 */
class SD_Logger : public Logger {
  public:
    SD_Logger(const byte theChipSelectPinNumber = 10);
    ~SD_Logger();
    /**
     * set the pin Number to use as chipSelect 
     * @warning this method must only be used before the object is initialized 
     * @param ChipSelect the number of the pin 10
     */
     void setChipSelect( byte ChipSelect); 
    /* Append data to the logfile, completed with a "\n" and flush the file */
    bool log(const String& data, const bool addFinalCR=true);

    /* Append data to the logfile, completed with a "\n" and flush the file */
    bool log(const char* str, const bool addFinalCR=true);

    /* Append data from a CansatReocrd to the logfile (in CSV format), terminated
     * with a "\n" and flush the file
     * @param record The record to store.*/
    bool log(const CansatRecord& record);

    /**  @return the size in bytes of the logfile in bytes (does not work for files larger than 2Gb on
     *  8-bits boards due to unsigned long limitation).
     */
    virtual unsigned long fileSize() ;

    /** Obtain a pointer to the SdFat object (fully initialized and ready to use) if any
     *  @return Pointer to the SdFat object, or null if none available.
     */
    SdFat* getSdFat(){ return &mySD;};

    /** Return the free space on SD card. */
    float getFreeSpaceInMBytes() ;

  protected:
    /** Initialize the underlying storage structures
     *  @return True if everything ok, false otherwise.
     */
    bool initStorage();
    virtual bool fileExists(const char* name);
  private:
    SdFat mySD;
    byte chipSelectPinNumber;
    static bool storageInitialized; /**< true if the storage lib was initialized by any instance */ 
    bool instanceInitialized;  /**< true if this instance is initialized. */

};
