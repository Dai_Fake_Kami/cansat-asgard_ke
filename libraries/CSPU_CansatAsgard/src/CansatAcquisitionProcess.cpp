/*
 * CansatAcquisitionProcess.cpp
 */
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatConfig.h"  // Contains includes for DebugCSPU.h and Timer.h, with appropriate configuration.
#include "CansatAcquisitionProcess.h"
#include "CansatXBeeClient.h"
#include "StringStream.h"

// Include header files for the right thermistor classes defined in
// CansatConfig.h. This requires some preprocessor tricks...
// See https://stackoverflow.com/questions/32066204/construct-path-for-include-directive-with-macro
// for details.
#define IDENT(x) x
#define XSTR(x) #x
#define STR(x) XSTR(x)
#define PATH(x,y) STR(IDENT(x)IDENT(y))
#define H_EXTENSION .h

#include PATH(THERMISTOR1_CLASS,H_EXTENSION)
#ifdef INCLUDE_THERMISTOR2
#include PATH(THERMISTOR2_CLASS,H_EXTENSION)
#endif
#ifdef INCLUDE_THERMISTOR3
#include PATH(THERMISTOR3_CLASS,H_EXTENSION)
#endif

// DBG_INIT, DBG_DIAGNOSTIC, DBG_ACQUIRE
// PRINT_ACQUIRED_DATA, PRINT_ACQUIRED_DATA_CSV are defined in CansatConfig.h
#define INIT_INFO 1 // Set to 1 to have the progress of initialisation documented on Serial.

CansatAcquisitionProcess::CansatAcquisitionProcess()
  : AcquisitionProcess(CansatAcquisitionPeriod), // Period from CansatConfig.h
	campaignStarted(false),
	lastRecordAltitude(-1),
	bmp(),
#ifdef ARDUINO_ARCH_SAMD
	gps((GPS_SerialPortNumber==1 ? Serial1 : Serial2)),
#endif
	thermistor(),
	storageMgr(CansatAcquisitionPeriod, CansatCampainDuration),
	averageSpeed(0.0f),
	record(nullptr)
{}

CansatAcquisitionProcess::~CansatAcquisitionProcess(){
	if (record) {
		delete record;
	}
}

void CansatAcquisitionProcess::init() {
  DPRINTSLN(INIT_INFO, "Initializing CansatAcquisitionProcess");
  // Get record before calling init(): init() will call all other
  // initialization methods which will initialize the record.
  record=getNewCansatRecord();
  if (!record) {
	  DPRINTSLN(DBG_DIAGNOSTIC, "*** Fatal error: pointer to record is null ***");
  }
  AcquisitionProcess::init();

  CansatHW_Scanner* hw=getHardwareScanner();
#ifdef PRINT_DIAGNOSTIC_AT_INIT
  hw->printFullDiagnostic(Serial);
  DDELAY(DBG_INIT, 1000);
#endif

  String CSV_Header;
  CSV_Header.reserve(200);
  StringStream sstr(CSV_Header);
  record->printCSV(sstr, CansatRecord::DataSelector::All, CansatRecord::HeaderOrContent::Header);
  storageMgr.init(	(*record),
		  	  	  	hw,
					CSV_Header,
		  	  	  	SD_FilesPrefix,
					SD_CardChipSelect,
					SD_RequiredFreeMegs,
					EEPROM_KeyValue);

  auto RF=GET_RF_STREAM((*hw));
  if (RF != NULL)
  {
	RF_OPEN_STRING(RF);
    *RF << F("\nLogging to ") << storageMgr.getSD_FileName();
    RF_CLOSE_STRING(RF);
  }

#if (defined(CANSAT_USE_EEPROMS) && (DBG_DIAGNOSTIC==1))
  float duration = ((float) storageMgr.getNumFreeEEEPROM_Records()) * CansatAcquisitionPeriod / 1000.0 / 60.0;
  Serial << F("EEPROM can accept ") << duration << F(" min. of data. (key=0x");
  Serial.print(EEPROM_KeyValue, HEX);
  Serial << ")" << ENDL;
  if (RF != NULL)
  {
	RF_OPEN_STRING(RF);
    *RF << F("EEPROM can accept ") << duration << F(" min. of data. Key=0x");
    RF->println(EEPROM_KeyValue, HEX); // This is currently not supported in API mode. Method to be added.
    RF_CLOSE_STRING(RF);
  }
#endif

  // Always print header on Serial port, even if PRINT_ACQUIRED_DATA_CSV is undefined:
  // Records are always printed when campaign is not started.
  Serial << ENDL << "Record header:" << ENDL;
  Serial << CSV_Header << ENDL;

  if (MinSpeedToStartCampaign > 10.0f) {
	  DPRINTSLN(DBG_DIAGNOSTIC, "*** Error: MinSpeedToStartCompaign should be lower than 10m/s");
  }

  DPRINTSLN(DBG_INIT, "CansatAcquisitionProcess::init done");
  if (StartMeasurementCampaignImmediately) {
	  startMeasurementCampaign("Setting in CansatConfig.h");
  }

  // Configure pin to force manually a campaign start
  if (ActivateStartCampaignPin && StartCampaignPin) {
	  pinMode(StartCampaignPin, INPUT_PULLUP);
	  Serial << "	Pin " << StartCampaignPin << " configured to force campaign start" << ENDL;
  }

  if (getSecondaryMissionController()) {
	  bool result;
#ifdef RF_ACTIVATE_API_MODE
	  result = getSecondaryMissionController()->begin(getXBeeClient());
#else
	  auto RF = GET_RF_STREAM((*CansatAcquisitionProcess::getHardwareScanner()));
	  result = getSecondaryMissionController()->begin(RF);
#endif
      if (result == false) {
        DPRINTS( DBG_DIAGNOSTIC, "***Error initializing SecondaryMissionController ***");
      }
  } 
  DPRINTSLN(INIT_INFO, "CansatAcquisitionProcess initialized.");
}

void CansatAcquisitionProcess::startMeasurementCampaign(const char* msg, float value) {
	campaignStarted = true;
#ifdef ARDUINO_ARCH_SAMD
	gps.enable(true); // Enable GPS is case it was disabled. This is harmless if
					  // if was already enabled, or if the enable pin is not connected.
#endif
	setLED(LED_Type::Campaign, LED_State::Off);

	String str("# === CAMPAIGN STARTED at ");
		// Initial # to create a comment line in the SD file.
	StringStream ss(str);
	ss << millis() << ": "<< msg << " (" << value << ") ===";
	DPRINTLN(DBG_DIAGNOSTIC, str.c_str());
	auto RF=GET_RF_STREAM((*CansatAcquisitionProcess::getHardwareScanner()));
	if (RF != NULL) {
#ifdef RF_ACTIVATE_API_MODE
		// Do not assume the XBee module is awake or sleeping
		if (UseXBeeSleepMode) {
			RF->wakeUpXBeeModule();  // Wake-up in case it's sleeping
			RF->configureSleepMngtPins(pin_XBeeSleepRequest,pin_XBeeOnSleep, false); // Permanently disable sleep mode.
		}
		if (!RF->setTransmissionPower(InCampaignPowerLevel)) {
			DPRINTSLN(DBG_DIAGNOSTIC, "**** Error: could not change transmission power level");
		}
#endif
		RF_OPEN_STRING(RF);
		*RF << str.c_str() << ENDL;
		RF_CLOSE_STRING(RF);
	}
	storageMgr.storeString(str);
}

void CansatAcquisitionProcess::stopMeasurementCampaign() {
	campaignStarted = false;
	// Do NOT disable GPS here: it will be disabled in the acquisition
	// loop as soon as a fix is acquired.
	lastRecordAltitude = -1;
	averageSpeed=0.0f;
	setLED(LED_Type::Campaign, LED_State::On);

	DPRINTSLN(DBG_DIAGNOSTIC, "");
	DPRINTSLN(DBG_DIAGNOSTIC, "=== CAMPAIGN STOPPED ===");
	auto RF=GET_RF_STREAM((*CansatAcquisitionProcess::getHardwareScanner()));
	if (RF != NULL) {
#ifdef RF_ACTIVATE_API_MODE
		// Sanity check
		if (UseXBeeSleepMode && RF->ModuleInSleepMode()) {
			DPRINTSLN(DBG_DIAGNOSTIC, "*** ERROR: module sleeping when campaign stopped??");
		}
#endif
		delay(200); // The command can be received at any moment, possibly immediately
					// after a record is sent. Avoid congestion by waiting.
		RF_OPEN_STRING(RF);
		*RF << ENDL << F("=== CAMPAIGN STOPPED ===");
		RF_CLOSE_STRING(RF);
#ifdef RF_ACTIVATE_API_MODE
		RF->setTransmissionPower(OutOfCampaignPowerLevel);
		if (UseXBeeSleepMode) {
			RF->configureSleepMngtPins(pin_XBeeSleepRequest, pin_XBeeOnSleep, true);
			RF->putXBeeModuleToSleep();
		}
#endif
	}
}

void CansatAcquisitionProcess::updateCampaignStatus() {
  static byte ignoredSamples=10;
  static elapsedMillis elapsedSinceLastInfo=0;
  static unsigned long lastRecordTimestamp=0;

  // Check whether the StartCampaignPin is used to toggle the status
  if (ActivateStartCampaignPin && StartCampaignPin && digitalRead(StartCampaignPin)==LOW) {
	  if (campaignStarted == true) stopMeasurementCampaign();
	  else startMeasurementCampaign("Forced through HW pin!", 0);
	  delay(500); // wait to avoid toggling more than once
	  return;
  }

  if (campaignStarted == true ) {
    return;
  }

  // Let's not start before readings are stable
  if (ignoredSamples >0) {
	  ignoredSamples--;
	  lastRecordTimestamp= record->timestamp;  // maintain the timestamp, otherwise it would cause an interruption and a new reset.
	  return;
  }

  // Check absolute altitude. This is a security, to cover for a µC reset during the flight
  if (record->altitude >= AltitudeThresholdToStartCampaign) {
	  DPRINTS(DBG_DIAGNOSTIC, "Forcing campaign start because altitude > ");
	  DPRINTLN(DBG_DIAGNOSTIC, AltitudeThresholdToStartCampaign);
	  startMeasurementCampaign("Altitude above threshold!", record->altitude);
	  return;
  }

  // Acquisition mode could have been interrupted, and our last information
  // could be outdated. In this case, we start averaging again
  if ((record->timestamp - lastRecordTimestamp) > (3*CansatAcquisitionPeriod)) {
	  DPRINTSLN(DBG_DIAGNOSTIC, "Reseting average speed calculation");
	  lastRecordTimestamp= record->timestamp;
	  lastRecordAltitude=-1; // This will cause a reset of the averaging.
	  ignoredSamples=10;     // This will cause the next 10 samples to be ignored.
	  return;
  }

  if (lastRecordAltitude == -1) {
	averageSpeed=0.0f; // Reset here in case the campaign was stopped manually after startup
	lastRecordAltitude = record->altitude;
    lastRecordTimestamp= record->timestamp;

    DPRINTSLN(DBG_DIAGNOSTIC, "");
    DPRINTS(DBG_DIAGNOSTIC, " ...Waiting for average speed of ");
    DPRINT(DBG_DIAGNOSTIC, MinSpeedToStartCampaign);
    DPRINTS(DBG_DIAGNOSTIC, " m/s during ");
    DPRINT(DBG_DIAGNOSTIC, NumSamplesToAverageForStartingCampaign);
    DPRINTS(DBG_DIAGNOSTIC, " readings. last rec. altitude=");
    DPRINTLN(DBG_DIAGNOSTIC, lastRecordAltitude);
    return;
  }

  // If the altitude is invalid, we don't do anything, campaign is not
  // started.
  if (record->altitude == BMP_Client::InvalidAltitude) {
	  return;
  }

  float speed = (record->altitude - lastRecordAltitude) / (record->timestamp - lastRecordTimestamp) * 1000;
  // Limit speed to 10 m/s to avoid than a single aberrant reading would increase
  // the average beyond the threshold.
  if (speed > 10.0f) speed = 10.0f;
  averageSpeed -= averageSpeed / NumSamplesToAverageForStartingCampaign;
  averageSpeed += speed / NumSamplesToAverageForStartingCampaign;

#ifdef DBG_CAMPAIGN_STARTED
  if ((elapsedSinceLastInfo > ReportingPeriodWhileWaitingForStartCampaign) &&
		  (averageSpeed > 2.0*MinSpeedToStartCampaign/3.0) ){
	  CansatHW_Scanner* hw=getHardwareScanner();
	  auto RF=GET_RF_STREAM((*hw));
	  if (RF != NULL)
	  {
		  RF_OPEN_STRING(RF);
		  *RF << millis() << ": Waiting for take-off...";
		  RF_CLOSE_STRING(RF);
		  RF_OPEN_STRING(RF);
		  *RF <<  "   current  alt.= " << record->altitude << ", prev. alt.= " << lastRecordAltitude;
		  RF_CLOSE_STRING(RF);
		  RF_OPEN_STRING(RF);
		  *RF <<  "   current speed= " << speed << ", avg speed= " << averageSpeed;
		  RF_CLOSE_STRING(RF);
	  }
	  DPRINTS (DBG_CAMPAIGN_STARTED, "Waiting for take-off.  curr. alt.=");
	  DPRINT(DBG_CAMPAIGN_STARTED, record->altitude);
	  DPRINTS (DBG_CAMPAIGN_STARTED, "  prev. altitude=");
	  DPRINT(DBG_CAMPAIGN_STARTED, lastRecordAltitude);
	  DPRINTS (DBG_CAMPAIGN_STARTED, "  speed=");
	  DPRINT(DBG_CAMPAIGN_STARTED, speed);
	  DPRINTS (DBG_CAMPAIGN_STARTED, "  avg speed=");
	  DPRINTLN(DBG_CAMPAIGN_STARTED, averageSpeed);
	  elapsedSinceLastInfo=0;
  }
#endif
  lastRecordAltitude = record->altitude;
  lastRecordTimestamp= record->timestamp;

  if (averageSpeed > MinSpeedToStartCampaign) {
	  startMeasurementCampaign("Average speed above threshold!", averageSpeed);
  }
}

void CansatAcquisitionProcess::acquireDataRecord() {
  static byte recordCounter = 0;
  DBG_TIMER("CansatAcqProcess::acqDataRec");
  DPRINTSLN(DBG_ACQUIRE, "CansatAcqProcess::acquireDataRecord()");
  record->clear();
  record->timestamp = millis();

  // use clients to fill buffer.
  DPRINTSLN(DBG_ACQUIRE, "Reading BMP");
  bmp.readData(*record);
#ifdef ARDUINO_ARCH_SAMD
  DPRINTSLN(DBG_ACQUIRE, "Reading GPS");
  gps.readData(*record);
#endif
  DPRINTSLN(DBG_ACQUIRE, "Reading thermistor(s)");
  thermistor.readData(*record);
  DPRINTSLN(DBG_ACQUIRE, "Acquiring 2ndary mission data");
  acquireSecondaryMissionData(*record); // Let the subclass populate secondary mission data.

  // Let the controller define whatever must be done and possibly add information
  // in the record.
  if (getSecondaryMissionController()) getSecondaryMissionController()->run(*record);

#ifdef PRINT_ACQUIRED_DATA
  {
    DBG_TIMER("Serial output");
    Serial << ENDL;
    record->print(Serial);
  }
#endif

#ifdef PRINT_ACQUIRED_DATA_CSV
  if (campaignStarted || (recordCounter >= CansatPrintEvery_X_Records))
  {
    DBG_TIMER("Serial output CSV");
    record->printCSV(Serial,CansatRecord::DataSelector::All, CansatRecord::HeaderOrContent::Content);
    Serial.println();
    if (!campaignStarted) {
  	  Serial.println(record->newGPS_Measures ? "GPS Fix OK" : "No GPS fix.");
    }
  }
#endif
  DDELAY(DBG_SLOW_DOWN_TRANSMISSION, 500);


  if (campaignStarted || (recordCounter >= CansatPrintEvery_X_Records)) {
	  DPRINTSLN(DBG_ACQUIRE, "Transmission required");
	  sendDataRecordToRF();
	  recordCounter = 0;
	  DPRINTSLN(DBG_ACQUIRE, "RF Transmission OK");

#if (DBG_DIAGNOSTIC == 1)
	  if (!campaignStarted) {
		  DPRINTSLN(DBG_ACQUIRE, "Serial display required");
		  // When campaign is not started we also send the record in CSV format, so it appears
		  // on the Serial output
		  String CSV_Buffer;
		  StringStream str(CSV_Buffer);
		  record->printCSV(str, CansatRecord::DataSelector::All,
				  CansatRecord::HeaderOrContent::Content);
		  DPRINTLN(DBG_DIAGNOSTIC, CSV_Buffer.c_str());
	  } // not campaign started.
#endif
  }

  recordCounter++;  // Increase even if not printed or sent on RF.

#ifdef ARDUINO_ARCH_SAMD
  // If campaign is not started and GPS fix has been acquired,
  // disable GPS is so required by the configuration.
  if (!campaignStarted) {
	  if (CansatGPS_DisableBeforeCampaign && gps.hasFix()) {
		  gps.enable(false);
	  }
  }
#endif

  DPRINTSLN(DBG_ACQUIRE, "End acquireDataRecord()");
}

void CansatAcquisitionProcess::sendDataRecordToRF() {
	DBG_TIMER("RF transmission");
	setLED(LED_Type::Transmission, LED_State::On);
	transmitRecordToXBeeModule();
	setLED(LED_Type::Transmission, LED_State::Off);
}

void CansatAcquisitionProcess::transmitRecordToXBeeModule(uint32_t destSH, uint32_t destSL) {
	auto RF=GET_RF_STREAM((*CansatAcquisitionProcess::getHardwareScanner()));
	if (RF != NULL)
	{
#ifdef RF_ACTIVATE_API_MODE
		if (!RF->send(*record, 0, destSH, destSL)) {
#if (DBG_DIAGNOSTIC==1)
			Serial << "*** Error sending record (ts=" << record->timestamp;
			Serial << ") to 0x";
			Serial.print(destSH, HEX);
			Serial << "-";
			Serial.println(destSL, HEX);
#endif

		}
#else
		record->printCSV(*RF); // Print slowly to avoid buffer overflow.
		RF->println();
#endif
	} // RF not null
}


void CansatAcquisitionProcess::storeDataRecord(const bool campaignStarted) {
  if (campaignStarted) {
#ifdef CANSAT_USE_EEPROMS
    setLED(LED_Type::UsingEEPROM, LED_State::On); // The storage LED is managed by IsaTwoAcquisitionProcess::run().
    storageMgr.storeOneRecord(*record, true); // during campaign, store in EEPROM
    setLED(LED_Type::UsingEEPROM, LED_State::Off);
#else
    storageMgr.storeOneRecord(*record, false);
#endif
  } else {
    storageMgr.storeOneRecord(*record, false); // Out of campaign do not store in EEPROM
  }
}

void CansatAcquisitionProcess::initCansatProject()  {
  DPRINTSLN(INIT_INFO, "  Initializing Cansat Project");
  CansatHW_Scanner* hw=getHardwareScanner();
  auto RF=GET_RF_STREAM((*hw));

  // 1. Configure RF Power level & Transmit hardware scanner diagnostic.
  if (RF != NULL)
  {
	  // Configure RF Power level
#ifdef RF_ACTIVATE_API_MODE
	  RF->setTransmissionPower(OutOfCampaignPowerLevel);
#endif
	  // Send diagnostic and header to RF
	  setLED(LED_Type::Transmission, LED_State::On);
	  DPRINTS(INIT_INFO, "  Transmitting full diagnostic...");

#ifdef RF_ACTIVATE_API_MODE
	  // This is ugly and highly inefficient: to be redesigned...
	  String stringBuffer;
	  StringStream s(stringBuffer);
	  hw->printFullDiagnostic(s);

	  auto last=stringBuffer.length()-1;
	  unsigned int first=0;
	  uint8_t idx=0;
	  RF->wakeUpXBeeModule();
	  while (first <= last) {
		  RF_OPEN_STRING_PART(RF, idx++);
		  if (first + XBeeClient::MaxStringSize < last) {
			  *RF << stringBuffer.substring(first, first+XBeeClient::MaxStringSize);
			  first+=XBeeClient::MaxStringSize;
		  } else {
			  *RF << stringBuffer.substring(first, last).c_str();
			  first=last+1;
		  }
		  RF_CLOSE_STRING_PART(RF);
		  if ((idx%5)==0) {
			  DPRINTS(INIT_INFO, ".");
			  delay(350);
		  } else {
			  delay(150); // 150 not enough
		  }
	  }
	  RF->putXBeeModuleToSleep();
	  DPRINTSLN(INIT_INFO, "OK");

#else
	  hw->printFullDiagnostic(*RF);
#endif
	  setLED(LED_Type::Transmission, LED_State::Off);
  } else {
	  DPRINTSLN(DBG_DIAGNOSTIC, "*** RF is null. No RF object ***");
  }

  // 2. Initialize sensors and other modules
  if (hw->isI2C_SlaveUpAndRunning(I2C_BMP_SensorAddress)) {
     if(bmp.begin(SeaLevelPressure_HPa))
     {
    	 DPRINTSLN(DBG_DIAGNOSTIC, "BMP initialized");
     }
     else {
    	 DPRINTSLN(DBG_DIAGNOSTIC, "*** ERROR initializing BMP");
     }
  }

#ifdef ARDUINO_ARCH_SAMD
  gps.begin((GPS_Client::Frequency) CansatGPS_Frequency, pin_GPS_Enable, true);
  // Awlays start with GPS enabled: it will possibly be disabled after a fix is
  // acquired until the campaign is started (if enable pin is connected and
  // pin_GPS_Enable and CansatGPS_DisableBeforeCampaign parameters are configured
  // accordingly in CansatConfig.h
  DPRINTS(DBG_DIAGNOSTIC, "GPS initialized. Enable pin=");
  DPRINTLN(DBG_DIAGNOSTIC,pin_GPS_Enable);
  if (CansatUsesExternalReference)
  {
	  analogReference (AR_EXTERNAL);
	  DPRINTSLN(DBG_DIAGNOSTIC, "Analog reference set to 'AR_EXTERNAL'.");
  }
  else {
	  DPRINTSLN(DBG_DIAGNOSTIC, "Analog reference is internal.");
  }
#else
  DPRINTSLN(DBG_DIAGNOSTIC, "GPS NOT initialized (not a SAMD board).");
  if (CansatUsesExternalReference)
  {
	  analogReference (EXTERNAL);
	  DPRINTSLN(DBG_DIAGNOSTIC, "Analog reference set to 'EXTERNAL'.");
  }
  else {
	  DPRINTSLN(DBG_DIAGNOSTIC, "Analog reference is internal.");
  }
#endif
  initThermistorClient(thermistor);

  // 3. Clear record.
  DASSERT(record != nullptr);
  record->clear();

  DPRINTSLN(INIT_INFO, "  Cansat Project initialized");
}

void CansatAcquisitionProcess::initThermistorClient(ThermistorClient& thermClient) {
	DPRINTSLN(DBG_INIT, "CansatAcqProcess::initThermistorClient");
	Thermistor* therm1 =
			new THERMISTOR1_CLASS(	ThermistorTension,
									Thermistor1_AnalogInPinNbr, Thermistor1_Resistor);
	Thermistor* therm2 = nullptr;
	Thermistor* therm3 = nullptr;
#ifdef INCLUDE_THERMISTOR2
	therm2 = new THERMISTOR2_CLASS(ThermistorTension,
										   Thermistor2_AnalogInPinNbr, Thermistor2_Resistor);
#endif
#ifdef INCLUDE_THERMISTOR3
	therm3 = new THERMISTOR3_CLASS(ThermistorTension,
								  Thermistor3_AnalogInPinNbr, Thermistor3_Resistor);
#endif
	thermClient.setThermistors(therm1, therm2, therm3);
	DPRINTSLN(DBG_INIT, "CansatAcqProcess::initThermistorClient done");
}

void CansatAcquisitionProcess::doIdle()
{
   storageMgr.doIdle();
}

HardwareScanner* CansatAcquisitionProcess::getNewHardwareScanner() {
	DPRINTSLN(DBG_INIT, "CansatAcqProcess::getNewHardwareScanner");
	return new (CansatHW_Scanner);
}

CansatRecord* CansatAcquisitionProcess::getNewCansatRecord() {
	return new CansatRecord;
}
