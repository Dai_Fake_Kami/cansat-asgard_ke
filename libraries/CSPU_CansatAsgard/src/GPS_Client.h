#include <Adafruit_GPS.h>
#ifdef __AVR__
#include <SoftwareSerial.h>
#endif
#include "CansatRecord.h"

/**
   @ingroup CSPU_CansatAsgard
   @brief This class acquires the following data from the gps: latitude, longitude, altitude, velocity (direction and angular speed).
   Latitude & longitude are expressed in the WGS_1984 referential. Altitude is relative to the mean sea level (geoid).
   The data sent by the GPS at a frequency defined in the call of begin() will be then stored in an SSC_Record.

   @warning Only a single instance of this class should be created.
*/
class GPS_Client {
  public:
    /**
         @param  serialPort The serial port to which the GPS is physically connected.
    */
    /**The feather m0 and other SAMD boards do not support a software serial, there has to be a clause that assigns a hardwareSerial to the class if the feather is used*/
#ifdef __AVR__
    GPS_Client(SoftwareSerial& serialPort);
#elif defined(ARDUINO_ARCH_SAMD)
    GPS_Client(HardwareSerial& serialPort);
#endif

    /** Enum values for possible frequencies */
    enum class Frequency {
      // Developer note: make sure value is the frequency in Hz to allow for casting to integer
      F1Hz = 1, /**< Set frequency to 1Hz */
      F5Hz = 5, /**< Set frequency to 5Hz */
      F10Hz = 10 /**< Set frequency to 10Hz */
    };
    
    /**
      @brief Call this method once in the the setup to configure the GPS device.
      @param updateFrequency The desired update frequency (Frequency::F1Hz, Frequency::F5Hz, or Frequency::F10Hz).
             Default is Frequency::F5Hz.
      @param enablePinNbr The pin connected to the "Enable" line of the GPS, if any, or
      	  	 0 if none. If provided, it is configured as digital output, the GPS is
      	  	 initially enabled, and method enable()
      	  	 can be used to further power up or shutdown the GPS.
      @param initiallyEnabled If an enable pin is provided (enablePinNbr!=0), this
      	  	 defines whether the GPS should be initially enabled or not. Without an
      	  	 enable pin, this parameter is unused.
    */
    void begin(const Frequency updateFrequency = Frequency::F5Hz,
    		uint8_t enablePinNbr=0, bool initiallyEnabled=true);

    /**
        @brief Change GPS update frequency
        @param updateFrequency The desired update frequency (Frequency::F1Hz, Frequency::F5Hz, or Frequency::F10Hz)
    */
    void changeFrequency(const Frequency updateFrequency);

    /**
        @brief Acquire GPS data and store in provided record
        @param record The SSC record in which fields GPS_LongitudeDegrees,GPS_LatitudeDegrees,GPS_Altitude,(GPS_VelocityKnots,GPS_VelocityAngleDegrees) are filled with GPS readings. newGPS_Measures field is also updated.
    */
    void readData(CansatRecord &record);

    /** @brief enable or disable the GPS using the Enable line connected on the pin
     *  	   provided in call to begin(). If none was provided, this method does
     *  	   not do anything.
     *  @param status The new status of the GPS (true=enabled, false=disabled.
     */
    void enable(bool status);

    /** @brief Query the status of the GPS device (enabled or not). If not enabled,
     * 		   no communication with the device is possible.
     * 		   This method returns the logical status of the GPS without actual
     * 		   communication.
     * @return True if enabled, false otherwise.
     */
    bool enabled() const;

    /** @brief Query the status of the GPS fix. The method reports any fix
     * 		   (GPS fix), 2 (DGPS fix), 3 (PPS fix).
     * 		   If the GPS is not enabled, it returns false, whatever the fix
     * 		   status of a possible previous use.
     * @return True if enabled with a fix, false otherwise.
     */
    bool hasFix() const;
  private:
    /** @brief Configure the GPS module at power up, according to the settings
     *  	   stored in the class */
    void configureGPS();

    /** @brief Fill a record with the last valid values obtained from the GPS.
     *  @param record The record to fill.
     */
    void usePreviousValues(CansatRecord &record);

    Adafruit_GPS myGps;		  /**< The Adafruit gps driver */
    float lastLongitudeValue; /**< Last recorded longitude */
    float lastLatitudeValue;  /**< Last recorded latitude  */
    float lastAltitudeValue;  /**< Last recorded altitude  */
#ifdef INCLUDE_GPS_VELOCITY
    float lastVelocityKnots;  /**< Last recorded velocity */
    float lastVelocityAngle;  /**< Last recorded velocity angle */
#endif
    uint8_t enablePin;		  /**< The pin connected to the Enable line of the GPS
    							   break-up, or 0 if not connected */
    Frequency currentUpdateFrequency; /**< The GPS update frequency in use */
};
