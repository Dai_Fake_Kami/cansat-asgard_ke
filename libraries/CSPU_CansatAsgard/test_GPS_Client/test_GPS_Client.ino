/*
 *  Test program for class GPS_Client in library CansatAsgardCSPU, 
 *  except for the enable/disable function (see specific test program).
 */
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "GPS_Client.h"
#include "Adafruit_GPS.h"


#define SERIAL2 //define if data should be transfered via Serial2 
                //   Feather M0 Express: TX is on D10 and RX on D11
                //   ItsyBitsy M4: TX is on A2 and RX on A3.

#ifdef __AVR__
#include "SoftwareSerial.h"
SoftwareSerial mySerial(3 , 2);
#elif defined(ARDUINO_ARCH_SAMD)

#  ifdef SERIAL2
#  include "Serial2.h"
HardwareSerial &mySerial = Serial2;
#  else
HardwareSerial &mySerial = Serial1;
#  endif
#else
#error "Only AVR and SAMD architectures are supported"
#endif
const bool detailedOutput = true; // Set to true to print all values on Serial
const uint8_t pinGPS_Enable=11;

GPS_Client g(mySerial);
CansatRecord rec;

unsigned long lastCycleChange = 0;
unsigned long lastCycleLength = 0;

GPS_Client::Frequency frequenciesToTest[] = {GPS_Client::Frequency::F1Hz, GPS_Client::Frequency::F5Hz, GPS_Client::Frequency::F10Hz};
int testingFrequencyIndex = 0;
const long int changeTestingFrequencyEvery = 10000; //milliseconds
unsigned long int lastFrequencyChange = 0;

void setup() {
  DINIT(115200);
  Serial << "calling gps.begin..." << ENDL;
  //Pass parameter GPS_Client::Frequency::F...Hz where ... is 1, 5 or 10. If nothing is passed, 5 is default
  g.begin(frequenciesToTest[testingFrequencyIndex], pinGPS_Enable, true /* enabled by default*/);
  Serial << "Setup ok. " << ENDL;
}

void loop() {
  static unsigned long lastMillis = millis();
  rec.clear();
  rec.timestamp = millis();
  g.readData(rec);

  float currentLongitude = rec.GPS_LongitudeDegrees;
  float currentLatitude = rec.GPS_LatitudeDegrees;

  if (rec.newGPS_Measures) {
    lastCycleLength = millis() - lastCycleChange;
    lastCycleChange = millis();
  }

  unsigned long delta = rec.timestamp - lastMillis;
  if (detailedOutput) {
    Serial.print(rec.timestamp);
    Serial.print(":  Longitude:"); Serial.print(currentLongitude , 10); //prints the different parameters sent by the gps
    Serial.print(", Latitude:"); Serial.print(currentLatitude , 10);
    Serial.print(", Altitude:"); Serial.print(rec.GPS_Altitude , 10);
    Serial << "  DeltaT =" << delta << ENDL;
    rec.newGPS_Measures ? Serial.println("NEW GPS MEASURES") : Serial.println("No new GPS measures");
    Serial << "Testing frequency: " << (int)frequenciesToTest[testingFrequencyIndex] << " Hz" << ENDL;
    Serial << "Actual frequency: " << (float)1000/(float)lastCycleLength << " Hz" << ENDL;
    Serial << "Length of last cycle (time between the 2 last changes) (in milliseconds): " << lastCycleLength << ENDL;
  }
  if (currentLatitude < 45) Serial << "******** Abnormal value of latitude ********" << ENDL;
  if (currentLongitude < 3) Serial << "******** Abnormal Value of longitude ********" << ENDL;


  unsigned long expectedCycleDuration = 1000 / ((int)frequenciesToTest[testingFrequencyIndex]);


  if ((lastCycleLength > expectedCycleDuration + 50) || (lastCycleLength < expectedCycleDuration - 50)) {
    Serial << "******** Abnormal frequency (target is " << (int)frequenciesToTest[testingFrequencyIndex] << "Hz) ********" << ENDL;
  }

  if (millis() - lastFrequencyChange >= changeTestingFrequencyEvery) {
    lastFrequencyChange = millis();
    testingFrequencyIndex = (testingFrequencyIndex + 1) % (sizeof(frequenciesToTest) / sizeof(*frequenciesToTest));
    g.changeFrequency(frequenciesToTest[testingFrequencyIndex]);
  }

  lastMillis = rec.timestamp;


  Serial << ENDL;

  delay(10); //test possibilities


}
