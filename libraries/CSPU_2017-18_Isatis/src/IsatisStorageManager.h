/*
 *  IsatisStorageManager.h
 *  A class extending StorageManager specifically for the ISATIS project
 *
 *  Created on: 20 janv. 2018
 *   
 */

#pragma once

#include "StorageManager.h"
#include "IsatisDataRecord.h"

class  IsatisStorageManager: public StorageManager {
public:
	IsatisStorageManager(unsigned int theCampainDurationInSec, unsigned int theMinStoragePeriodInMsec);
	/* Perform the ISATIS-specific part of data storage, after the storage manager has validated that acquisition was performed successfully
	 * and the data is relevant (as evaluated by the dataRelevant() method. */
	NON_VIRTUAL void storeIsatisRecord(const IsatisDataRecord& data,const bool useEEPROM);
};

