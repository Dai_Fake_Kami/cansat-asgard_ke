/*
   Software to interact with the ADT7420 sensor on Diligent Pmod TMP2 break-out board.
   Ref: https://create.arduino.cc/projecthub/digilent/using-the-pmod-tmp2-with-arduino-uno-c172f2
*/

class ADT7420 {
  public:
  static float readTemperature(const byte ADT7420_Address);
  static bool init(const byte ADT7420_Address);
};

