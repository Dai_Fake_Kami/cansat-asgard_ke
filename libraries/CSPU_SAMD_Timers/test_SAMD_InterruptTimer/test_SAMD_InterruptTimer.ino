/*
   Test portability of SAMD_InterruptTimer library for SAMD21 and SAMD51
*/

// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatConfig.h"
#include "SAMD_InterruptTimer.h"

uint32_t startTS;


const uint32_t TargetPeriod1 = 500;
const uint32_t TargetPeriod2 = 1000;

volatile unsigned long counter1 = 0;
volatile unsigned long counter2 = 0;

void IRQ_CallBack1() {
  static uint32_t lastTS;
  uint32_t now = millis();
  uint32_t delta = now - lastTS;
  Serial << now << ": IRQ1: period=" << TargetPeriod1  << ENDL;
  if (delta != TargetPeriod1) {
    Serial << " *** Error on period=" << delta << ENDL;
  }

  counter1++;
  lastTS = now;
}

void IRQ_CallBack2() {
  static uint32_t lastTS;
  uint32_t now = millis();
  uint32_t delta = now - lastTS;
  Serial << now << ": IRQ2: period=" << TargetPeriod2  << ENDL;
  if (delta != TargetPeriod2) {
    Serial << "Error on period=" << delta << ENDL;
  }
  counter2++;
  lastTS = now;
}

SAMD_InterruptTimer timer1, timer2;

void dummyHandler() { };

void setup() {
  Serial.begin(115200);
  while (!Serial);
  delay(100);
  // TODO: test disableAll before start.
  // Check library logic
  Serial << "Available timers: " << SAMD_InterruptTimer::getNumAvailableTimers() << ENDL;
  DASSERT(SAMD_InterruptTimer::getNumAvailableTimers() == 16);
  Serial << "Used timers: " << SAMD_InterruptTimer::getNumTimers() << ENDL;
  DASSERT(SAMD_InterruptTimer::getNumTimers() == 0);
  Serial << "Timer1 enabled (before start): " << (int) timer1.isEnabled() << ENDL;
  DASSERT(!timer1.isEnabled());
  Serial << "Timer2 enabled (before start): " << (int) timer2.isEnabled() << ENDL;
  DASSERT(!timer2.isEnabled());

  // You can use up to 16 timer for each SAMD_ISR_Timer
  Serial << "Starting timers..." << ENDL;
  timer1.start(TargetPeriod1, IRQ_CallBack1);
  timer2.start(TargetPeriod2, IRQ_CallBack2);

  DASSERT(timer1.isEnabled());
  DASSERT(timer2.isEnabled());
  Serial << "Available timers: " << SAMD_InterruptTimer::getNumAvailableTimers() << ENDL;
  DASSERT(SAMD_InterruptTimer::getNumAvailableTimers() == 14);
  delay(3000);
  Serial << "Toggling timer1... " << ENDL;
  timer1.toggle();
  DASSERT(!timer1.isEnabled());
  delay(2000);
  Serial << "Toggling timer1 back... " <<  ENDL;
  timer1.toggle();
  DASSERT(timer1.isEnabled());
  delay(2000);
  Serial << "Disabling all timers... " << ENDL;
  SAMD_InterruptTimer::disableAll();
  DASSERT(!timer1.isEnabled());
  //DASSERT(!timer2.isEnabled());
  delay(2000);
  Serial << "Enabling all timers... " << ENDL;
  SAMD_InterruptTimer::enableAll();
  DASSERT(timer1.isEnabled());
  DASSERT(timer2.isEnabled());
  delay(2000);

  Serial << "Exhausting timers..." << ENDL;
  {
    SAMD_InterruptTimer extraTimers[14];
    for (unsigned int i = 0; i < 14; i++) {
      Serial << "Starting extra timer " << i << "...." << ENDL;
      if (!extraTimers[i].start(10000, dummyHandler))
      {
        Serial << "***** Error creating extraTimers[" << i << "]" << ENDL;
      }
      Serial << "Remaining timers: " << SAMD_InterruptTimer::getNumAvailableTimers() << ENDL;
      DASSERT(SAMD_InterruptTimer::getNumAvailableTimers() == 14 - (i + 1));
      DASSERT(SAMD_InterruptTimer::getNumTimers() == i + 3);
      delay(100);
    } // for
    Serial << "Creating one time too many..." << ENDL;
    SAMD_InterruptTimer extraneousTimer;
    if (extraneousTimer.start(10000, dummyHandler))
    {
      Serial << "***** Error: extraneous timer started!!" << ENDL;
    }
    DASSERT(SAMD_InterruptTimer::getNumAvailableTimers() == 0);
    for (unsigned int i = 0; i < 14; i++) {
      Serial << "Deleting extra timers " << i + 2 << "...." << ENDL;
      extraTimers[i].clear();
      Serial << "Remaining timers: " << SAMD_InterruptTimer::getNumAvailableTimers() << ENDL;
      DASSERT(SAMD_InterruptTimer::getNumAvailableTimers() == i + 1);
      DASSERT(SAMD_InterruptTimer::getNumTimers() == 16 - (i + 1));
      delay(100);
    } // for
  } // block
  delay(2000);

  Serial << "Clearing timer 1..." << ENDL;
  timer1.clear();
  delay(1000);
  Serial << "Changing period of Timer 2 to 2 s..." << ENDL;
  timer2.changePeriod(2000);
  delay(5000);
  Serial << "Clearing timer 2.... (resources should be released if SAMD_InterruptTimerHelper::ReleaseDynamicResourcesWhenNotNeeded is true)" << ENDL;
  timer2.clear();
  Serial << "Timer 2 cleared" << ENDL;
  delay (1000);
  Serial << "Restarting timer 2 (500 msec)... (resources should be reallocated if previously released)" << ENDL;
  bool result = timer2.start(500, IRQ_CallBack1);
  if (!result) Serial << "** Error while starting..." << ENDL;
  DASSERT(result);
  delay(2000);
  Serial << "Closing shop..." << ENDL;
  timer2.clear();
  DASSERT(SAMD_InterruptTimer::getNumAvailableTimers() == 16);
  DASSERT(SAMD_InterruptTimer::getNumTimers() == 0);
  Serial << "End of job." << ENDL;
} // setup

void loop() {
  delay(1000);
}
