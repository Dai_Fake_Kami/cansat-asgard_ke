/*
   test_Imager

   Wiring:
      Arduino Uno: I2C: SDA=A4, SCL=A5
                   SPI: MOSI=11   MISO=12  SCK=13
                   GND, VCC, SD_CS, Camera_CS
*/




#define DEBUG_CSPU
#include "DebugCSPU.h"
#include <Wire.h>
#include <SPI.h>
#include "IsaTwoImager.h"
#include "IsaTwoConfig.h"

const bool continuousImaging = true;

// Parameters for constant imaging.
const uint8_t smallestSize = OV2640_160x120;
const uint8_t largestSize = OV2640_1600x1200;
const uint8_t imgSizeIncrement=8; // for OV2640, min size = 0 (120x160) max = 8 (1200x1600)
const uint8_t  firstLightMode=Auto;
const uint8_t  lastLightMode=Auto; // Set to Cloudy to test auto/sunny/cloudy

const byte SD_CS = 12;
const byte Camera_CS = 5;
const byte triggerPin = 10;

const byte RegulatorEnablePin = 11;
const byte DebugCtrlPin = 9;

IsaTwoImager img(Camera_CS);
bool initOK;
unsigned int counter = 1;

void  setup()
{
  DINIT_IF_PIN(115200, DebugCtrlPin)
  Serial << "test_Imager sketch..." << ENDL;

  SPI.begin();
  Wire.begin();
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);

  // enable regulator, in case the camera is powered by it
  pinMode(RegulatorEnablePin, OUTPUT);
  digitalWrite(RegulatorEnablePin, HIGH);
  // Waiting for regulator to kick in and components to start
  delay(2000);

  Serial << F("Initialising the IsaTwoImager... (CS: SD=") << SD_CS << F(", Camera=") << Camera_CS << ")" <<  ENDL;
  while (!img.begin(SD_CS))
  {
    Serial << F("Error during IsaTwoImager.begin()") << ENDL;
    delay(1000);
  }
  pinMode(triggerPin, INPUT_PULLUP);
  if (continuousImaging) {
    Serial << "Imaging continuously." << ENDL;
  }
  else
  {
    Serial << F("Imaging when pin ") << triggerPin << F(" is pulled down only") << ENDL;
  }
  initOK = true;
}

void loop()
{
  if (!initOK) return;
  if (!continuousImaging && (digitalRead(triggerPin) == HIGH)) {
    Serial << F("*** Waiting for pin #") << triggerPin << F(" to go LOW...") ;
    while (digitalRead(triggerPin) == HIGH) {
      delay(200);
    }
    Serial << ENDL;
  }

  /* Serial << counter++ << F(" About to take one image in each possible size...");
    delay(1000);
    Serial << F("3 ");
    delay(1000);
    Serial << F("2 ");
    delay(1000);
    Serial << F("1 ");
    delay(1000);
    Serial << ENDL << "Capturing from timestamp " << millis() << ENDL;
  */

  /*for (uint8_t imgSize = OV2640_160x120; imgSize <= OV2640_1600x1200; imgSize++)

  */

  for (uint8_t imgSize = smallestSize; imgSize <= largestSize; imgSize+=imgSizeIncrement)
  {
    for (uint8_t lightMode = firstLightMode ; lightMode <= lastLightMode; lightMode++) {
      for (uint8_t brightness = Brightness4 ; brightness <= Brightness_4 ; brightness++) {
        unsigned long ts = millis();
        Serial << ts << ": imgSize= " << imgSize << ", lightMode=" << lightMode << ", brightness=" << brightness << ENDL;
        img.setImageSize(imgSize);
        img.setLightMode(Sunny);
        img.setBrightness(brightness);
        bool result = img.image(ts);
        if (!result)
        {
          Serial << F("Error :( ") << ENDL;
        }
      } // brightness
    } // for lightMode
  } // size

  if (continuousImaging) {
    Serial << "---------" << ENDL << "Image series over. Waiting for 10 secs)." << ENDL;
    delay(10000);
  }

}
