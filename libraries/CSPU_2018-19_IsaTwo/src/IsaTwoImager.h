/*
    IsaTwoImager.h
*/

#include "ArduCAM.h" 
#include "SdFat.h"

/**
    @brief The class used to interface the ArduCAM OV2640 camera and SD card.

	Usage:
	@code
	    IsaTwoImager img(SD_CS);
		// in setup():
		img.begin(CameraCS, OV2640_1024x768);

		// as often as required:
		img.setImageSize(OV2640_1024x768);
		img.image(currentTimestamp);
		(...)
	@endcode

    Design note
    -----------
    This class takes care of configuring the SPI bus for each slave whenever
    initiating communication, ensuring each slave is addressed with the right
    SPI settings (clock speed, byte order, SPI mode) whatever the requirement
    set for a previous slave.
    This is not only good practice, but is mandatory to support both the Uno
    and Feather M0 Express boards, otherwise the Feather would exceed the
    capacities of the OV2640 (limited to 8Mhz, while the Feather can go up
    to 12MHz).

*/
class IsaTwoImager
{
  public :
    /**
     * @param cameraChipSelect The number of the pin used as CS for the camera.
     * @param SD_chipSelect, the number of the pin used as CS for the camera.
     * @param cameraSPI_Speed The SPI speed in Hz, to used with the camera. Maximum speed for
     * 						  OV2640 is 8Mhz according to spec, but this speed seems very
     * 						  difficult to achieve (short wires etc...)
     */
    IsaTwoImager(byte cameraChipSelect, uint32_t cameraSPI_Speed=4000000);

    /** 
     *  Call this once before using the object
     *  @param imageSize use one of the constants defined in ArduCAM.h:  OV2640_160x120,
     *  				 OV2640_176x144, OV2640_320x240, OV2640_352x288, OV2640_640x480,
     *  				 OV2640_800x600, OV2640_1024x768, OV2640_1280x1024 or
     *  				 OV2640_1600x1200
     */
    bool begin(byte SD_chipSelect, uint8_t imageSize=OV2640_1600x1200);

    /** 
     *  Capture one image and save to SD Card
     *  @param timestamp The timestamp to use to name the image file
     *  @return true if image succesfully saved to file.
     */
    bool image(unsigned long timestamp);
	void setImageSize(uint8_t imageSize);
  void setLightMode(uint8_t mode) { myCAM.OV2640_set_Light_Mode(mode);};
  void setBrightness(uint8_t brightness) { myCAM.OV2640_set_Brightness(brightness);};
  protected:
    bool captureImage();
    void buildFileName(char* buffer,unsigned long timestamp);
    void buildDirName(const char* fourCharPrefix, const unsigned int number);
    bool saveImageToFile(const char* fileName);
    bool checkFileCreation();
    bool checkSPI_Bus();
	bool checkCamera();
	File openSD_File(const char* fileName);
	void blinkBuiltInLED();

  private:
    ArduCAM myCAM; /**< The camera driver */
    SdFat 	mySD;  /**< The SdFat object, private to the imager*/
    SPISettings	spiCamSettings; 	/**<  The SPI settings used for interacting with the camera. This is used to
    									  configure the bus everytime it is used with the camera. The SdFat
    									  library takes care of configuring it everytime the SD card is accessed. */
    String dirName;  /**< The name of the directory in which image files are stored. */
};
