/*
    ClockSynchronizer.h
*/

#include "Arduino.h"

#define DBG_IRQ 1   // Define to grant public access to member syncReceived (debugging only)

/** @ingroup IsaTwoCSPU
 *  The ClockSynchronizer_Slave and Master classes implement communication between to boards for clock synchronization.
 *  It makes the value of the master clock available on the slave.
    It could theoretically be used to synchronise more than 1 slave (although this feature is untested).

    @remark: A board can be a master and a slave on the I2C bus (see http://www.gammon.com.au/i2c), so the slave
             can communicate with other devices on the bus.
             Nevertheless, it can currently not communicate with master for anything else than clock
             synchronisation (see "limitations", below).

    @par Implementation notes
    - This class only static methods (there is no point in instanciating this class).
    - Accuracy is limited by the total transmission delay (assuming no bus latency) is at least:
        Address: 7 bits + Ack = 8 bits
        Data: 4 x (8 bits + Ack) = 36 bits
        Total: 44 bits, ie. at 400 kHz: 44/400000 approx 0,1 msec. )
      It is neglected.   
      Adding the bus latency + interrupt latency, we expected an accuracy of about 10 ms. 

    @par Typical usage:
    Master:
      @code
      // in setup
      Wire.begin(); (possibly with address)
      // anywhere at anytime:
      bool result = ClockSynchronizer_Master::setMasterClock(slaveAddress, 3); // update clock offset. 
      if (!result) {
          // Slave could not be reached: report error
      }
      (...) ideally, call regularly to avoid drift.
      @endcode
    Slave:
      @code
      // in setup
      Wire.begin(slaveAddress);
      result=ClockSynchronizer_slave::begin();
       
      // anywhere at anytime:
      unsigned long masterClock;
      bool result=ClockSynchronizer_Slave::getMasterClock(masterClock);
      if (!result) { 
          // no synchronisation info available: masterClock was set to slave clock
          // as fallback. Possibly report?
      }
      
      @endcode

    @par current limitations
    - It is currently assumed that the I2C communication between the 2 boards is exclusively used
      for clock synchronisation. If any other communication must occur, this class must be
      completed, to discriminate between synchronization communication and any other communication.
*/
class ClockSynchronizer_Master {
  public:
    /** Method used by the master to transmit its clock and synchronize
        it on the imaging slave board. This object stores an offset between the
        slave and master clock, in order to be able to know the master clock
        at any time.
        The slave board can access the master clock by calling getMasterclock();
        @param slaveAddress The I2C address of the slave to synchronize
        @param maxDurationInSec The maximum duration for retrying the I2C transmission,
                                if the slave is not responding (0 = no timeout).
        @return true is transfer succeeded, false otherwise.
        @pre The Wire library must have been initialized.
        @pre The slave board must have called the registerAsSlave() method. If it
             did not happen when the method is called, the I2C transmission will
             be retried every 10 ms for maxDurationInSec seconds.
    */
    static bool setMasterClock(byte slaveAddress, byte maxDurationInSec=0);
};

/** See class ClockSynchronizer_Master for a complete documentation. */
class ClockSynchronizer_Slave {
  public:
    /** This is the method to be called once by the slave in its setup(). It transparently registers
        the interrupt handler required to receive the synchronisation messages from the master.
        This method can be called more than once, e.g. when switching from I2C master to
        I2C slave role.
        @return true if successful, false otherwise.
        @pre The Wire library must have been initialized providing a slave address.
    */
    static void begin();

    /** Method called by the slave to access the master's current clock value.
        The value is calculated from the known offset (which is 0 if no synchronisation occured.
        If no synchronization occured, an error is reported.
        @return true if the returned value is the master's clock, false
                otherwise (in this case, masterClock is set to the slave's clock).
    */
    static bool getMasterClock(unsigned long &masterClock);
    
    /** Check whether synchronization information was received from the master */
    static bool isSynchronized() {return syncReceived; } ;

    /** Obtain the advance of the master clock, in msec (for debugging) */
    static unsigned long getMasterAdvance() { return masterAdvance;};
    
  private:
    /** The IRQ handler called by interrupt service routine when incoming data arrives
        @param howMany The number of bytes received.
    */
    static void receiveSyncEvent(int howMany);

    static long masterAdvance; /**< The offset between master and slave clock (positive when master is in advance).
                              value MIN_LONG denotes an unsynchronized state. */
#ifdef DBG_IRQ  // for debugging allow to read and reset this flag.
  public:
#endif
    static bool syncReceived;
} ;
