/* 
 *  IsaTwoBMP280_Client.h
 */
#pragma once

#include "IsaTwoRecord.h"
#include "BMP_Client.h"
#include "Adafruit_BMP280.h"
#include "Adafruit_Sensor.h"
#include "Wire.h"
/** @ingroup IsaTwoCSPU
    xxxxxxx
*/
class IsaTwoBMP280_Client : public BMP_Client {
  public:
  
    /** @brief Read sensor data and populate data record.
     *  @param record The data record to populate (fields temparature, altitude and pressure).
     *  @return True if reading was successful.
     */
    bool readData(IsaTwoRecord& record);
};
