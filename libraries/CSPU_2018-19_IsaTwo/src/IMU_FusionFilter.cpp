/*
 * IMU_FusionFilter.cpp
 */

#include "IMU_FusionFilter.h"

#define DEBUG_CSPU
#ifndef DBG_DIAGNOSTIC
#  define DBG_DIAGNOSTIC 	0  // Do not overwrite setting from global config file, if any.
#endif
#define DBG_CALIBRATION 0   // Define to 1 to output the calibrated IMU readings at each update.
#define DBG_RAW 		0   // Define to 1 to output the uncalibrated IMU readings at each update.
#define DBG_FUSION_FILTER_INIT 		1
#include "DebugCSPU.h"

#include "elapsedMillis.h"
#ifdef USE_NXP_PRECISION_IMU
#include "IMU_NXP_CalibrationData.h"
#else
#include "IMU_LSM_CalibrationData.h"
#endif

// Debugging utilities
//define FORCE_GYROS_TO_ZERO 	// Define for forcing the gyros to 0 and check how static situations are handled.
								// This SHOULD NOT be defined in the operational version
#define USE_MAGNETOMETER_DATA 	// Define to use the regular algorithm using the 9 readings. Otherwise only gyroscopes and accelerometers are used.
								// This SHOULD be defined in the operation version.

#ifdef USE_NXP_PRECISION_IMU
IMU_FusionFilter::IMU_FusionFilter(): accelmag(0x8700A, 0x8700B), gyro(0x0021002C), updatePeriodInMsec(0) {}
#else
IMU_FusionFilter::IMU_FusionFilter() : updatePeriodInMsec(0){}
#endif

bool IMU_FusionFilter::begin(unsigned int theUpdatePeriodInMsec){
	updatePeriodInMsec=theUpdatePeriodInMsec;
	float frequency=(float) (1000.0F/updatePeriodInMsec);
#ifdef USE_NXP_PRECISION_IMU
	DPRINTS(DBG_FUSION_FILTER_INIT, "Using NXP Precision IMU with ID=");
	DPRINTLN(DBG_FUSION_FILTER_INIT, IMU_REFERENCE);
	if (!accelmag.begin(ACCEL_RANGE_2G)) {
		DPRINTSLN(DBG_DIAGNOSTIC, "Error initializing NXP accel/mag.");
		return false;
	}
	if (!gyro.begin(GYRO_RANGE_250DPS))  {
		DPRINTSLN(DBG_DIAGNOSTIC, "Error initializing NXP gyro.");
		return false;
	}
#else
	DPRINTS(DBG_FUSION_FILTER_INIT, "Using LSM9DS0 IMU with ID=");
	DPRINTLN(DBG_FUSION_FILTER_INIT, IMU_REFERENCE);
	if (!lsm.begin()) {
		DPRINTSLN(DBG_DIAGNOSTIC, "Error initializing IMU module");
		//return false;
	}
	// configure lsm
	lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_2G);
	lsm.setupMag(lsm.LSM9DS0_MAGGAIN_2GAUSS);
	lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_245DPS);
#endif

#ifndef USE_MAGNETOMETER_DATA
	Serial << "*** WARNING: Magnetometer data not used! ***" << ENDL;
#endif

#ifdef FORCE_GYROS_TO_ZERO
	Serial << "*** WARNING: Gyro are forced to 0 ***" << ENDL;
#endif

#if AHRS_FUSION_ALGORITHM==AHRS_FUSION_MADGWICK
	DPRINTSLN(DBG_DIAGNOSTIC, "Using Madgwick fusion filter");
#else
	DPRINTSLN(DBG_DIAGNOSTIC, "Using Mahony fusion filter");
#endif

	filter.begin(frequency);
	DPRINTS(DBG_FUSION_FILTER_INIT,"Fusion filter initialized at ");
	DPRINT(DBG_FUSION_FILTER_INIT,frequency );
	DPRINTSLN(DBG_FUSION_FILTER_INIT," Hz");
	elapsedSinceUpdate=0;
	return true;
}


void IMU_FusionFilter::update() {
	if (elapsedSinceUpdate != updatePeriodInMsec) {
		DPRINTS(  DBG_DIAGNOSTIC, "*** Error: Update period (msec)=");
		DPRINT(   DBG_DIAGNOSTIC, updatePeriodInMsec);
		DPRINTS(  DBG_DIAGNOSTIC, ", elapsed=");
		DPRINTLN( DBG_DIAGNOSTIC, elapsedSinceUpdate);
	}
	elapsedSinceUpdate=0;
#ifdef USE_NXP_PRECISION_IMU
	sensors_event_t unusedEvent, mag;

	/* Get a new sensor event */
	accelmag.getEvent(&unusedEvent, &mag);
	gyro.getEvent(&unusedEvent);

	/* (mag data is in uTesla) */
	calibrateMag(mag.magnetic.x,mag.magnetic.y,mag.magnetic.z, calibratedMag);
	calibrateGyro(gyro.raw.x,gyro.raw.y,gyro.raw.z, calibratedGyro);
	calibrateAccel(accelmag.accel_raw.x,accelmag.accel_raw.y,accelmag.accel_raw.z, calibratedAccel);
#else
	// Read info from LSM9DS0 IMU.
	lsm.read();
	constexpr float MicroT_PerLSB=LSM9DS0_MAG_MGAUSS_2GAUSS/10.0f;
	// Unit conversion is wrong in the library, so we do it ourselves. Calibration is
	// applied on figures in µTesla
	calibrateMag(lsm.magData.x*MicroT_PerLSB,lsm.magData.y*MicroT_PerLSB,lsm.magData.z*MicroT_PerLSB, calibratedMag);
	calibrateGyro(lsm.gyroData.x,lsm.gyroData.y,lsm.gyroData.z, calibratedGyro);
	calibrateAccel(lsm.accelData.x,lsm.accelData.y,lsm.accelData.z, calibratedAccel);

	// Warning: LSM9DS0 magnetometers use a left-handed referential
	// so we change the sign of the z component.
	calibratedMag[2]=-calibratedMag[2];
#endif

#ifdef FORCE_GYROS_TO_ZERO
	for (int i = 0 ; i< 3; i++) { calibratedGyro[i]=0.0f; }
#endif
#if (DBG_RAW !=0)
	Serial << "Raw Mag (G): " << mag.magnetic.x << " / " << mag.magnetic.y << " / " <<mag.magnetic.z << ENDL;
#endif
#if (DBG_CALIBRATION!=0)
	Serial << "Accel (m/s^2): ";
	for (int i = 0; i< 3 ; i++ ) {
		Serial << calibratedAccel[i] << " / ";
	}
	Serial << "  Gyro (dps): ";
	for (int i = 0; i< 3 ; i++ ) {
		Serial << calibratedGyro[i] << " / ";
	}
	Serial << "  Mag (uT): "  ;
	for (int i = 0; i< 3 ; i++ ) {
		Serial << calibratedMag[i] << " / ";
	}
	Serial << ENDL;
#endif

#ifdef USE_MAGNETOMETER_DATA
	filter.update(calibratedGyro[0], calibratedGyro[1], calibratedGyro[2],
			calibratedAccel[0], calibratedAccel[1], calibratedAccel[2],
			calibratedMag[0], calibratedMag[1], calibratedMag[2]);
#else
	filter.updateIMU(calibratedGyro[0], calibratedGyro[1], calibratedGyro[2],
			calibratedAccel[0], calibratedAccel[1], calibratedAccel[2]);
#endif
}

void IMU_FusionFilter::calibrateMag(const float& magX, const float& magY, const float& magZ, float calibrated[3]) {
	float unCalibrated[3];
	unCalibrated[0] = magX - IMU_Calib.magOffset[0];
	unCalibrated[1] = magY - IMU_Calib.magOffset[1];
	unCalibrated[2] = magZ - IMU_Calib.magOffset[2];

	for(int i = 0; i<3; i++){
		calibrated[i]=0.0f;
		for(int j = 0; j<3; j++){
			calibrated[i] += IMU_Calib.magTransformationMatrix[i][j] * unCalibrated[j];
		}
	}
}

void IMU_FusionFilter::calibrateGyro(const float& gyroX, const float& gyroY, const float& gyroZ, float calibrated[3]) {
	calibrated[0] = (gyroX - IMU_Calib.gyroZeroRate[0]) * IMU_Calib.gyroResolution[0];
	calibrated[1] = (gyroY - IMU_Calib.gyroZeroRate[1]) * IMU_Calib.gyroResolution[1];
	calibrated[2] = (gyroZ - IMU_Calib.gyroZeroRate[2]) * IMU_Calib.gyroResolution[2];
}

void IMU_FusionFilter::calibrateAccel(const float& accelX, const float& accelY, const float& accelZ, float calibrated[3]) {
	calibrated[0] = (accelX - IMU_Calib.accelZeroG[0]) * IMU_Calib.accelResolution[0];
	calibrated[1] = (accelY - IMU_Calib.accelZeroG[1]) * IMU_Calib.accelResolution[1];
	calibrated[2] = (accelZ - IMU_Calib.accelZeroG[2]) * IMU_Calib.accelResolution[2];
}
