			***********************************************
			* ABOUT THIS VERSION OF THE RadioHead LIBRARY *
			***********************************************

This is a fork of the RadioHead library provided by Adafruit.
It is used in project Ethera to manage the RFM69 module. 

Originally, we used the original RadioHead library, which works fine,
but has some incompatibility with the SdFat library. As a consequence,
accessing the on-board SD-Card of the Feather Adalogger board, prevented
any reception by the RFM69. 

This problem is described e.g. in https://github.com/greiman/SdFat/issues/92,
which includes a workaround which does not work. 

The RadioHead library uses the SPI driver during interrupts, and apparently does
not do it in a safe way (although there is one, using SPItransaction). 

		/// The RH_RF69 driver uses interrupts to react to events in the RFM module,
		/// such as the reception of a new packet, or the completion of transmission
		/// of a packet. The RH_RF69 driver interrupt service routine reads status from
		/// and writes data to the the RFM module via the SPI interface. It is very
		/// important therefore, that if you are using the RH_RF95 driver with another
		/// SPI based deviced, that you disable interrupts while you transfer data to
		/// and from that other device. Use cli() to disable interrupts and sei() to
		/// reenable them.
		
* Applying a patch in the SdFat library (add noInterrupts(); to the activate() function 
  and interrupts(); to the deactivate() function in SdSpiDriver.h)  does not fix the 
  issue.
* Applying the same strategy at a higher lever (around the SD-access method) does
  not fix the issue. 
  
This version of the library seems to adequately fix it. 

NB: The sources have been moved to a /src folder and a property file added, to 
    enable .a linkage. 
    												 (A. Baudhuin, Feb. 2023). 