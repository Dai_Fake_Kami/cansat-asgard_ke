#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "GMiniSecondaryMissionController.h"
GMiniRecord record;
GMiniSecondaryMissionController ctrl;
constexpr bool detailDBG = false;
bool takeoffHappened = ctrl.takeoffDetected();
int numErrors = 0;

void testConstantValue() {
  record.clear();
  record.descentVelocity = (VelocityThresholdForTakeoff / 2.0);

  Serial << "Testing constant velocity = " << record.descentVelocity << " , num samples = " << ctrl.NumberOfVelocitySamples  << ENDL;
  Serial << " sending " << ctrl.NumberOfVelocitySamples << " records" << ENDL;
  for (int i = 1  ; i <= 2 * ctrl.NumberOfVelocitySamples ; i++) {
    if (detailDBG) Serial << "Average Velocity before record #" << i << ":" << ctrl.getAverageVelocity() << ENDL;
    ctrl.manageSecondaryMission(record);
    float avg = ctrl.getAverageVelocity();
    if ( i < ctrl.NumberOfVelocitySamples) {
      if (avg != ctrl.InvalidVelocity) {
        Serial << "ERROR : i =" << i << ", velocity should be invalid" << ENDL;
        numErrors++;
      }
    }
    else {
      if (fabs( ctrl.getAverageVelocity() - record.descentVelocity) > 0.001)   {
        Serial << "Unexpected Value =" << record.descentVelocity << ENDL;
        numErrors++;
      }
    }
  }
  if ( fabs( ctrl.getAverageVelocity() - record.descentVelocity) > 0.001) {
    Serial << ctrl.getAverageVelocity() << ENDL;
    Serial << "Unexpected Value =" << record.descentVelocity << ENDL;
    numErrors++;
  }
  else {
    Serial << "Averaging succeeded --> Current Value =" << record.descentVelocity << ENDL;
    if (takeoffHappened == true) {
      Serial << "takeoff has been detected" << ENDL;
    }
  }
  Serial << "---------------" << ENDL;
}
//

void testAvg(float firstVelocityValue, float increment) {
  Serial << ENDL << "Test average with initial value " << firstVelocityValue << ", increment = " << increment << ENDL;
  float avg = (2 * firstVelocityValue + ctrl.NumberOfVelocitySamples * increment) / 2;
  record.timestamp = 7;
  record.descentVelocity = firstVelocityValue;
  for (int i = 0; i < ctrl.NumberOfVelocitySamples; i++) {
    ctrl.manageSecondaryMission(record);
    record.descentVelocity += increment;
    record.timestamp += CansatAcquisitionPeriod;
  }
  if ((fabs( ctrl.getAverageVelocity() - ctrl.getAverageVelocity()) > 0.001)) {
    Serial << "UnExpected Value =" << ctrl.getAverageVelocity() << ENDL;
    Serial << "Expected Value =" << avg << ENDL;
    numErrors++;
  }
  else {
    Serial << "Test Average is okay" << ENDL;
    if (takeoffHappened == true) {
      Serial << "takeoff has been detected" << ENDL;
    }
  }
  Serial << "------------------" << ENDL << ENDL;
}


void testavgAlternatingValues(float value1, float value2) {
  Serial << ENDL << "Test Alternating values with as first value: " << value1 << " and as second value: " << value2 << ENDL;
  float avg = (value1 + value2) / 2;
  for (int i = 0; i < 10 * ctrl.NumberOfVelocitySamples; i++) {
    if ((i % 2) == 0) {
      record.descentVelocity = value1;
    }
    else {
      record.descentVelocity = value2;
    }
  }
  if ((fabs( ctrl.getAverageVelocity() - ctrl.getAverageVelocity()) > 0.001)) {
    Serial << "UnExpected Value =" << ctrl.getAverageVelocity() << ENDL;
    Serial << "Expected Value =" << avg << ENDL;
    numErrors++;
  }
  else {
    Serial << "Test Alternating Value is okay" << ENDL;
    if (takeoffHappened == true) {
      Serial << "takeoff has been detected" << ENDL;
    }
  }
  Serial << "-------------------------------------------" << ENDL;
}

void testavgDelayedTimestamp() {
  record.timestamp = 11;
  record.descentVelocity = VelocityThresholdForTakeoff / 2.0;
  for (int i = 1  ; i <= 2 * ctrl.NumberOfVelocitySamples ; i++) {
    if (detailDBG) Serial << "Average Velocity before record #" << i << ":" << ctrl.getAverageVelocity() << ENDL;
    ctrl.manageSecondaryMission(record);
    record.timestamp += CansatAcquisitionPeriod;
  }

  if (ctrl.getAverageVelocity() == ctrl.InvalidVelocity) {
    Serial << "ERROR Velocity should not be invalid" << ENDL;
    numErrors++;
  }
  else {
    Serial << "Part 1 of Average test with a delayed timestamp is OK : INVALID AVERAGE VELOCITY RECEIVED" << ENDL;

    if (takeoffHappened == true) {
      Serial << "takeoff has been detected" << ENDL;
    }
  }
  record.timestamp += 4 * CansatAcquisitionPeriod;
  ctrl.manageSecondaryMission(record);
  if (ctrl.getAverageVelocity() != ctrl.InvalidVelocity) {
    Serial << "ERROR Velocity should be invalid after too big gap between 2 timestamps " << ENDL;
    numErrors++;
  }
  else {
    Serial << " Part 2 of Average test with a delayed timestamp is OK : INVALID VELOCITY AVERAGE RECEIVED" << ENDL;

    if (takeoffHappened == true) {
      Serial << "takeoff has been detected" << ENDL;
    }
  }
  for (int i = 1  ; i <= 2 * ctrl.NumberOfVelocitySamples ; i++) {
    if (detailDBG) Serial << "Average Velocity before record #" << i << ":" << ctrl.getAverageVelocity() << ENDL;
    ctrl.manageSecondaryMission(record);
    record.timestamp += CansatAcquisitionPeriod;
  }

  if (ctrl.getAverageVelocity() == ctrl.InvalidVelocity) {
    Serial << "ERROR Velocity should not be invalid" << ENDL;
    numErrors++;
  }
  else {
    Serial << "Part 1 of Average test with a delayed timestamp is OK :VALID AVERAGE VELOCITY  RECEIVED " << ":" << ctrl.getAverageVelocity() << ENDL;

    if (takeoffHappened == true) {
      Serial << "takeoff has been detected" << ENDL;
    }
  }
  record.timestamp -= 71;
  ctrl.manageSecondaryMission(record);
  if (ctrl.getAverageVelocity() != ctrl.InvalidVelocity) {
    Serial << "ERROR Velocity should be invalid after a too old timestamp" << ENDL;
    numErrors++;
  }
  else {
    Serial << "TEST 4 OK : INVALID AVERAGE VELOCITY RECEIVED" << ENDL;
    if (takeoffHappened == true) {
      Serial << "takeoff has been detected" << ENDL;
    }
  }
}

void testEjection(GMiniRecord& record) {
  Serial << ENDL << "Ejection Test started" << ENDL;
  record.descentVelocity = 1;
  record.timestamp = 1000;
  bool ejected = false;
  bool takeoffAcknowledged = false;
  while (!ejected) {
    ctrl.manageSecondaryMission(record);
    record.descentVelocity -= 0.1;
    // Serial << "current Velocity: " << record.descentVelocity << ENDL;
    record.timestamp += 1;
    // Serial << "current timestamp: " << record.timestamp << ENDL;
    if (ctrl.takeoffDetected() && (!takeoffAcknowledged)) {
      Serial << "Takeoff at " << record.timestamp << ", avg: " << ctrl.getAverageVelocity() << ", Threshold: " << VelocityThresholdForTakeoff << ENDL;
      takeoffAcknowledged = true;
    }
    if (record.subCanEjected  && (!ejected))  {
      Serial << "Ejecting at " << record.timestamp << " Ejection Delay " << MaxDelayFromTakeoffToEjection / 1000.0 << "s" << ENDL;
      ejected = true;
    }
  }
  if (record.subCanEjected == true) {
    Serial << "TEST OK : SUBCAN GOT SUCCESSFULLY EJECTED" << ENDL;
  }
  else {
    Serial << "ERROR : NO CAN GOT EJECTED" << ENDL;
    numErrors++;
  }
  Serial << "-----------------" << ENDL;
}

void testInvalidVelocity(GMiniRecord& record) {
  Serial << ENDL << "Invalid Velocity Test started" << ENDL;
  record.descentVelocity = BMP_Client::InvalidDescentVelocity;
  ctrl.manageSecondaryMission(record);
  if (ctrl.getAverageVelocity() == ctrl.InvalidVelocity) {
    Serial << " TEST OK :Velocity got succesfully cleared current value= " << ctrl.getAverageVelocity()  << ENDL;
  }
  else {
    Serial << " ERROR :Velocity isn't cleared current value= " << ctrl.getAverageVelocity() << ENDL;
    numErrors++;
  }
}

void setup() {
  DINIT(115200);
  if (!ctrl.begin()) {
    Serial << "Unexpected error" << ENDL;
    exit (0);
  }

  testConstantValue();
  testAvg(0, 1);
  testAvg(2, 10);
  testAvg(VelocityThresholdForTakeoff / 2, 13);
  testAvg(VelocityThresholdForTakeoff / 4, 0.13);
  testAvg(-VelocityThresholdForTakeoff, -0.2);
  testavgAlternatingValues(VelocityThresholdForTakeoff / 9, 2);
  testavgAlternatingValues(VelocityThresholdForTakeoff / 3 , 11);
  testavgAlternatingValues(-3.14, 2.4);
  testavgAlternatingValues(VelocityThresholdForTakeoff / 10, -2.94);
  testavgAlternatingValues(-VelocityThresholdForTakeoff / 4 , -2.91);
  testavgDelayedTimestamp();
  testAvg(VelocityThresholdForTakeoff + 7, 6);
  testEjection(record);
  testInvalidVelocity(record);
  Serial << "Number of errors: " << numErrors << ENDL;
  exit (0);
}

void loop() {


}
