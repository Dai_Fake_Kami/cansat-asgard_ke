/*
   GMiniCanCommander.cpp
*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "CansatInterface.h"
#include "GMiniCanCommander.h"
#include "CansatXBeeClient.h"

#ifdef GMINI_SUPPORT_LATCH_COMMANDS
#include "GMiniMainAcquisitionProcess.h"
#endif

#define DBG_READ_MSG 1
#define DBG_DIAGNOSTIC 1

GMiniCanCommander::GMiniCanCommander(unsigned long int theTimeOut) :
  RT_CanCommander(theTimeOut) {
}

#ifdef RF_ACTIVATE_API_MODE
void GMiniCanCommander::begin(CansatXBeeClient &xbeeClient, SdFat* theSd,
                              CansatAcquisitionProcess* theProcess) {
  RT_CanCommander::begin(xbeeClient, theSd, theProcess);
#ifdef GMINI_SUPPORT_LATCH_COMMANDS
  GMiniMainAcquisitionProcess* gminiProcess = (GMiniMainAcquisitionProcess*) theProcess;
latch = gminiProcess->getLatch();
#endif
}
#else
void GMiniCanCommander::begin (Stream& theResponseStream, SdFat* theSd, CansatAcquisitionProcess* theProcess) {
  RT_CanCommander::begin(theResponseStream, theSd, theProcess);
#ifdef SUPPORT_LATCH_COMMANDS
  GMiniMainAcquisitionProcess* gminiProcess = (GMiniMainAcquisitionProcess*) theProcess;
  latch = gminiProcess->getLatch();
#endif
}
#endif

#ifdef GMINI_SUPPORT_LATCH_COMMANDS
void GMiniCanCommander::processReq_GetLatchStatus(char* &nextCharAddress) {

  DPRINTSLN(DBG_READ_MSG, "GetLatchStatus");
 Latch::LatchStatus status = latch->getStatus();
  RF_OPEN_CMD_RESPONSE(RF_Stream);
  *RF_Stream << (int)CansatCmdResponseType::Latch_Status << "," <<  (int)status
             << "," ;
  switch (status) {
    case Latch::LatchStatus::Neutral:
      *RF_Stream << "Neutral";
      break;
    case Latch::LatchStatus::Locked:
      *RF_Stream << "Locked";
      break;
    case Latch::LatchStatus::Unlocked:
      *RF_Stream << "Unlocked";
      break;
    case Latch::LatchStatus::Undefined:
      *RF_Stream << "Undefined";
      break;
    default:
      *RF_Stream << "Unexpected value";
      break;
  }
  RF_CLOSE_CMD_RESPONSE(RF_Stream);
}

void GMiniCanCommander::processReq_Latch_Lock(char* &nextCharAdress) {
  DPRINTSLN(DBG_READ_MSG, "Latch_Lock");
  latch->lock();
  RF_OPEN_CMD_RESPONSE(RF_Stream);
  *RF_Stream << (int)CansatCmdResponseType::Latch_Status << "," << (int)Latch::LatchStatus::Locked << ",Locked" ;
  RF_CLOSE_CMD_RESPONSE(RF_Stream);
}

void GMiniCanCommander::processReq_Latch_Unlock(char* &nextCharAdress) {
  DPRINTSLN(DBG_READ_MSG, "Latch_Unlock");
  latch->unlock();
  RF_OPEN_CMD_RESPONSE(RF_Stream);
  *RF_Stream << (int)CansatCmdResponseType::Latch_Status << "," << (int)Latch::LatchStatus::Unlocked << ",Unlocked" ;
  RF_CLOSE_CMD_RESPONSE(RF_Stream);
}

void GMiniCanCommander::processReq_Latch_SetNeutral(char* &nextCharAdress) {
  DPRINTSLN(DBG_READ_MSG, "Latch_SetNeutral");
  latch->neutral();
  RF_OPEN_CMD_RESPONSE(RF_Stream);
  *RF_Stream << (int)CansatCmdResponseType::Latch_Status << "," << (int)Latch::LatchStatus::Neutral << ",Neutral";
  RF_CLOSE_CMD_RESPONSE(RF_Stream);
}
#endif

bool GMiniCanCommander::processProjectCommand(CansatCmdRequestType requestType,
    char* cmd) { 
      DPRINTS(DBG_READ_MSG, "process GMiniProjectCommand ");
  bool result = true; 

  switch (requestType) {
#ifdef GMINI_SUPPORT_LATCH_COMMANDS
    case CansatCmdRequestType::Latch_GetStatus:
      processReq_GetLatchStatus(cmd);
      break;

    case CansatCmdRequestType::Latch_Lock:
      processReq_Latch_Lock(cmd);
      break;

    case CansatCmdRequestType::Latch_Unlock:
      processReq_Latch_Unlock(cmd);
      break;

    case CansatCmdRequestType::Latch_SetNeutral:
      processReq_Latch_SetNeutral(cmd);
      break;
#endif

    default:
      // Do not report error here: this is done by the superclass.
      result = false;
  } // Switch
  return result;
}
