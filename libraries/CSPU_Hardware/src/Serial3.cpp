/*
 * Serial3.cpp
 */

#ifdef __SAMD21G18A__
#ifndef ARDUINO_SAMD_FEATHER_M0_EXPRESS
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "Serial3.h"
#include "wiring_private.h"

SercomSerial2 Serial3;

SercomSerial2::SercomSerial2()
  : Uart(&sercom1,  3, 4 ,  SERCOM_RX_PAD_1 , UART_TX_PAD_0), rx(3), tx(4)
 {
    pinPeripheral(rx, PIO_SERCOM_ALT);
    pinPeripheral(tx, PIO_SERCOM_ALT);
 }

// Interrupt request handler
void SERCOM2_Handler()
{
  Serial3.IrqHandler();
}
#endif
#endif
