/*
   HBridgeMotor.cpp
*/

#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG_BACKWARDS 0
#define DBG_FORWARD 0
#define DBG_BEGIN 0
#include "HBridgeMotor.h"

bool HBridgeMotor::begin(uint8_t pwmPin, uint8_t forwardPin, uint8_t reversePin) {
  HBridgeMotorPWM_Pin = pwmPin;
  HBridgeMotorForward_Pin = forwardPin;
  HBridgeMotorReverse_Pin = reversePin;
  DPRINTS(DBG_BEGIN, "HBridgeMotorPWM_Pin=");
  DPRINTLN(DBG_BEGIN, HBridgeMotorPWM_Pin );
  DPRINTS(DBG_BEGIN, "HBridgeMotorForward_Pin=");
  DPRINTLN(DBG_BEGIN, HBridgeMotorForward_Pin );
  DPRINTS(DBG_BEGIN, "HBridgeMotorReverse_Pin=" );
  DPRINTLN(DBG_BEGIN, HBridgeMotorReverse_Pin );
  pinMode(HBridgeMotorForward_Pin, OUTPUT);
  pinMode(HBridgeMotorReverse_Pin, OUTPUT);
  pinMode(HBridgeMotorPWM_Pin, OUTPUT);
  return true;
}


void HBridgeMotor::TurnForward(uint32_t duration, uint8_t power) {
  DPRINTSLN(DBG_FORWARD, "Activation of the H-bridge");
  digitalWrite(HBridgeMotorReverse_Pin, LOW);
  digitalWrite(HBridgeMotorForward_Pin, HIGH);
  analogWrite(HBridgeMotorPWM_Pin, power);
  DPRINTS(DBG_FORWARD, "Motor turns forward during ");
  DPRINT(DBG_FORWARD, duration );
  DPRINTSLN(DBG_FORWARD, " milliseconds.");
  delay(duration);
  analogWrite(HBridgeMotorPWM_Pin, 0);
  digitalWrite(HBridgeMotorForward_Pin, LOW);
  digitalWrite(HBridgeMotorReverse_Pin, LOW);
  DPRINTSLN(DBG_FORWARD, "Deactivation of the H-bridge");
}

void HBridgeMotor::TurnBackwards(uint32_t duration, uint8_t power) {
  DPRINTSLN(DBG_FORWARD, "Activation of the H-bridge");
  digitalWrite(HBridgeMotorForward_Pin, LOW);
  digitalWrite(HBridgeMotorReverse_Pin, HIGH);
  analogWrite(HBridgeMotorPWM_Pin, power);
  DPRINTS(DBG_FORWARD, "Motor turns backwards during ");
  DPRINT(DBG_FORWARD, duration );
  DPRINTSLN(DBG_FORWARD, " milliseconds.");
  delay(duration);
  analogWrite(HBridgeMotorPWM_Pin, 0);
  digitalWrite(HBridgeMotorForward_Pin, LOW);
  digitalWrite(HBridgeMotorReverse_Pin, LOW);
  DPRINTSLN(DBG_FORWARD, "Deactivation of the H-bridge");
}
