/*
   LinearStepper.cpp
*/
#define DEBUG_CSPU
#include "LinearStepper.h"
#include "CSPU_Debug.h"
#define DBG 0

bool LinearStepper::begin(uint8_t theDirectionPin, uint8_t theStepPin, uint8_t theSleepPin, uint32_t totalNumOfSteps) {
  DPRINTSLN(DBG, "Linearstepper::Begin");
  pinDir = theDirectionPin;
  pinStep = theStepPin;
  pinSleep = theSleepPin;
  currentStepPosition = 0;
  totalNumberOfSteps = totalNumOfSteps;

  pinMode( pinSleep, OUTPUT );
  pinMode( pinDir  , OUTPUT );
  pinMode( pinStep , OUTPUT );

  return true;
}

void LinearStepper::setPosition (uint8_t position) {
  DPRINTSLN(DBG, "Linearstepper::setPosition");
  uint32_t targetPosition = (totalNumberOfSteps * position) / 100;
  int delta = targetPosition - currentStepPosition;
  DPRINTS(DBG, "  target= ");
  DPRINTLN(DBG, targetPosition);
  DPRINTS(DBG, "  delta= ");
  DPRINTLN(DBG, delta);
  if (delta > 0) {
    setDirection(true);
    moveN_Steps(delta);
  }
  else {
    setDirection(false);
    moveN_Steps(-delta);
  }
  currentStepPosition = targetPosition;
}

void LinearStepper::setDirection (bool positive) {
  DPRINTSLN(DBG, "Linearstepper::setDirection");
  digitalWrite( pinDir, (positive ? LOW : HIGH));
}

void LinearStepper::moveN_Steps(long int numSteps) {
  DPRINTS(DBG, "Linearstepper::moveN_Steps numSteps=");
  DPRINTLN(DBG, numSteps);
  digitalWrite( pinSleep, HIGH);
  delay(100);
  for (int i = 1; i <= numSteps ; i++) {
    digitalWrite( pinStep, HIGH );
    delayMicroseconds( DurationHigh );
    digitalWrite( pinStep, LOW );
    delayMicroseconds( DurationLow );
  }
  digitalWrite( pinSleep, LOW);


}

void LinearStepper::resetPosition() {
  DPRINTSLN(DBG, "Linearstepper::resetPosition");
  setPosition(0);
}

void LinearStepper::forceToZero(uint32_t extraSteps) {

  DPRINTSLN(DBG, "Linearstepper::forceToZero");
  setPosition(0);
  setDirection(true);
  moveN_Steps(extraSteps);
}
