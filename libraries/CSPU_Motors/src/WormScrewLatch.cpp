/*
   WormScrewLatch.cpp
*/

#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG_LOCK 0
#define DBG_UNLOCK 0
#define DBG_BEGIN 0
#include "WormScrewLatch.h"

bool WormScrewLatch::begin(uint8_t pwmPin, uint8_t forwardPin, uint8_t reversePin, uint8_t power, uint32_t delayLock, uint32_t delayUnlock) {
  if (!HBridgeMotor::begin( pwmPin, forwardPin, reversePin)) {
    DPRINTS(DBG_DIAGNOSTIC, "error in beginning");
    return false;
  }
  screwLatchPower = power;
  screwLatchDelayLock = delayLock;
  screwLatchDelayUnlock = delayUnlock;
  DPRINTS(DBG_BEGIN, "screwLatchPower=");
  DPRINTLN(DBG_BEGIN, screwLatchPower );
  DPRINTS(DBG_BEGIN, "screwLatchDelayLock=");
  DPRINTLN(DBG_BEGIN, screwLatchDelayLock );
  DPRINTS(DBG_BEGIN, "screwLatchDelayUnlock=");
  DPRINTLN(DBG_BEGIN, screwLatchDelayUnlock );
  return true;
}

void WormScrewLatch::unlock() {
  TurnForward(screwLatchDelayUnlock, screwLatchPower);
  theStatus = LatchStatus::Unlocked;
}

void WormScrewLatch::lock() {
  TurnBackwards(screwLatchDelayLock, screwLatchPower);
  theStatus = LatchStatus::Locked;
}
