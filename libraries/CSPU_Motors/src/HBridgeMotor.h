/*
   HBridgeMotor.h
*/

#pragma once

#include "Arduino.h"

/** @ingroup CSPU_Motors
    @brief Interface class to control a DC motor power using a H-bridge.
    The class has been tested during project G-Mini with:
    - Motor TO BE COMPLETED
    - H-Bridge L923D.
*/
class HBridgeMotor {

  public :
    /** Method to call before any other method. Initializes whatever must be.
        @param pwmPin the PWM Pin number actives the H-Bridge.
        @param forwardPin the Forward Pin is used to turn forward.
        @param reversePin the Reverse Pin is used to turn backwards.
        @return true if initialization succeeded
    */
    bool begin(uint8_t pwmPin, uint8_t forwardPin, uint8_t reversePin);
    /** Run the motor in forward direction for a predefined duration and
        speed. This method is synchronous and return after the motor
        is stopped again.
        @param power defines the power to control the velocity of the motor
        			(between 0 and 255).
        @param duration The duration of the movement, in msec.
    */
    void TurnForward (uint32_t duration, uint8_t power); /**< Method to turn the motor forward during a defined time of milliseconds */
    /** Run the motor in reverse direction for a predefined duration and
        speed. This method is synchronous and return after the motor
        is stopped again.
        @param power defines the power to control the velocity of the motor
        			(between 0 and 255).
        @param duration The duration of the movement, in msec.
    */
    void TurnBackwards (uint32_t duration, uint8_t power); /**< Method to turn the motor backwards during a defined time of milliseconds */

  private :
  
    uint8_t HBridgeMotorPWM_Pin; /**< Number of the PWM (Enable1 pin of H-Bridge) */
    uint8_t HBridgeMotorForward_Pin; /**< Number of the pin connected to pin #2 (input1) of the H-bridge */
    uint8_t HBridgeMotorReverse_Pin; /**< Number of the pin connected to pin #7 (input2) of the H-bridge */

};
