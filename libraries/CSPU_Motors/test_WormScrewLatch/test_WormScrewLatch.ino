#define DEBUG_CSPU
#define USE_ASSERTIONS
#include "DebugCSPU.h"
#include "WormScrewLatch.h"
#include "GMiniConfig.h"

WormScrewLatch myScrew ;

void setup() {
  DINIT(115200);
  myScrew.begin(ScrewLatchPWM_Pin, ScrewLatchForward_Pin, ScrewLatchReverse_Pin, Power, DelayLock, DelayUnlock);
}

void loop() {
  Serial << "unlock"<< ENDL;
  myScrew.unlock();
  delay(2000);
  Serial << "lock"<< ENDL;
  myScrew.lock();
  delay(2000);
};
