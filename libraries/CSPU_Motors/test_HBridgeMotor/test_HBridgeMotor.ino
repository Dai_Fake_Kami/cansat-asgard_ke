/* 
 *  Test program for class HBridge_Motor.
 */
 
#define DEBUG_CSPU
#define USE_ASSERTIONS
#include "DebugCSPU.h"
#include "HBridgeMotor.h"
#include "GMiniConfig.h"

HBridgeMotor myMotor ;

void setup() {
  DINIT(115200);
  myMotor.begin(ScrewLatchPWM_Pin, ScrewLatchForward_Pin, ScrewLatchReverse_Pin);
}

void loop() {
  Serial << "Turning forward"<< ENDL;
  myMotor.TurnForward(500, Power);
  delay(1000);
  Serial << "Turning backwards"<< ENDL;
  myMotor.TurnBackwards(500, Power);
  delay(1000);
}
