/*
   RF_RealisticTest_Can

   A sketch which simulates the can side of the RF transmission:
     - By default permanently emit CansatRecordExample records every <period> msec, and listen
       for a string CMD_STRING. Timestamp is increased by 1 in the record
       each time, in order to be able to detect lost records.
     - When the string is received, answer with RSP_STRING, suspend emission
       for <suspensionDelay> seconds and during this delay, echo every string received on
       the RF channel.

     - The board waits for an actual USB Serial link, unless the SerialActivationPin is pulled down.

*/

// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatConfig.h"
#include "CansatRecordExample.h"
#include "elapsedMillis.h"
#include "string.h"
#include "CansatXBeeClient.h"

#ifndef RF_ACTIVATE_API_MODE
#  error "This program only works if RF_ACTIVATE_API_MODE is defined in CansatConfig.h"
#endif

// Configuration constants
constexpr unsigned long emissionPeriod = 100; // msec.
constexpr unsigned long suspensionDelay = 10000; // msec
constexpr byte RF_RxPinOnUno = 9;
constexpr byte RF_TxPinOnUno = 11;
constexpr byte SerialActivationPin = A5; // If this pin is pulled-down the board will wait for Serial port initialisation,
										// If it is open or pulled-up, it will not
//define SIMULATE_ON_USB_SERIAL // Define to have RF reception and transmission performed on Serial (for debugging)
//define PRINT_IGNORED_STRING   // Define to output the strings received from RF and discarded.
#define CMD_STRING "5,12345,67890"
#define RSP_STRING "** Command received **"
constexpr unsigned long RF_BaudRate = 115200; // This must be consistent with the configuration of the RF modules. 9600 is just enough!
constexpr bool showBufferOverlow = false; // Set to true to get messages on Serial when the reception buffer overflows.
constexpr bool timeRecordEmission = false; // Set to true to print on Serial the duration of the emission of a record.

// -----------------------------------------------------------------------------------------
//Define a RF Serial port.
#ifdef SIMULATE_ON_USB_SERIAL
auto &RF = Serial;
#else
#  ifdef ARDUINO_ARCH_SAMD
HardwareSerial &RF = Serial1;
#   else
#   include "SoftwareSerial.h"
SoftwareSerial RF(RF_RxPinOnUno, RF_TxPinOnUno);
#   endif
#endif

CansatRecordExample outgoingRecord, incomingRecord;
CansatXBeeClient xbc(GroundXBeeAddressSH, GroundXBeeAddressSL); // Defined in CansatConfig.h

bool acquisitionMode; // true if emitting records, false if answering commands.
elapsedMillis elapsed, heartbeatElapsed;

void processRecord(CansatRecordExample &rec) {
	Serial << "R";
}

void processString(const char* str, CansatFrameType type) {
	Serial << ENDL << "Received '" << str << "'" << ENDL;
	if ((type == CansatFrameType::CmdRequest) && (strcmp(str, CMD_STRING) == 0)) {
		Serial << "Sending response" << ENDL;
		xbc.openStringMessage(CansatFrameType::CmdResponse);
		xbc << RSP_STRING;
		xbc.closeStringMessage();
		acquisitionMode = false;
		elapsed = 0;
	} else if (acquisitionMode) {
#ifdef PRINT_IGNORED_STRINGS
		Serial << F("Ignoring unexpected string '") << buffer << "'" << ENDL;
#endif
	} else {
		Serial << F("Echoing received string... ") << ENDL;
		xbc.openStringMessage(CansatFrameType::CmdResponse);
		xbc << str;
		xbc.closeStringMessage();
	}
}

void setup() {
  DINIT_IF_PIN(115200, SerialActivationPin);
  Serial << F(" Using XBee pair ") << RF_XBEE_MODULES_SET << ENDL;
  Serial << F("=== Simulating the CAN ===") << ENDL;
#ifdef SIMULATE_ON_USB_SERIAL
  Serial << F(" === Using USB Serial instead of RF Serial for test! ===") << ENDL;
#else
  RF.begin(RF_BaudRate);
#endif
  xbc.begin(RF);

  pinMode(LED_BUILTIN, OUTPUT);
  outgoingRecord.initValues();
  outgoingRecord.timestamp=100;
  acquisitionMode = true;
  elapsed = heartbeatElapsed = 0;
  Serial << F("This is the record which is transferred:") << ENDL;
  outgoingRecord.print(Serial);
  Serial << ENDL;
#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
  Serial << "Feather board detected: using RX-TX pins for RF communication" << ENDL;
#else
  Serial << "Assuming AVR board: using softwareSerial (rx=" << RF_RxPinOnUno << ", tx=" << RF_TxPinOnUno
         << ") for RF communication" << ENDL;
#endif
  Serial << F("RF baud rate   : ") << RF_BaudRate << F(" baud") << ENDL;

  Serial << F("Record emission period=") << emissionPeriod << F(" msec") << ENDL;
  Serial << F("Suspension when received cmd=") << suspensionDelay / 1000.0 << F(" sec") << ENDL;

  Serial << F("Using PowerLevel 4") << ENDL;
  xbc.setTransmissionPower(XBeeTypes::TransmissionPowerLevel::level4);
  Serial << "Configuration of XBee module (assumed to be the can side of set " << RF_XBEE_MODULES_SET << ")" << ENDL;
  xbc.printConfigurationSummary(Serial);
  auto xbeeRole=xbc.getXBeeSystemComponent();
  if (xbeeRole != CansatXBeeClient::CansatComponent::Can) {
	  Serial << "Error: this XBee module is '" << xbc.getLabel(xbeeRole) << "' while it should be '"
			  << xbc.getLabel(CansatXBeeClient::CansatComponent::Can) << "'." << ENDL;
	  Serial << "Check the content of Cansat config (RF_XBEE_SET, address of XBee) and connected module." << ENDL;
	  Serial << "**** ABORTED ***" << ENDL;
	  Serial.flush();
	  exit(-1);
  }
  Serial << ENDL << F("Setup complete") << ENDL;
}

void loop() {
  static int counter = 0;
  // Sending always performed with a complete record.
  if (acquisitionMode) {
    if (elapsed >= emissionPeriod)
    {
      elapsed = 0;
      counter++;
      if (timeRecordEmission) {
        unsigned long start = millis();
        xbc.send(outgoingRecord);
        unsigned long stop = millis();
        Serial << '(' << stop - start << " msec)";
      }
      else xbc.send(outgoingRecord);

      outgoingRecord.timestamp++;
      Serial << ".";
      if (counter >= 100) {
        counter = 0;
        Serial << ENDL;
      }
    }
  }
  else {
    if (elapsed >= suspensionDelay) {
      acquisitionMode = true;
      elapsed = 0;
      counter = 0;
      Serial << F("Resuming acquisition mode") << ENDL;
    }
  }


  bool gotRecord;
  CansatFrameType stringType;
  char incomingString[xbc.MaxStringSize+1];
  uint8_t seqNbr;
  // Receiving always character by character, in order to
  // avoid delaying emission if garbage is received.
  while (xbc.receive(incomingRecord, incomingString, stringType, seqNbr, gotRecord )) {
    if (gotRecord) {
      processRecord(incomingRecord);
    } else {
      processString(incomingString, stringType);
    }
  } // while receive()
  if (heartbeatElapsed > 500) {
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
    heartbeatElapsed = 0;
  }
}
