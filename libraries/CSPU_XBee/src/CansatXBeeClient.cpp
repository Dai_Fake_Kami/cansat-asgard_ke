/*
 CansatXBeeClient.cpp
 */
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatConfig.h"
#include "CansatXBeeClient.h" // This one must be outside the #ifdef to have the symbol defined

#ifdef RF_ACTIVATE_API_MODE

#define DBG_DIAGNOSTIC 1
#define DBG_STREAM 0

bool CansatXBeeClient::send(const CansatRecord& record,
		int timeOutCheckResponse,
		uint32_t destSH, uint32_t destSL) {
	// Beware of byte alignment. Only even addresses can be accessed directly
	// in SAMD 16-bit architecture.
	uint8_t recordSize= (uint8_t) record.getBinarySize();
	uint8_t bufferSize=recordSize + 2; // Do not use sizeof !
	uint8_t buffer[bufferSize];
	buffer[0] = (uint8_t) CansatFrameType::DataRecord;
	buffer[1] = 0;
	if (!record.writeBinary(buffer + 2,recordSize)) {
		DPRINTS(DBG_DIAGNOSTIC, "Error streaming record");
		return false;
	}
	if (DisplayOutgoingMsgOnSerial) {
		Serial << ENDL << "  --- Outgoing record ---" << ENDL;
		record.print(Serial);
		Serial << ENDL << "  --- End of record ---" << ENDL;
		printFrame(buffer, bufferSize);
	}
	return XBeeClient::send((uint8_t *) buffer, bufferSize, timeOutCheckResponse, destSH, destSL);
}

bool CansatXBeeClient::receive(char* string, CansatFrameType &stringType, uint8_t& seqNbr) {
	uint8_t *buffer;
	uint8_t payloadSize;
	bool result=false;
	if (XBeeClient::receive(buffer, payloadSize)) {
		if (getString(string, stringType, seqNbr, buffer, payloadSize, true)) {
			if (DisplayIncomingMsgOnSerial) {
				Serial << ENDL << "  --- Incoming string ---" << ENDL;
				printString(buffer, payloadSize);
			}
			result=true;
		} // received string
	} // received anything
	return result;
}

bool CansatXBeeClient::receive(CansatRecord& record, char* string,
		CansatFrameType &stringType, uint8_t& stringSeqNbr, bool& gotRecord) {
	uint8_t *buffer;
	uint8_t payloadSize;
	bool result = false;
	if (XBeeClient::receive(buffer, payloadSize)) {
		gotRecord=(buffer[0] == (uint8_t) CansatFrameType::DataRecord);
		if (gotRecord) {
			result = getDataRecord(record, buffer, payloadSize);
		} else {
			result = getString(string, stringType, stringSeqNbr,  buffer, payloadSize);
		}

		if (DisplayIncomingMsgOnSerial) {
			if (gotRecord) {
				Serial << ENDL << "  --- Incoming record ---" << ENDL;
				record.print(Serial);
				Serial << ENDL << "  --- End of record ---" << ENDL;
			} else {
				Serial << ENDL << "  --- Incoming string ---" << ENDL;
				printString(buffer, payloadSize);
			}
		}
	}
	return result;
}

void CansatXBeeClient::openStringMessage(CansatFrameType frameType, uint8_t seqNbr) {
	DPRINTS(DBG_STREAM, "CansatXBeeClient::openStringMessage(), type= ");
	DPRINTLN(DBG_STREAM, (uint8_t ) frameType);
	XBeeClient::openStringMessage((uint8_t) frameType, seqNbr);
}

bool CansatXBeeClient::getDataRecord(CansatRecord& record, const uint8_t* data,
		uint8_t dataSize) const {
	uint8_t recordSize=record.getBinarySize();
	if (dataSize != (recordSize + 2)) {
		DPRINTS(DBG_DIAGNOSTIC, "*** CansatXBClient::getDataRecord: Error: inconsistent size. dataSize=");
		DPRINT(DBG_DIAGNOSTIC, dataSize);
		DPRINTS(DBG_DIAGNOSTIC, ", CansatRecord size=");
		DPRINT(DBG_DIAGNOSTIC, recordSize);
		DPRINTSLN(DBG_DIAGNOSTIC, ", expected data=record+2");
		return false;
	}
	if (data[0] != (uint8_t) CansatFrameType::DataRecord) {
		DPRINTS(DBG_DIAGNOSTIC, "*** Error: inconsistent record type (");
		DPRINT(DBG_DIAGNOSTIC, data[0]);
		DPRINTS(DBG_DIAGNOSTIC, ", expected ");
		DPRINTLN(DBG_DIAGNOSTIC, (uint8_t ) CansatFrameType::DataRecord);
		return false;
	}

	if (!record.readBinary(data+2, dataSize-2)) {
		DPRINTS(DBG_DIAGNOSTIC, "*** Error reading record");
		return false;
	}
	return true;
}

bool CansatXBeeClient::getString(char* str, CansatFrameType &stringType,
		uint8_t& seqNbr,const uint8_t* data, uint8_t dataSize,
		bool silentIfError) const {
	uint8_t sType;
	bool result = XBeeClient::getString(str, sType, seqNbr, data, dataSize);

	stringType = (CansatFrameType) data[0];
	switch (stringType) {
	case CansatFrameType::StatusMsg:
	case CansatFrameType::CmdRequest:
	case CansatFrameType::CmdResponse:
	case CansatFrameType::StringPart:
		break;
	default:
		if (!silentIfError) {
			DPRINTS(DBG_DIAGNOSTIC, "*** Error: inconsistent record type (");
			DPRINT(DBG_DIAGNOSTIC, data[0]);
			DPRINTS(DBG_DIAGNOSTIC,
					", expected StatusMsg, CmdRequest, CmdResponse or StringPart).");
		}
		result = false;
		break;
	} // switch
	return result;
}

bool CansatXBeeClient::printConfigurationSummary(Stream &stream,
		const uint32_t & sh, const uint32_t& sl) {
	autoWakeUpXBeeModule();
	if (!XBeeClient::printConfigurationSummary(stream, sh, sl)) {
		autoPutXBeeModuleToSleep();
		return false;
	}

	uint8_t  moduleID;
	if ((sh==0)&&(sl==0)) {
		moduleID=getXBeeModuleID();
	}
	else moduleID=getXBeeModuleID(sh, sl);

	stream << "  According to CansatConfig.h: " << ENDL;
	stream << "    XBee module ID     : ";
	if (moduleID !=0) stream << moduleID << ENDL;  else stream << "UNKNOWN" << ENDL;
	stream << "    XBee set in use    : " << RF_XBEE_MODULES_SET << ENDL;
	stream << "    Role of this module: " << getLabel(getXBeeSystemComponent(sh,sl)) << ENDL;

	autoPutXBeeModuleToSleep();
	return  true;
}

bool CansatXBeeClient::doCheckXBeeModuleConfiguration(bool correctConfiguration, bool &configurationChanged,
		const uint32_t& sh, const uint32_t& sl) {
	autoWakeUpXBeeModule();
	bool result=checkCommonModuleConfiguration(correctConfiguration, configurationChanged, sh, sl);
    CansatComponent component = getXBeeSystemComponent(sh,sl);

	switch(component) {
	case CansatComponent::Can:
		if (!checkCanModuleConfiguration(correctConfiguration, configurationChanged, sh, sl)) {
			result=false;
		}
		break;
	case CansatComponent::RF_Transceiver:
		if (!checkGroundModuleConfiguration(correctConfiguration, configurationChanged, sh, sl)) {
			result=false;
		}
		break;
	case CansatComponent::Undefined:
		result=false;
		Serial << "*** Unidentified system component: " << getLabel(component) << ENDL;
		Serial << "Check the XBee pair configured in CansatConfig" << ENDL;
		break;
	default:
		result=false;
		Serial << "*** Unexpected system component (" << (uint8_t) getXBeeSystemComponent() << ")" << ENDL;
	}
	autoPutXBeeModuleToSleep();
	return result;
}

bool CansatXBeeClient::checkCommonModuleConfiguration(
		bool correctConfiguration, bool &configurationChanged,
		const uint32_t& sh, const uint32_t& sl) {
	bool result=true, tmpResult;

	autoWakeUpXBeeModule();
	//NB: XBeeChannels is defined in CansatConfig.h
	tmpResult = checkParameter("SC", XBeeChannels,
			correctConfiguration, configurationChanged, sh, sl);
	result = result && tmpResult;
	// PS: do not do result = result && checkParameter(...): the check would not
	//     be performed if result is false!
	constexpr uint8_t NameSize=20;
	char name[NameSize];
	if (!queryParameter("NI", name, NameSize, sh, sl)) {
		result=false;
	} else {
		if (strlen(name) > 3) {
			uint8_t expectedID=((name[0] -'0') << 8) + (name[1]-'0');
			tmpResult = (getXBeeModuleID(sh,sl) == expectedID);
			if (!tmpResult ){
				Serial << "NI is not OK: prefix does not match module ID ('"
					   << name << "' vs. " << getXBeeModuleID() << ")." << ENDL;
			}
			result = result && tmpResult;
		} else {
			tmpResult=false;
			Serial << "NI is not OK: too short to support module ID prefix." << ENDL;
		}
	}
	result = result && tmpResult;
	tmpResult = checkParameter("ZS", (uint8_t)  0x02,
			correctConfiguration, configurationChanged, sh,sl);
	result = result && tmpResult;
	// For some reason parameter BD returns 4 bytes.
	tmpResult = checkParameter("BD", (uint32_t) 0x07,
			correctConfiguration, configurationChanged, sh,sl);
	result = result && tmpResult;
	tmpResult = checkParameter("AP", (uint8_t) 0x02,
			correctConfiguration, configurationChanged, sh,sl);
	result = result && tmpResult;
	tmpResult = checkParameter("JV", (uint8_t) 0x00,
			correctConfiguration, configurationChanged, sh,sl);
	result = result && tmpResult;
	tmpResult = checkParameter("ID", (uint64_t) 0x1001,
			correctConfiguration, configurationChanged, sh,sl);
	result = result && tmpResult;
	tmpResult = checkParameter("AO", false,
			correctConfiguration, configurationChanged, sh,sl);
	result = result && tmpResult;
	tmpResult = checkParameter("NH", (uint8_t) XBeeNH_Parameter,
			correctConfiguration, configurationChanged, sh,sl);
	result = result && tmpResult;
	tmpResult = checkParameter("EE", (uint8_t) 0x00,
			correctConfiguration, configurationChanged, sh,sl);
	result = result && tmpResult;
	tmpResult = checkParameter("NJ", (uint8_t) 0xFF,
			correctConfiguration, configurationChanged, sh,sl);
	result = result && tmpResult;
	tmpResult = checkParameter("SP", (uint16_t) 0xAF0,
			correctConfiguration, configurationChanged, sh,sl);
	result = result && tmpResult;
	tmpResult = checkParameter("SN", (uint16_t) 0x01,
			correctConfiguration, configurationChanged, sh,sl);
	result = result && tmpResult;
	tmpResult = checkParameter("SO", (uint16_t) 0x00,
			correctConfiguration, configurationChanged, sh,sl);
	result = result && tmpResult;

	//GPIO pins configuration to minimize sleep currents
	// 0x04 = digital output default low.
	tmpResult = checkParameter("P0", (uint8_t) 0x04,
			correctConfiguration, configurationChanged, sh,sl);
	result = result && tmpResult;
	tmpResult = checkParameter("P1", (uint8_t) 0x04,
			correctConfiguration, configurationChanged, sh,sl);
	result = result && tmpResult;
	tmpResult = checkParameter("P2", (uint8_t) 0x04,
			correctConfiguration, configurationChanged, sh,sl);
	result = result && tmpResult;
	tmpResult = checkParameter("P3", (uint8_t) 0x01, /* UART data out, pin 2 */
			correctConfiguration, configurationChanged, sh,sl);
	result = result && tmpResult;
	tmpResult = checkParameter("P4", (uint8_t) 0x01, /* UART data in, pin 3 */
			correctConfiguration, configurationChanged, sh,sl);
	result = result && tmpResult;

	tmpResult = checkParameter("D0", (uint8_t) 0x04,
			correctConfiguration, configurationChanged, sh,sl);
	result = result && tmpResult;
	tmpResult = checkParameter("D1", (uint8_t) 0x04,
			correctConfiguration, configurationChanged, sh,sl);
	result = result && tmpResult;
	tmpResult = checkParameter("D2", (uint8_t) 0x04,
			correctConfiguration, configurationChanged, sh,sl);
	result = result && tmpResult;
	tmpResult = checkParameter("D3", (uint8_t) 0x04,
			correctConfiguration, configurationChanged, sh,sl);
	result = result && tmpResult;
	tmpResult = checkParameter("D4", (uint8_t) 0x04,
			correctConfiguration, configurationChanged, sh,sl);
	result = result && tmpResult;
	tmpResult = checkParameter("D5", (uint8_t) 0x04,
			correctConfiguration, configurationChanged, sh,sl);
	result = result && tmpResult;
	tmpResult = checkParameter("D6", (uint8_t) 0x04,
			correctConfiguration, configurationChanged, sh,sl);
	result = result && tmpResult;
	tmpResult = checkParameter("D7", (uint8_t) 0x04,
			correctConfiguration, configurationChanged, sh,sl);
	result = result && tmpResult;
	// D8 - D9 configure sleep pins. Leave to default.
	// By default power should be set to InCampaignPowerLevel:
	tmpResult = checkParameter("PL", (uint8_t) InCampaignPowerLevel,
			correctConfiguration, configurationChanged, sh , sl);
	result = result && tmpResult;

	autoPutXBeeModuleToSleep();
	return result;
}

uint8_t CansatXBeeClient::getXBeeModuleID(const uint32_t &sh,const uint32_t &sl) {
	uint32_t mySH=sh, mySL=sl;
	if ((mySH==0)&&(mySL==0)) {
		if ((!queryParameter("SH", mySH)) || (!queryParameter("SL", mySL)) ) {
			DPRINTSLN(DBG_DIAGNOSTIC, "Cannot retrieve XBee address");
			return 0;
		}
	}
    if ((mySH==XBeeAddressSH_01) && (mySL==XBeeAddressSL_01)) return 1;
    if ((mySH==XBeeAddressSH_02) && (mySL==XBeeAddressSL_02)) return 2;
    if ((mySH==XBeeAddressSH_03) && (mySL==XBeeAddressSL_03)) return 3;
    if ((mySH==XBeeAddressSH_04) && (mySL==XBeeAddressSL_04)) return 4;
    if ((mySH==XBeeAddressSH_05) && (mySL==XBeeAddressSL_05)) return 5;
    if ((mySH==XBeeAddressSH_06) && (mySL==XBeeAddressSL_06)) return 6;
    if ((mySH==XBeeAddressSH_07) && (mySL==XBeeAddressSL_07)) return 7;
    if ((mySH==XBeeAddressSH_08) && (mySL==XBeeAddressSL_08)) return 8;
    if ((mySH==XBeeAddressSH_09) && (mySL==XBeeAddressSL_09)) return 9;
    if ((mySH==XBeeAddressSH_10) && (mySL==XBeeAddressSL_10)) return 10;
	return 0;
}

CansatXBeeClient::CansatComponent CansatXBeeClient::getXBeeSystemComponent(
		const uint32_t &sh, const uint32_t &sl) {
	autoWakeUpXBeeModule();
	uint32_t mySH=sh, mySL=sl;
	CansatXBeeClient::CansatComponent result=CansatComponent::Undefined;
	if ((mySH==0)&&(mySL==0)) {
		if ((!queryParameter("SH", mySH)) || (!queryParameter("SL", mySL)) ) {
			DPRINTSLN(DBG_DIAGNOSTIC, "Cannot retrieve XBee address");
			autoPutXBeeModuleToSleep();
			return result;
		}
	}

	if ((mySH == CanXBeeAddressSH) && (mySL == CanXBeeAddressSL)) {
		result= CansatComponent::Can;
	}
	else if ((mySH == GroundXBeeAddressSH) && (mySL == GroundXBeeAddressSL)) {
		result=CansatComponent::RF_Transceiver;
	}
	autoPutXBeeModuleToSleep();
	return result;
}

const char* CansatXBeeClient::getLabel(CansatComponent component) {
	switch (component) {
	case CansatComponent::Can:
		return "CAN";
		break;
	case CansatComponent::RF_Transceiver:
		return "GROUND STATION";
		break;
	case CansatComponent::Undefined:
		return "UNIDENTIFIED";
		break;
	default:
		return "Unknown";
		DPRINTS(DBG_DIAGNOSTIC, "Unexpected system component: ");
		DPRINTLN(DBG_DIAGNOSTIC,(uint8_t) component);
	}
}

void CansatXBeeClient::updateModuleID_Prefix(char* name,
		const uint32_t& sh, const uint32_t& sl ) {
	if (strlen(name) < 5) {
		DPRINTSLN(DBG_DIAGNOSTIC, "Error: updateModuleID_Prefix() got name shorter than 5 chars: ");
		DPRINTLN(DBG_DIAGNOSTIC, name);
		return;
	}
	uint8_t id=getXBeeModuleID(sh,sl);
	name[0]='0'+ (id >> 8);
	name[1]='0' + (id & 0xF);
}

bool CansatXBeeClient::checkCanModuleConfiguration(
		bool correctConfiguration, bool &configurationChanged,
		const uint32_t& sh, const uint32_t& sl ) {
	bool success=true;

	autoWakeUpXBeeModule();
	char name[10]="nn_CAN_x";
	updateModuleID_Prefix(name,sh,sl);
	name[7]=RF_XBEE_MODULES_SET;
	if (!checkParameter(	"NI",
			name,
			correctConfiguration,
			configurationChanged,
			sh,sl)) success=false;
	// Do not check DH-DL: it's not relevant in API mode.
	// Can must be the Coordinator.
	if (!checkDevicetype(	XBeeTypes::DeviceType::Coordinator,
			correctConfiguration,
			configurationChanged, sh, sl)) success = false;
	autoPutXBeeModuleToSleep();
	return success;
}

bool CansatXBeeClient::checkGroundModuleConfiguration(
		bool correctConfiguration, bool &configurationChanged,
		const uint32_t& sh, const uint32_t& sl ) {
	bool success=true;

	autoWakeUpXBeeModule();
	char name[14]="nn_RF-TSCV_x";
	updateModuleID_Prefix(name, sh, sl);
	name[11]=RF_XBEE_MODULES_SET;
	if (!checkParameter(	"NI",
			name,
			correctConfiguration,
			configurationChanged,
			sh,sl)) success=false;
	// Do not check DH-DL: it's not relevant in API mode.
	// Ground must be a router.
	if (!checkDevicetype(	XBeeTypes::DeviceType::Router,
			correctConfiguration,
			configurationChanged,
			sh,sl)) success = false;
	autoPutXBeeModuleToSleep();
	return success;
}
#endif
