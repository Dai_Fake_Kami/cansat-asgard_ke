/*
   EtheraAcquisitionProcess.h
*/

#pragma once
#include "CansatAcquisitionProcess.h"
#include "EtheraSecondaryMissionController.h"
#include "LinearStepper.h"

/** @ingroup EtheraCSPU
 *  @brief the Acquisition process for the Ethera project
*/
class EtheraAcquisitionProcess: public CansatAcquisitionProcess {
  public:
    EtheraAcquisitionProcess() {};
    virtual ~EtheraAcquisitionProcess() {};


    /** Obtain the LinearStepper object managed by the secondary mission controller */
    LinearStepper* getLinearStepper() {
      return theSecondaryController.getLinearStepper();
    };

  protected:
    /** @name Protected methods overridden from base class.
      	See detailed description in base classes.
        @{   */

    virtual void doIdle() override  {
      CansatAcquisitionProcess::doIdle();
      // do our own background tasks here, if any
    };

    /** @} */

  private:
    /** @name Private methods overridden from base class.
      	See detailed description in base classes.
        @{   */

    virtual CansatRecord* 	getNewCansatRecord () override {
      return new EtheraRecord;
    };

    virtual CansatHW_Scanner* getNewHardwareScanner() override {
      return new CansatHW_Scanner; // Or EtheraHW_Scanner ?;
    }

    virtual void initSpecificProject () override {
      // Perform specific project initialisation here
    };

    virtual void acquireSecondaryMissionData(CansatRecord &record) override {
      // Acquire secondary mission data here, if required.
    } ;

    virtual SecondaryMissionController* getSecondaryMissionController() override {
      return &theSecondaryController;

    };
    /** @} */
    EtheraSecondaryMissionController theSecondaryController; // Our secondaryMissionController.

};
