/*
 * EtheraCSPU.h
 * 
 * This files is only used for documentation of the library using Doxygen.
 * Every class in the library should include a tag @ingroup EtheraCSPU
 * in the class documentation block.
 */

 /** @defgroup EtheraCSPU EtheraCSPU library
 *  @brief The library of classes specific to the CanSat 2022/2023 EtheraCSPU project.
 *  
 *  The EtheraCSPU library contains all the code for the EtheraCSPU project, which is assumed
 *  not to be reused across project
 *  
 *  _Dependencies_\n
 *  This library requires the following other generic libraries (in addition to hardware-specific libraries,
 *  and standard Arduino libraries, which are not listed here):
 *  - DebugCSPU
 *  - TimeCSPU
 *  - elapsedMillis
 *  - cansatAsgardCSPU
 *  @todo TO BE COMPLETED.
 *  
 *  
 *  _History_\n
 *  The library was created by the 2022/2023 Cansat team (EtheraCSPU) based on the EtheraCSPU, 
 *  TorusCSPU, IsatisCSPU, IsaTwoCSPU and SpySeeCanCSPU libraries.
 */
