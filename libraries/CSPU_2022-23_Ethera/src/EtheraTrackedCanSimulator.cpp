
#include "EtheraTrackedCanSimulator.h"
#include "EtheraConfig.h"
#include "CSPU_Debug.h"
#include "EtheraRecord.h"

EtheraTrackedCanSimulator::EtheraTrackedCanSimulator() :  rf69(RFM69_ChipSelectPin, RFM69_InterruptPin)
{}


void EtheraTrackedCanSimulator::initMessage() {
	strcpy(message, TrackedCanMessage[canID]);
	Serial << "Message initialized to '" << message << "'" << ENDL;
}

bool EtheraTrackedCanSimulator::begin(int8_t id) {
  Serial << "Ethera Tracked can simulator #" << id << "." << ENDL;
  canID = id;
  elapsedSinceLastMsg = TrackedCanPeriod[canID];

  initMessage();

  pinMode(RFM69_ResetPin, OUTPUT);
  digitalWrite(RFM69_ResetPin, LOW);

  // manual reset
  digitalWrite(RFM69_ResetPin, HIGH);
  delay(10);
  digitalWrite(RFM69_ResetPin, LOW);
  delay(10);

  if (!rf69.init()) {
    Serial.println("*** RFM69 radio init failed.");
    return false;
  }
  Serial.println("  RFM69 radio init OK!");
  // Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM (for low power module)
  // No encryption

  rf69.setTxPower(TrackedCanPower[canID], true);  // range from 14-20 for power, 2nd arg must be true for 69HCW
  Serial << "  Power set to " << TrackedCanPower[canID] << ENDL;

  if (!rf69.setFrequency(CanFrequency[canID])) {
    Serial.println("*** setFrequency failed. Aborted.");
    return false;
  }
  Serial << "  Frequency set to " << CanFrequency[canID] << " MHz" << ENDL;


  if (CanUseEncryption[canID]) {
    Serial << "  Encryption active" << ENDL;
    unsigned char key[CanEncryptionKeySize];
    for (int i = 0 ; i < CanEncryptionKeySize ; i ++) {
      key[i] = CanEncryptionKey[canID][i];
      Serial.print(CanEncryptionKey[canID][i], HEX);
      Serial << '-';
    }
    Serial << ENDL;
    rf69.setEncryptionKey(key);
  }
  else Serial << "  No encryption" << ENDL;
  Serial << "  Can Period=" << TrackedCanPeriod[canID] << " ms" << ENDL;
  Serial << "Simulator initialization OK for can '" << CanName[canID] <<"'" << ENDL;

  return true;
}

void EtheraTrackedCanSimulator::run() {
  if (elapsedSinceLastMsg > TrackedCanPeriod[canID]) {
    rf69.send((uint8_t *) message, strlen(message));
    elapsedSinceLastMsg = 0;
    Serial << CanFrequency[canID] << " MHz";
    if (CanUseEncryption[canID]) {
    	Serial << " (E)";
    }
    Serial << ": Sent '" << message << "'" << ENDL;
  }
}
