/*
   EtheraCanCommander.h
*/

#pragma once
#include "RT_CanCommander.h"
#include "LinearStepper.h"


/** @ingroup EtheraCSPU
    @brief A subclass of RT_CanCommander adding support for the Ethera-specific commands
    as specified in CansatInterface.h
*/
class EtheraCanCommander : public RT_CanCommander {
  public:
    /** @brief Constructor
          @param theTimeOut The duration (in milliseconds) for the Command mode to time out (and switch back to acquisition).
    */
    EtheraCanCommander(unsigned long int theTimeOut);
    virtual ~EtheraCanCommander() {};

#ifdef RF_ACTIVATE_API_MODE
    /** @brief Method used to initialize the object when using the RF API mode.
        @param xbeeClient The interface to the XBee module to use for output .
        @param theSd Initialized object of type SdFat that will be used by the class for various Sd Card related actions.
        @param theProcess Initialized object of type AcquisitionProcess used for interaction with it by the class when handling commands.

    */
    void begin(CansatXBeeClient& xbeeClient, SdFat* theSd = NULL, CansatAcquisitionProcess* theProcess = NULL);
#else
    /** @brief Method used to initialize various pointers.
        @param RF_Stream The output stream used by the class.
        @param theSd Initialized object of type SdFat that will be used by the class for various Sd Card related actions.
        @param theProcess Initialized object of type AcquisitionProcess used for interaction with it by the class when handling commands.

    */
    void begin(Stream& RF_Stream, SdFat* theSd = NULL, CansatAcquisitionProcess* theProcess = NULL);
#endif


  protected:
    /** Process a project specific command request while in command mode.
        This method implements Ethera-specific commands.
       @param requestType the request type value
       @param cmd Pointer to the first character after the command type. Command parameter can be parsed from
              this position which should point to a separator if any parameter is present, or to the final '\0'
              if none is provided.
       @return True if the command was processed, false otherwise.
    */
    virtual bool processProjectCommand(CansatCmdRequestType requestType, char* cmd)  override;
    /** Process the opening or the closing of the airbrakes */
    void processReq_AirBrakes_Set(char* &nextCharAddress);
    /** process the request for the current position of airbrakes */
    void processReq_AirBrakes_Get(char* &nextCharAddress);
    /** process the opening or the closing of the airbrakes by a certain number of steps */
    void processReq_AirBrakes_SetNumSteps(char* &nextCharAddress);
    /** process the request for the current steps made */
    void processReq_AirBrakes_GetNumSteps(char* &nextCharAddress);
    /** process the reposition of 0 */
    void processReq_AirBrakes_ForceZero(char* &nextCharAddress);

  private:
    /** The object that calls the commands */
    LinearStepper* theLinearStepper;

};
