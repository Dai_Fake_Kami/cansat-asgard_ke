#pragma once
#include "CansatRecord.h"
#include "CansatInterface.h"

/** @ingroup EtheraCSPU
    @brief The record carrying all data acquired or computed by the Ethera CanSat
*/
class EtheraRecord : public CansatRecord {
  public:
    uint8_t brakePosition;  // Range: 0-100
    uint8_t flightPhase;
    uint16_t distance; // unit = 1 m, max range = 65535 m.
    FlightControlCode flightControlCode;

    /** Set the frequency in the record. Only 0.1 MHz precision is stored.
     *  @param frequency The frequency to set (in MHz)
     */
    void setFrequency(float newFrequency) {
    	this->frequency = (uint16_t)(newFrequency*10.0f);
    };

    /** Set the RSSI in the record. Only 0.1 dBM precision is stored.
	 *  @param rssi The RSSI to set (in dBm)
	 */
	void setRSSI(float newRssi) {
		this->rssi = (int16_t)(newRssi*10.0f);};

	float getFrequency() { return frequency/10.0f; };
	float getRSSI() { return rssi/10.0f; } ;
	bool hasRSSI_Data() { return (frequency != 0); };
     
  protected:
	uint16_t frequency; //unit 0,1 MHz
	int16_t rssi; //  unit = 0.1 dBM.
    virtual void printCSV_SecondaryMissionData(Stream &str, bool startWithSeparator, bool finalSeparator) const;
    virtual void printCSV_SecondaryMissionHeader(Stream &str, bool startWithSeparator, bool finalSeparator) const;
    virtual void clearSecondaryMissionData();
    virtual uint16_t getSecondaryMissionMaxCSV_Size() const;
    virtual uint16_t getSecondaryMissionCSV_HeaderSize() const;
    virtual void printSecondaryMissionData(Stream& str) const;
    virtual uint8_t writeBinarySecondaryMissionData(uint8_t* const destinationBuffer, uint8_t bufferSize) const;
    virtual uint8_t readBinarySecondaryMissionData(const uint8_t* const sourceBuffer, const uint8_t bufferSize);
    virtual uint8_t getBinarySizeSecondaryMissionData() const;

    friend class EtheraRecord_Test;
};
