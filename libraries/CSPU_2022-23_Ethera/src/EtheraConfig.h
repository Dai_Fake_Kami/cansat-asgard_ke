/*
   EtheraConfig.h

   Specific configuration for the Ethera project
*/

#pragma once
#include "CansatConfig.h"

// ***************************************
// **  Secondary Mission 1: airbrakes   **
// ***************************************

// Configuration of linear steppers
constexpr uint8_t LinearStepperDirectionPin = 12;
constexpr uint8_t LinearStepperStepPin = 6;
constexpr uint8_t LinearStepperSleepPin = 9;
constexpr uint32_t LinearStepperNumSteps = 4823;



// EtheraSecondaryMissionController
constexpr unsigned int MaxDelayFromTakeoffToEjection = 15000; /**< over this limit in msec, the sub can will be ejected*/
constexpr uint8_t VelocityAveragingPeriod = 2; /**< sec*/
constexpr float VelocityThresholdForTakeoff = -2.00;/**< m/s positive when falling but during the takeoff we are going up so it is negative*/

// ****************************************************
// **  Secondary Mission 2: distance from RF power   **
// ****************************************************
constexpr float MinRSSI_Value = -100;
constexpr float MaxRSSI_Value = 0;
constexpr uint16_t RSSI_MeasurementTimeout = CansatAcquisitionPeriod/2; // msec
struct ReferenceValue_t {
	float x0;    // The reference distance (m)
	float rssi0; // The corresponding rssi (dBm)
};

// Configuration of tracked beacons
constexpr uint8_t NumBeacons = 2;
constexpr uint8_t MaxNumBeacons = 2;
constexpr uint8_t NumSamplesForRSSI0 = 5;
constexpr uint8_t BeaconEncryptionKeySize = 16;
constexpr unsigned int BeaconPeriod[MaxNumBeacons] = {200, 200}; /*msec*/
constexpr float BeaconFrequency[MaxNumBeacons] = {434.0, 435.2};
constexpr bool BeaconUseEncryption[MaxNumBeacons] = { true, true };
constexpr int8_t BeaconPower[MaxNumBeacons] = {20, 20};  // in dBm. Range from 14-20 for RF69M
constexpr unsigned char BeaconEncryptionKey[MaxNumBeacons][BeaconEncryptionKeySize] ={
                                                 { 0x11, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 
                                                   0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08 } ,
                                                 { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
                                                   0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08 }
                                             };
constexpr ReferenceValue_t BeaconReferences[MaxNumBeacons] = {
								{100.0f,  -31.6f },
								{0, 0 }  // If x0 = 0, the entry is ignored.
							};

// Configuration of tracked CanSats
constexpr uint8_t NumTrackedCans=2;
constexpr uint8_t NumConfiguredCans=2;
constexpr uint8_t CanEncryptionKeySize = 16;
constexpr float CanFrequency[NumConfiguredCans] = {434.5, 435.6};
constexpr bool CanUseEncryption[NumConfiguredCans] = { false, true };
constexpr const char * CanName[NumConfiguredCans] = {	"Can with ID 0",
														"Can with ID 1"};
constexpr unsigned char CanEncryptionKey[NumConfiguredCans][CanEncryptionKeySize] ={
                                                 { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
                                                   0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08 } ,
                                                 { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
                                                   0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08 }
                                             };
constexpr ReferenceValue_t CanReferences[NumConfiguredCans] = {
								{112.0f,  -34.6f },
								{0, 0 }  // If x0 = 0, the entry is ignored.
							};
// Additional information for use by our simulators
constexpr unsigned int TrackedCanPeriod[NumConfiguredCans] = {200, 200}; /*msec*/
constexpr const char * TrackedCanMessage[NumConfiguredCans] = {	"Message from tracked can 0",
														"Message from tracked can 1"};
constexpr int8_t TrackedCanPower[NumConfiguredCans] = {20, 20};  // in dBm. Range from 14-20 for RF69M



// Hardware configuration of RFM69
constexpr uint8_t RFM69_InterruptPin = 5;
constexpr uint8_t RFM69_ChipSelectPin = A3;
constexpr uint8_t RFM69_ResetPin = A4;
