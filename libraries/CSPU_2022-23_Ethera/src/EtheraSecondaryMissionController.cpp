/*
   EtheraSecondaryMissionController.cpp
*/

// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "EtheraSecondaryMissionController.h"
#define DBG_VELOCITY_BUFFER 0
#define DBG_VELOCITY 0
#define DBG_EJECTION 0

void EtheraSecondaryMissionController::printVelocityTable() {
  for (int i = 0; i < NumberOfVelocitySamples; i++)
    Serial << "  " << i << ": " << velocityTable[i] << ENDL;
}

void EtheraSecondaryMissionController::updateStatus(EtheraRecord& rec) {
#if (DBG_VELOCITY_BUFFER == 1)
  printVelocityTable();
#endif
#if (DBG_VELOCITY == 1)
  Serial << "BEFORE: ts=" << rec.timestamp << ", avg velocity=" << averageVelocity << ", currentIdx=" << currentVelocityIndex << ", lastTs=" << lastRecordTimestamp << ENDL;
#endif
  if (rec.descentVelocity == BMP_Client::InvalidDescentVelocity) {
#if (DBG_VELOCITY == 1)
    Serial << "INVALID VELOCITY DETECTED velocity= " <<  rec.descentVelocity << ENDL;
#endif
    clearAverageVelocity();
    lastRecordTimestamp = rec.timestamp;
    return;
  }
  if ((rec.timestamp - lastRecordTimestamp) > (3 * CansatAcquisitionPeriod)) {
    clearAverageVelocity();
#if (DBG_VELOCITY == 1)
    Serial << "INVALID REC DETECTED ts=" << rec.timestamp << ", avg velocity=" << averageVelocity << ", currentIdx=" << currentVelocityIndex << ENDL;
#endif
    lastRecordTimestamp = rec.timestamp;
    return;
  }

  if (rec.timestamp < lastRecordTimestamp) {
    clearAverageVelocity();

#if (DBG_VELOCITY == 1)
    Serial << "INVALID PAST REC DETECTED ts=" << rec.timestamp << ", avg velocity=" << averageVelocity << ", currentIdx=" << currentVelocityIndex << ENDL;
#endif
  }
  averageVelocity = averageVelocity + (rec.descentVelocity - velocityTable[currentVelocityIndex]) / NumberOfVelocitySamples;
  velocityTable[currentVelocityIndex] = rec.descentVelocity;
  currentVelocityIndex++;
  if (currentVelocityIndex == NumberOfVelocitySamples) {
    currentVelocityIndex = 0;
    DPRINTSLN(DBG_VELOCITY, "Index reset"); //
    velocityAverageValid = true;
  }
  lastRecordTimestamp = rec.timestamp;
#if (DBG_VELOCITY_BUFFER == 1)
  Serial << "AFTER: ts=" << rec.timestamp << ", avg velocity=" << averageVelocity << ", currentIdx=" << currentVelocityIndex << ENDL;
  printVelocityTable();
#endif
  if ((!takeoffHappened) && velocityAverageValid && (averageVelocity  < VelocityThresholdForTakeoff )) {
    DPRINTLN(DBG_EJECTION, takeoffHappened);
    DPRINTLN(DBG_EJECTION, velocityAverageValid);
    DPRINTLN(DBG_EJECTION, averageVelocity);
    DPRINTLN(DBG_EJECTION, VelocityThresholdForTakeoff);
    takeoffHappened = true;
    takeoffTimestamp = rec.timestamp;
#ifdef RF_ACTIVATE_API_MODE
     CansatXBeeClient *xbee=getXbeeClient();
    if(xbee != nullptr){
      RF_OPEN_STRING(xbee);
      *xbee << millis() << ": 2-ary: Take off detected !";
      RF_CLOSE_STRING(xbee);
    }
#endif
    DPRINTS(DBG_EJECTION, "takeoff : timestamp set to ");
    DPRINT(DBG_EJECTION, takeoffTimestamp);
    DPRINTS(DBG_EJECTION, ", threshold (m/s) ");
    DPRINTLN(DBG_EJECTION, VelocityThresholdForTakeoff);

  }

  // Update flight status
  // TODO: DEFINE FLIGHT STATUS
}

void EtheraSecondaryMissionController::manageAirBrakes(EtheraRecord &record)
{
	// TO BE COMPLETED
}

void EtheraSecondaryMissionController::manageSecondaryMission(CansatRecord & record)
{
  static uint16_t counter=0;
  if (counter < 10000/CansatAcquisitionPeriod) {
    counter++;
    return;
  }
  EtheraRecord& myEtheraRecord = (EtheraRecord&) record;
  updateStatus(myEtheraRecord);

  if (rssiCycleManagerOK) {
	  rssiCycleManager.run(myEtheraRecord);
  }
  if (linearStepper) {
	  manageAirBrakes(myEtheraRecord);
	  // Add brakes position in record
	  myEtheraRecord.brakePosition=linearStepper->getPosition();
  }

}

void EtheraSecondaryMissionController::clearAverageVelocity() {
  for (int i = 0; i < NumberOfVelocitySamples; i++) {
    velocityTable[i] = 0;
  }
  averageVelocity = 0;
  velocityAverageValid = false;
}

bool EtheraSecondaryMissionController::initEthera() {
  takeoffHappened = false;
  takeoffTimestamp = 0;

  rssiCycleManagerOK = rssiCycleManager.begin();
  if (!rssiCycleManagerOK) {
	  DPRINTSLN(DBG_DIAGNOSTIC, "*** ERROR initialising RSSI cycle manager. ");
  }
  // If not OK, still try to initialize the other mission.

  LinearStepper * localLinearStepper = new LinearStepper;
  if (localLinearStepper == nullptr) {
    DPRINTSLN(DBG_DIAGNOSTIC, "*** ERROR ALLOCATING LinearStepper");
  } else {
    if (!localLinearStepper->begin(	LinearStepperDirectionPin,
    								LinearStepperStepPin,
									LinearStepperSleepPin,
									LinearStepperNumSteps)) {
    	delete localLinearStepper;
    	localLinearStepper = nullptr;
    }
  }

  clearAverageVelocity();
  this->linearStepper = localLinearStepper;
  return (linearStepper || rssiCycleManagerOK);
}

#ifdef RF_ACTIVATE_API_MODE
bool EtheraSecondaryMissionController::begin(CansatXBeeClient* xbeeClient) {
  if (!SecondaryMissionController::begin(xbeeClient)) return false;
  return initEthera();
}
#else
bool EtheraSecondaryMissionController::begin(Stream* RF_Stream) {
	if (!SecondaryMissionController::begin(RF_Stream)) return false;
	return initEthera();
}
#endif


float EtheraSecondaryMissionController::getAverageVelocity() {
  if (velocityAverageValid == true) {
    return averageVelocity;
  }
  else {
    return InvalidVelocity;
  }
}

bool EtheraSecondaryMissionController::takeoffDetected() {
  return takeoffHappened;
}
