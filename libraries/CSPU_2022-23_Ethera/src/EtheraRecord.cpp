// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatConfig.h"
#include "EtheraRecord.h"

#define DBG_BINARY 0

void EtheraRecord::printCSV_SecondaryMissionData(Stream & str, bool startWithSeparator, bool finalSeparator) const {
  if (startWithSeparator) str << separator;
  str << (uint8_t) flightControlCode << separator;
  str << (uint8_t) brakePosition << separator;
  str << (uint8_t)flightPhase << separator;
  str.print((float) frequency/10.0f, 1);
  str << separator;
  str.print((float) rssi/10.0f, 1);
  str << separator;
  str << (uint16_t) distance;

  if (finalSeparator) str << separator;
}

void EtheraRecord::printCSV_SecondaryMissionHeader(Stream &str, bool startWithSeparator, bool finalSeparator) const {
  if (startWithSeparator) str << separator;
  str << "flightCtrlCode,brakePosition,flightPhase,frequency,rssi,distance";
  if (finalSeparator) str << separator;
}

void EtheraRecord::clearSecondaryMissionData() {
  flightControlCode = FlightControlCode::NoData;
  brakePosition = 0;
  flightPhase = 0;
  frequency = 0;
  rssi = 0;
  distance=0;
}
uint16_t EtheraRecord::getSecondaryMissionMaxCSV_Size() const {
  return  1 /*,*/ + 1  /* flightControlCode (uint8_t) */
		  + 1 /*,*/ + 13 /*brakePosition (uint8_t)*/ + 1 /*,*/
		  + 11 /*flightPhase (uint8_t)*/ + 1 /*,*/
		  + 9  /*frequency (float)*/  + 1 /*,*/
		  + 9  /*rssi (float)*/ + 1 /*,*/
		  + 5  /*distance, uint16 */;
}

uint16_t EtheraRecord::getSecondaryMissionCSV_HeaderSize() const {
  return 1 /*,*/ + 15 /* flightCtrlCode,*/ + 49 /*brakePosition,flightPhase,frequency,rssi,distance (amount of characters including comma)*/;
}

void EtheraRecord::printSecondaryMissionData(Stream& str) const {
  str << "flightCtrlCode: " << (uint8_t) flightControlCode << ENDL;
  str << " brakePosition: " << (uint8_t) brakePosition << ENDL;
  str << "   flightPhase: " << (uint8_t) flightPhase << ENDL;
  str << "     frequency: " << (float) frequency/10.0f << " MHz" << ENDL;
  str << "          rssi: " << (float) rssi/10.0f << " dBm" << ENDL;
  str << "      distance: " << distance << "m" << ENDL;
}

uint8_t EtheraRecord::writeBinarySecondaryMissionData(uint8_t* const destinationBuffer, uint8_t bufferSize) const {
  uint8_t written = 0;
  uint8_t* dst = destinationBuffer;
  uint8_t remaining = bufferSize;
  written += writeBinary(dst, remaining, (uint8_t) flightControlCode);
  written += writeBinary(dst, remaining, (uint8_t) brakePosition);
  written += writeBinary(dst, remaining, (uint8_t) flightPhase);
  written += writeBinary(dst, remaining, (uint16_t) frequency);
  written += writeBinary(dst, remaining, (int16_t) rssi);
  written += writeBinary(dst, remaining, (uint16_t) distance);

  DPRINTS(DBG_BINARY, " writeBinary: brakePosition=");
  DPRINTLN(DBG_BINARY, (uint8_t) brakePosition);
  DPRINTS(DBG_BINARY, " writeBinary: flightPhase=");
  DPRINTLN(DBG_BINARY, (uint8_t) flightPhase);
  DPRINTS(DBG_BINARY, " writeBinary: frequency=");
  DPRINTLN(DBG_BINARY, (uint16_t) frequency);
  DPRINTS(DBG_BINARY, " writeBinary: rssi=");
  DPRINTLN(DBG_BINARY, (int16_t) rssi);
  DPRINTS(DBG_BINARY, " writeBinary: distance=");
  DPRINTLN(DBG_BINARY, (uint16_t) distance);


  return written;
}

uint8_t EtheraRecord::readBinarySecondaryMissionData(const uint8_t* const sourceBuffer, const uint8_t bufferSize) {
  uint8_t read = 0;
  const uint8_t* src = sourceBuffer;
  uint8_t remaining = bufferSize;
  uint8_t tmpUInt8;
  read += readBinary(src, remaining, tmpUInt8);
  flightControlCode= (FlightControlCode) tmpUInt8;
  read += readBinary(src, remaining, brakePosition);
  read += readBinary(src, remaining, flightPhase);
  read += readBinary(src, remaining, frequency);
  read += readBinary(src, remaining, rssi);
  read += readBinary(src, remaining, distance);

  DPRINTS(DBG_BINARY, " readBinary: brakePosition=");
  DPRINTLN(DBG_BINARY, (uint8_t) brakePosition);
  DPRINTS(DBG_BINARY, " readBinary: flightPhase=");
  DPRINTLN(DBG_BINARY, (uint8_t) flightPhase);
  DPRINTS(DBG_BINARY, " readBinary: frequency=");
  DPRINTLN(DBG_BINARY, (uint16_t) frequency);
  DPRINTS(DBG_BINARY, " readBinary: rssi=");
  DPRINTLN(DBG_BINARY, (int16_t) rssi);
  DPRINTS(DBG_BINARY, " readBinary: distance=");
  DPRINTLN(DBG_BINARY, (uint16_t) distance);

  return read;
}

uint8_t EtheraRecord::getBinarySizeSecondaryMissionData() const {
  return sizeof(uint8_t /* flightControlCode */)
		  + sizeof(uint8_t /* brakePosition */)
		  + sizeof(uint8_t /* flightPhase */)
		  + sizeof(uint16_t /*frequency*/)
		  + sizeof(int16_t /* rssi */)
		  + sizeof(uint16_t) /* distance */ ;
}
