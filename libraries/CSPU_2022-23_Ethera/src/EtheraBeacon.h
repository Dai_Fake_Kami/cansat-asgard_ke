#pragma once
#include "elapsedMillis.h"
#include "RH_RF69.h"

/** @ingroup EtheraCSPU
 *  A class implementing a beacon which sends a standard message at regular intervals.
 *  The content of the message is "n Ethera Beacon ts=xxxxx ****************"
 *  where:	n is the beaconID
 *  		xxxx is the current timestamp
 *  		the message is RH_RF69_MAX_MESSAGE_LEN bytes long (last ones filled with '*'
 *  		and null-terminated.
 */
class EtheraBeacon {
  public:
    EtheraBeacon();
    bool begin(int8_t id);
    void run();

protected:
    /** Initialize the content of the message sent by the beacon */
	void initBeaconMessage();
	/** Update the value of the timestamp included in the message. */
	void setInfoInMsg();

  private:
    int8_t beaconID;
    uint32_t seqNumber;
    elapsedMillis elapsedSinceLastMsg;
    RH_RF69 rf69;
    char beaconMessage[RH_RF69_MAX_MESSAGE_LEN];
    static constexpr const char *MsgPrefix = "  Ethera Beacon"; // Character 1 is for beacon ID.

};
