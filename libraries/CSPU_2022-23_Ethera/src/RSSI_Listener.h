/*
   RSSI_Listener.h
*/

#pragma once
#include "RH_RF69.h"
#include "EtheraConfig.h"
/**
 * @ingroup EtheraCSPU
 * The class implementing the logic for listening in a particular channel.
 */
class RSSI_Listener {

  public:
    enum class ListenerStatus {
      Uninitialized = 0,
      ReadyForRSSI = 1,
      ReadyForDistance = 2
    };

    /** Initialize the listener with all required paramenters except the x0-RSSI0 pair.
        @param rf69  An operational instance of the RF69 class. Note: this class does NOT
                     configure or initialise this object, but sets the frequency whenever
                     it listens to the radio.
        @param frequency The frequency in MHz on which this object should work.
        @param bool useEncryption True if encryption must be used. In this case,
       	   key may not be null.
        @param key A pointer to a 16 unsigned char array to be used as encryption key.
       	   If useEncryption is false, it may be nullptr.
        @param timeoutInMsec The time-out (in msec) used whenever a message is
       	   expected from the radio. If nothing is received within this delay, the
       	   operation will be considered unsuccessful.
        @param beaconID An integer identifying the beacon being listen. It is used to check
       	   the content of the message and assert it is indeed from the expected beacon.
       	   Value -1 denotes "no beacon". In this case, no check is performed on the
       	   content of the message.
        @return True if initialisation was successful, false otherwise.
    */
    bool begin(RH_RF69& rf69, float frequency,  bool useEncryption, const unsigned char* key,
               uint16_t timeoutInMsec, int8_t beaconID = -1);

    /** Measure the current RSSI and associate it with distance x0 (in m). The couple x0/rssi0
        is further used for converting RSSI measures into distances.
        This method must be successfully called before any call to measureDistance() can
        be performed successfully.
        This method fails if begin() was not successfully call before.
        @param x0 The current distance of the source, in m.
        @param timeoutInMsec The timeout to be used when listening to the source. If
               timeoutInMsec is 0, the timeout provided in the begin() is used.
        @return True if measurement was successful, false otherwise.
    */
    bool measureRSSI0(float x0, uint16_t timeoutInMsec = 0);

    /** Configure frequency, listen for at most duration msec, and convert
        RSSI to distance, if anything is received.
        This method may not be called before mesureRSSI0 has been called.
        @param	rssi The rssi associated with the incoming message
        @param  distance The distance calculated from the rssi associated with the
       		incoming message, based on the value of x0 and RSSI0.
        @return True if a message was received, RSSI measured and converted.
                False if nothing was received of RSSI0 is not available.
    */
    bool measureDistance(float& rssi, float& distance) ;

    /** Wait for a message on the listener's frequency during the timeout duration
        provided in the call to begin, and return the corresponding RSSI indicator.
        If the beaconID  provided in the call to begin() is not -1, the content of
        the message is checked to assert that it indeed originated from the beacon.
        @param rssi  The measured RSSI
        @param timeoutInMsec The timeout to be used when listening to the source. If
               timeoutInMsec is 0, the timeout provided in the begin() is used.
        @return true if a message was received, possibly validated and a corresponding
                RSSI value returned. False if begin() was never called, if mesureRSSI0()
                was not successfully called, of if no valid message was received within
                the timeout duration.
    */
    bool getRSSI(float& rssi, uint16_t timeoutInMsec = 0);

    /** Convert RSSI measurement into distance from source, using rssi0 and x0
        @param rssi The RSSI to convert
        @param distance The resulting distance.
        @return true if calculation was successful, false otherwise (e.g. if
       		rssi0 / x0 is not available.
    */
    bool convertRSSI_ToDistance(float rssi, float& distance);

    /** Obtain the current status of the object.
        @return The current status.
    */
    
    ListenerStatus getStatus() {
      return status;
    };

    /** Configure the RF module for this RSSI_Listener. This method checks the
     *  currentFrequency static member and actually configures the RF69 module
     *  only in case the current frequency is not the right one.
     *  @return true if configuration was successful, false otherwise
     */
    bool configureRF();

    /** Set the values of x0 and RSSI0 (overwriting possible previous ones obtained
     *  by a previous call to setReferenceValues() or to measureRSSI0().
     *  This method fails if the status is not at least ReadyForRSSI() (ie if begin()
     *  was not successful and changes the status to readyForDistance.
     *  @param x0  The reference distance in m
     *  @param rssi0 The reference rssi measure at distance x0 (in dBm)
     *  @return true if everything ok, false otherwise.
     */
    bool setReferenceValues(float x0, float rssi0);

    /** Obtain the frequency being monitored..
        @return The frequency in MHz.
    */
    float getFrequency() {
      return frequency;
    };

    
    float getRSSI0() {
      return rssi0;
    }
    
  private:
    /** Check whether the object status is at least minStatus. If not, issue a debug
        message and return false.
        @param minStatus: The minimum expected status value
        @return true if current status >= minStatus, false otherwise.
    */
    bool checkStatus(ListenerStatus minStatus);

    float rssi0;	   /**< The reference RSSI, corresponding to distance x0. */
    float x0 = 0;		   /**< The reference distance, corresponding to rssi0. Set to 0
							until a successful call to mesureRSSI0() is performed. */
    float k0;
    uint16_t timeout;  /**< The timeout in msec to be used for measurements */
    int8_t beaconID = -1; /**< The ID of the beacon being listen to (-1 = no beacon). */
    float frequency = 0.0f; /**< The frequency on which this listener operated. set to
							   0 until begin() was successfully called. */
    bool useEncryption;   /**< If true the encryption key is used whenever listening to the
							   RF interface. */
    unsigned char key[BeaconEncryptionKeySize];	  /**< A The key to be used if
							   encryption is activated. */
    RH_RF69 *rf69 = nullptr; /**< The address of the fully initialised RFM69 instance */
    ListenerStatus status = ListenerStatus::Uninitialized;	 /**< The current state of the object */
    static float currentFrequency;   /**< This variable is shared by all instances of
    									  RSSI_Listener, which assume that only the RSSI_Listeners
    									  modify the RF69 configuration. It is used to avoid
    									  configuring the receiver when the frequency did not change. */
};
