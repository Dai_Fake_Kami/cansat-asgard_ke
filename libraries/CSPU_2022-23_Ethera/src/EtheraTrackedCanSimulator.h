#pragma once
#include "elapsedMillis.h"
#include "RH_RF69.h"

/** @ingroup EtheraCSPU
 *  A class simulating a can tracked by Ethera. It sends a configurable message at regular intervals.
 *  The content of the message, period, frequency and can name are defined in EtheraConfig.h
 */
class EtheraTrackedCanSimulator {
  public:
	EtheraTrackedCanSimulator();
    bool begin(int8_t id);
    void run();

protected:
    /** Initialize the content of the message sent by the beacon */
	void initMessage();

  private:
    int8_t canID;
    elapsedMillis elapsedSinceLastMsg;
    RH_RF69 rf69;
    char message[RH_RF69_MAX_MESSAGE_LEN];
};
