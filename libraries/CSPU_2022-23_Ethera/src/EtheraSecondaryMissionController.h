/*
   EtheraSecondaryMissionController.h
*/

#pragma once
#include "float.h"
#include "CansatXBeeClient.h"
#include "SecondaryMissionController.h"
#include "EtheraRecord.h"
#include "EtheraConfig.h"
#include "BMP_Client.h"
#ifdef RF_ACTIVATE_API_MODE
#  include "CansatXBeeClient.h"
#endif
#include "LinearStepper.h"
#include "RSSI_CycleManager.h"

/** @ingroup EtheraCSPU
 * @brief our secondary mission manager for the Ethera Project
  This class can :
  - use the velocity in the record to make a moving average
  - check if the velocity in the record are valid
  - check if the average is valid
  - put the velocity values in a table and clear it if he receives an invalid velocity
  - detects the takeoff
  - manages airbrakes and RSSI monitoring
  By doing this last action, this class can actually modify one of the data contained in the records.
*/
class EtheraSecondaryMissionController: public SecondaryMissionController {

  public:
    EtheraSecondaryMissionController() :
      linearStepper(nullptr),
      takeoffTimestamp(0),
      velocityAverageValid(false),
      takeoffHappened(false) { };
    /** Initialize the controller before use
        @return true if initialization is successful, false otherwise
    */
#ifdef RF_ACTIVATE_API_MODE
    virtual bool begin(CansatXBeeClient* xbeeClient = nullptr) override;
#else
    virtual bool begin(Stream* RF_Stream = nullptr);
#endif

    void manageSecondaryMission(CansatRecord & record) override;

    LinearStepper * getLinearStepper() {
      return linearStepper;
    };


    float getAverageVelocity(); /**< return the velocity average if it is valid **/
    static constexpr uint8_t NumberOfVelocitySamples = (VelocityAveragingPeriod * 1000 / CansatAcquisitionPeriod) + 1; /**< this is the number of the samples in order to fill entirely a table and get a valid velocity average */

    static constexpr float InvalidVelocity = FLT_MAX;
    bool takeoffDetected(); /**< true if the takeoff has been detected */
  protected:

  private:
     /** Initialize all Ethera-specific resources
      * @return True if initialisation ok, false otherwise.
      */
     bool initEthera();

    /** This method stores the velocity within the record in a table and averages it. Then, it checks if velocities got are valid and then allows us to know if the takeoff has already happened.
      @param record containing the descent velocity
    */
    void updateStatus(EtheraRecord & record);
    /** Update airbrakes position based on flight status (as updated by updateStatus()
     *  and populate the record accordingly.
     */
    void manageAirBrakes(EtheraRecord &record);
    void clearAverageVelocity(); /**< clear up all data of the table */
    void printVelocityTable(); /**< it prints the table with the descent velocity variable within the record */

    RSSI_CycleManager rssiCycleManager; /**< The class managing RSSI measures */
    bool rssiCycleManagerOK;	    /**< If false, the RSSI cycle manager is not used */
    LinearStepper* linearStepper;	/**< The interface to the linear steppers controlling the airbrakes.
     	 	 	 	 	 	 	 	 	 If null, air-brakes are not managed. */
    unsigned long takeoffTimestamp; /**< timestamp of the takeoff */
    float velocityTable[NumberOfVelocitySamples] = {}; /**< table used to store the velocity values of the record */
    float averageVelocity = 0; /**< current average of the velocity in the table*/
    uint8_t currentVelocityIndex = 0; /**< indicates which index or row of the table we are in */
    bool velocityAverageValid; /**< if true when the table is fully filled with  valid velocity values */
    unsigned long lastRecordTimestamp = 0; /**< timestamp of the current last record */
    bool takeoffHappened; /**< if true the takeoff of the rocket is detected */
    FlightControlCode flightStatus;
};
