/**
    A program just running the EtheraSecondaryMissionController with the period configured in the CansatConfig.h.

*/
#include "EtheraSecondaryMissionController.h"
#include "elapsedMillis.h"
#include "EtheraRecord.h"

#define RUN_GPS   // Define to include the GPS in order to check for interrupt conflicts.
#define RUN_STORAGE_MGR // Define to initialize the SD-Card, logger etc.
#define STORE_TO_SD // Define to actually store records on SD card.
				   // 20220207: writing to SD prevents any reception from RHRF69.
				   //           calling noInterrupts() / interrupts() in
				   //           SpSpiDriver.h (activate()/deactivate) does NOT
				   // 			fix the issue.
				   // 			Surrounding storageMgr.storeOneRecord() with
				   //           noInterrupts()/interrupts() does not FIX the issue.
				   // Replacing RadioHead library with the one forked by Adafruit fixes the problem!

#ifdef RUN_GPS
#include "Serial2.h"
// GPS is included to check for interrupt conflicts.
#include "GPS_Client.h"
GPS_Client gps(GPS_SerialPortNumber == 1 ? Serial1 : Serial2);
#endif

#ifdef RUN_STORAGE_MGR
#include "StringStream.h"
#include "StorageManager.h"
#include "CansatHW_Scanner.h"
#include "CansatXBeeClient.h"
CansatXBeeClient xbc(GroundXBeeAddressSH, GroundXBeeAddressSL);
StorageManager storageMgr(CansatAcquisitionPeriod, CansatCampainDuration);
CansatHW_Scanner hw;
#endif

constexpr bool DisplayAllRecords = true; // If true ALL records are displayed, if false, only the record with an
// an actual RSSI measurement are displayed.

EtheraSecondaryMissionController ctrl;
EtheraRecord record;
elapsedMillis elapsed;

void setup() {
  DINIT(115200);
  Serial << "Test of EtheraSecondaryMissionController with period=" << CansatAcquisitionPeriod << " msec." << ENDL;

  if (!ctrl.begin()) {
    Serial << "*** Error initializing the EtheraSecondaryMissionController" << ENDL;
    exit(-1);
  }
  record.clear();

#ifdef RUN_GPS
  Serial << "--- Running GPS Cliet ---" << ENDL;
  // Initialise GPS to check for interrupt conflicts
  gps.begin((GPS_Client::Frequency) CansatGPS_Frequency, pin_GPS_Enable, true);
#endif

#ifdef RUN_STORAGE_MGR
  Serial << "--- Running the storage manager ";
#ifdef STORE_TO_SD
  Serial << " AND actually storing." << ENDL;
#else
  Serial << " BUT not actually storing." << ENDL;
#endif
  hw.init();
  String CSV_Header;
  CSV_Header.reserve(200);
  StringStream sstr(CSV_Header);
  record.printCSV(sstr, CansatRecord::DataSelector::All, CansatRecord::HeaderOrContent::Header);
  storageMgr.init(	record,
		  	  	  	&hw,
					CSV_Header,
		  	  	  	SD_FilesPrefix,
					SD_CardChipSelect,
					SD_RequiredFreeMegs,
					EEPROM_KeyValue);
   Serial << F("\nLogging to ") << storageMgr.getSD_FileName() << ENDL;

#endif
  Serial << "Initialisation OK." << ENDL;
}

void loop() {
  if (elapsed > CansatAcquisitionPeriod) {
    elapsed = 0;
    record.clear();
    ctrl.manageSecondaryMission(record);
    if (DisplayAllRecords || (record.hasRSSI_Data())) {
      Serial << "Freq=" << record.getFrequency() << " MHz, RSSI=" << record.getRSSI()
             << " dBm, distance=" << record.distance << "m" << ENDL;
    }

#ifdef RUN_GPS
    gps.readData(record);
    record.printCSV(Serial);
    Serial.println();
#endif

#if (defined(RUN_STORAGE_MGR) && defined(STORE_TO_SD))
    Serial << "Storing..." << ENDL;
    auto start=millis();
    noInterrupts();
    storageMgr.storeOneRecord(record);
    interrupts();
    auto stop=millis();
    Serial << "Stored in " << stop - start << " msec" << ENDL;
#endif
  }

}
