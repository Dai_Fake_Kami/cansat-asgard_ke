#include <RH_RF69.h>
#include "EtheraConfig.h"
#include "CSPU_Debug.h"
#include <RHReliableDatagram.h>
#include <math.h>
#include "RSSI_Listener.h"

RSSI_Listener listener;

// 2 constant needed
// set the beacon to 3 meter
// need to tell to enter manually data in
#if defined(ADAFRUIT_FEATHER_M0_EXPRESS) // FEATHER_M0_EXPRESS w/ radio
// Feather M0 w/Radio
#define RFM69_CS      A3
#define RFM69_INT     5
#define RFM69_RST     A4
#define LED           13
#else
#error "This program only works for FeatherM0 Express and Arduino Uno"
#endif

RH_RF69 rf69(RFM69_CS, RFM69_INT);



constexpr float RF69_Frequency = BeaconFrequency[0]; // Mhz

const int maxIrrelevantValue = 0;
const int minIrrelevantValue = -100;
float zeroConstants;
float rssiZero = 0;
int distanceZero;// meter
const int NumSamples = 5;




void inputX0_Value() {

  Serial << " enter the distance between the receiver and the beacon an integer in meter (distanceZero= " << ENDL;
  while (distanceZero <= 0) {
    if (Serial.available()) {
      distanceZero = Serial.parseInt();
    }
  }
  Serial << " x0 = " << distanceZero << "m" << ENDL;
  if (!listener.measureRSSI0(distanceZero, 1000)) {
    Serial << "ERROR COMPUTING RSSI0" << ENDL;
    exit(-1);
  }
}

float calculateTheoreticalRSSI(float x, float x0, float rssi0) {
  return (rssi0 + 20 * log10(x0) - 20 * log10(x));
}

void testConversionAccuracy(float x0, float rssi0)
{
  Serial << "Testing the accuracy of the Conversion for x0="  << x0 << " and rssi0 = " << rssi0 << ENDL;
  if (!listener.setReferenceValues(x0, rssi0)) {
    Serial << "Error setting reference valures" << ENDL;
    return;
  }
  Serial << "rssi0 is : " << rssi0 << ENDL;
  Serial << " x0 is : " << x0 << ENDL;
  for (float x = 10; x <= 150; x += 50) {
    Serial << "Testing for x = " << x << "m." << ENDL;
    float rssi = calculateTheoreticalRSSI(x, x0, rssi0);
   
    float convertedX;
    if (!listener.convertRSSI_ToDistance(round(rssi), convertedX))
    {
      Serial << "Error while converting rssi " << rssi << ENDL;
      Serial << "Error on conversion : " << x - convertedX << ENDL;
      return;
    }
    Serial << "    calculated rssi = " << rssi << ", rounded to " << round(rssi) << ENDL;
    Serial << "    calculated distance : " << convertedX << ENDL;
    Serial << "    error = " << convertedX - x << ENDL;

  }
}

void setup() {
  DINIT(115200);
  pinMode(LED, OUTPUT);
  pinMode(RFM69_RST, OUTPUT);
  digitalWrite(RFM69_RST, LOW);

  Serial.println("Ethera test distance converter RFM69");
  Serial.println();

  // manual reset
  digitalWrite(RFM69_RST, HIGH);
  delay(10);
  digitalWrite(RFM69_RST, LOW);
  delay(10);


  DINIT(115200);
  if (!rf69.init()) {
    Serial << "RFM69 radio init failed. Aborted." << ENDL;
    while (1);
  }
  Serial.println("RFM69 radio init OK!");
  listener.begin(rf69, BeaconFrequency[0], BeaconUseEncryption[0], BeaconEncryptionKey[0], RSSI_MeasurementTimeout, 0) ;

  testConversionAccuracy(100, -35.0);

  exit(0);
  inputX0_Value();
  float testDistance;
  listener.convertRSSI_ToDistance(listener.getRSSI0(), testDistance) ;
  Serial << "Recomputing x0: " << testDistance << ENDL;
}



void loop() {
  float rssi;
  float distance;

  if (listener.measureDistance(rssi, distance)) {
    Serial << "you are at " << distance << "m of your beacon" << ENDL;
    Serial << "the rssi is " << rssi << ENDL;
    delay(2000);
  }

}
