#include "EtheraBeacon.h"
#include "EtheraConfig.h"
#include "CSPU_Debug.h"

int8_t beaconID = 0;

void setup() {

 EtheraBeacon::begin(beaconID);
}

void loop() {
  EtheraBeacon::run();

}
