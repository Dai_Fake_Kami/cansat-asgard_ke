## CANSAT-ASGARD aliases
## Call this script from your .bash_profile after having exported variables
## CANSAT_ROOT, CANSAT_GROUND_ROOT, CANSAT_BUILD_FILES, CANSAT_GROUND_BIN, CANSAT_DEFAULT_FQBN

echo "...cansat-asgard/utils/shellScripts/bashAliases..."

export CANSAT_CURRENT_PROJECT=CanSat2021-22_GMini
export PATH=$PATH:$CANSAT_ROOT/utils/shellScripts:$CANSAT_GROUND_BIN
if [ -z ${CANSAT_DEFAULT_FQBN+x} ]; then 
	export CANSAT_DEFAULT_FQBN="adafruit:samd:adafruit_feather_m0_express"
	
	echo "   Default board set to $CANSAT_DEFAULT_FQBN for arduino-cli"
fi 
export ONE_DRIVE_LIB_FOLDER="~/Library/CloudStorage/OneDrive-Bibliothèquespartagées-CollègeSaint-PierreUccle"
export CANSAT_DOC_ROOT="$ONE_DRIVE_LIB_FOLDER/Cansat\ -\ General"

# cd aliases
alias cst="cd $CANSAT_ROOT"
alias cstg="cd $CANSAT_GROUND_ROOT"
alias cstlib="cd $CANSAT_ROOT/libraries"
alias cstdb="cd $CANSAT_ROOT/libraries/DebugCSPU"
alias cstca="cd $CANSAT_ROOT/libraries/cansatAsgardCSPU"
alias cstdoc="cd $CANSAT_DOC_ROOT"
alias cstdocp="cd $CANSAT_DOC_ROOT/$CANSAT_CURRENT_PROJECT"
alias killtni="kill  \`ps -A | grep activh | grep -v grep | cut -c 1-6 \`"
alias greptni="ps -A | grep activh | grep -v grep "
alias cd1900="cd $CANSAT_DOC_ROOT/$CANSAT_CURRENT_PROJECT/1000_*/1900_*"
# Miscellaneous
alias dbf="rm -Rf $CANSAT_BUILD_FILES/*; echo 'Build files cleared'"
alias rtp="cd $CANSAT_GROUND_ROOT ;$CANSAT_GROUND_BIN/RT-Processing"

# Alias for arduino-cli
# Compilation
alias clic="echo '=============== Compiling for $CANSAT_DEFAULT_FQBN... ================'; arduino-cli compile --fqbn $CANSAT_DEFAULT_FQBN --warnings all"
alias clici="echo '=============== Compiling for ItsyBitsy M4... ================'; arduino-cli compile --fqbn adafruit:samd:adafruit_itsybitsy_m4 --warnings all"
alias clicf="echo '=============== Compiling for Feather M0 Express... ================'; arduino-cli compile --fqbn adafruit:samd:adafruit_feather_m0_express --warnings all"
alias clicu="echo '=============== Compiling for Genuino Uno... ================'; arduino-cli compile --fqbn arduino:avr:uno --warnings all"
# Uploading
alias cliu="echo '=============== Uploading to $CANSAT_DEFAULT_FQBN... ================';arduino-cli upload --fqbn $CANSAT_DEFAULT_FQBN" 
alias cliui="echo '=============== Uploading to ItsyBitsy M4... ================';arduino-cli upload --fqbn adafruit:samd:adafruit_itsybitsy_m4" 
alias cliuf="echo '=============== Uploading to Feather M0 Express... ================'; arduino-cli upload --fqbn adafruit:samd:adafruit_feather_m0_express"
alias cliuu="echo '=============== Uploading to Genuino Uno... ================'; arduino-cli upload --fqbn arduino:avr:uno"
#checking library dependency of a sketch
alias cstdep="echo '=============== Listing sketch dependencies ($CANSAT_DEFAULT_FQBN)... ================';arduino-cli compile --fqbn $CANSAT_DEFAULT_FQBN --only-compilation-database -v |grep 'Compiling library'"

# Datya processing utilities
alias gend=generateGMiniFile.py
alias splitd=processRecordFile.py
