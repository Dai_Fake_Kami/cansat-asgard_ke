/*
    echoSerialOnUSB

    A small utility which dumps serial data received on Serial1 RX (or pin RX_DATA on Uno) to the USB Serial, 
    and sends serial data received on USB Serial on Serial1 TX (or pin TX_DATA on Uno). 

*/
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

constexpr byte RX_DATA = 10; // Serial RX in case a Uno board is used.
constexpr byte TX_DATA = 11; // Serial TX in case a Uno board is used.

#ifndef ARDUINO_ARCH_SAMD
#include "SoftwareSerial.h"
SoftwareSerial Serial1(RX_DATA, TX_DATA);
#endif

#define DEBUG_CSPU
#include "DebugCSPU.h"

void setup() {
  DINIT(115200);
  Serial1.begin(115200);

  Serial << "Init ok." << ENDL;
#ifndef ARDUINO_ARCH_SAMD
  Serial << "Echoing with SoftwareSerial, from pin #" << RX_DATA << " to USB Serial" << ENDL;
  Serial << "Echoing with SoftwareSerial, from USB Serial to pin #" << TX_DATA << " to USB Serial" << ENDL;
#else
  Serial << "Echoing from hardware Serial 1 to USB Serial and conversely" << ENDL;
#endif
}

void loop() {
  while (Serial1.available()) {
    Serial << (char) Serial1.read();
  }
  while (Serial.available()) {
    Serial1 << (char) Serial.read();
  }
}
