#include <Wire.h>

#include <sdpsensor.h>

SDP8XXSensor sdp;

void setup() {
  Wire.begin();
  Serial.begin(9600);
  delay(1000); // let serial console settle
  do {
    int ret = sdp.init();
    if (ret == 0) {
      Serial.print("init(): success\n");
      break;
    } else {
      Serial.print("init(): failed, ret = ");
      Serial.println(ret);
      delay(1000);
    }
  } while (true);
}

void loop() {
  int ret = sdp.readSample();
  if (ret == 0) {
    float AirSpeed;
    AirSpeed = sqrt(2 * fabs(sdp.getDifferentialPressure()) / 1.225); // 0°C = 1.292 / 5°C = 1,269 / 10°C = 1.247 / 15°C = 1.225 / 20°C = 1.204
    Serial.print("AirSpeed: ");
    Serial.print (AirSpeed);
    Serial.print("m/s, ");
    Serial.print("Differential pressure: ");
    Serial.print(sdp.getDifferentialPressure());
    Serial.print("Pa | ");

    Serial.print("Temp: ");
    Serial.print(sdp.getTemperature());
    Serial.print("C\n");
  } else {
    Serial.print("Error in readSample(), ret = ");
    Serial.println(ret);
  }

  delay(2500);
}
