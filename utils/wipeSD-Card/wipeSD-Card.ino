/*
 * Utility script to clear an SD-Card
 * 
 */
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#define DEBUG_CSPU
#include "DebugCSPU.h"
 #include "SdFat.h"

constexpr byte SD_chipSelect=4;  // 13 for RF-Transceiver, 12 for slave (IsaTwo), 4 for Master (IsaTwo) and
    // any other Feather Adalogger. 
 SdFat mySD;
 
 void setup() {
 DINIT(115200);
 if (!mySD.begin(SD_chipSelect)) {
    Serial << "*** SD.begin() failed. CS=" << SD_chipSelect << ENDL;
    while(1) delay(100);
 } else {
  Serial << "SD init: OK" << ENDL;
 }
 
 Serial << " This utility will erase any file from the SD-Card." << ENDL;
 Serial << "Proceed (y/n) ? " ;
 while (Serial.available()) {
  Serial.read();
 }
 bool done=false;
 bool proceed=false;
 
 while (!done) {
     if (Serial.available()) {
        char c= Serial.read();
        if (c == 'y') {
            proceed=true;
        }
        done = true;
     }
 } // while
 if (proceed) {
    Serial << ENDL << "Wiping volume..." << ENDL;
    mySD.wipe(&Serial);
    Serial << ENDL << "Done. " << ENDL;
 }
 else Serial << ENDL << "Cancelled" << ENDL;
 
}

void loop() {
  delay(1000);

}
