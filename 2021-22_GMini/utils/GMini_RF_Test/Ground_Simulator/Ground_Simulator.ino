/**
  Sketch simulating RT transmission from subcan 1.
  Test with Feather M0 or ItsyBitsy M4 board.

  The contents of the records are defined in GMiniRecordExample class .h
  (because it is shared by the various sketches.

  Wiring: µC to XBee module.
       Feather/ItsyBitsy
       3.3V to VCC
       RX   to DOUT  CONFIRMED although it is the opposite to connect to XCTU!
       TX   to DIN   CONFIRMED although it is the opposite to connect to XCTU!
       GND  to GND

*/

// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#ifndef ARDUINO_ARCH_SAMD
#  error "This program only works on SAMD boards (Feather MO_Express, ItsyBitsy M4 Express, etc.)"
#endif

#include "GMiniConfig.h"
#include "GMiniXBeeClient.h"
#include "GMiniRecordExample.h"
#include "CSPU_Test.h"

#ifndef RF_ACTIVATE_API_MODE
#  error "This program only works if RF_ACTIVATE_API_MODE is defined in CansatConfig.h"
#endif
#undef DEBUG_TS   // Define to print out timestamps in incoming records

constexpr uint32_t  DefaultDestinationSH = GM_MainCan_XBeeAddressSH;
constexpr uint32_t  DefaultDestinationSL = GM_MainCan_XBeeAddressSL;
const char * InterruptionInstructions = "Send 's' to print stats, 'z' to reset stats, or any other character to get help";
constexpr unsigned int CommandEmissionPeriod = 3000; // ms. Period for sending a pseudo-command to the can.
constexpr bool RegularlyDisplayChannel = false; // if true, the current channel is displayed
//                                                 every ChannelPrintPeriod ms (debug: disrupts
//                                                 RF link)
constexpr unsigned int ChannelPrintPeriod = 20000; // ms
constexpr unsigned int StatsPrintPeriod = 30000; // ms
constexpr unsigned int MaxLineLength = 100;
constexpr bool DetailedErrorMsgOnEmission = false; // if true, detailed error message when failing
//                                                    to send to ground.  (debug: disrupts
//                                                    RF link)
constexpr uint16_t RF_AckDelay = 5; // ms

// ----------------- Globals ----------------------
HardwareSerial &RF = Serial1;
GMiniXBeeClient xbc(DefaultDestinationSH, DefaultDestinationSL); // Defined in GMiniConfig.h
GMiniRecordExample myRecord, incomingRecord;
char myString[xbc.MaxStringSize];
CansatFrameType stringType;
bool cmdInterrupted = true;
char myself[25];
unsigned long numRecordsOK[5] = {0, 0, 0, 0}; // Count OK records from main can & subCans
unsigned long numRecordsKO[5] = {0, 0, 0, 0}; // Count KO records from main can & subCans
unsigned long numMsgToCanOK = 0; // Count OK records to ground
unsigned long numMsgToCanKO = 0; // Count KO records to ground
unsigned long charCounter = 0;  // character counter to split lines.

elapsedMillis elapsed = 1000, elapsedSinceChannelPrint = 0, statsElapsed, statsDuration;
unsigned long numErrors = 0;

// -------------------------------------------------------------------
void  printStats(bool forced = false) {
  if (forced || (statsElapsed > StatsPrintPeriod)) {
    statsElapsed = 0;
    auto duration = statsDuration;
    Serial << ENDL << "Ground stats (last " << duration / 1000.0 << " s): " << ENDL;
    Serial <<   "  From Main Can: " << numRecordsKO[0] << "/" << numRecordsKO[0] + numRecordsOK[0]
           << " = " << (float) (100.0 * numRecordsKO[0]) / (numRecordsOK[0] + numRecordsKO[0]) << "% errors, avg. period="
           << (float) duration /numRecordsOK[0]  << " msec" << ENDL;
           // NB: period is only about received records.

    for (int i = 1; i <= 4 ; i++) {
      Serial << "  From SubCan " << i << ": " << numRecordsKO[i] << "/" << numRecordsKO[i] + numRecordsOK[i]
             << " = " << (float) (100.0 * numRecordsKO[i]) / (numRecordsOK[i] + numRecordsKO[i]) << "% errors, avg. period="
             << (float) duration / numRecordsOK[i]  << " msec" << ENDL;
    }
    Serial << "  To can       : " << numMsgToCanKO << "/" << numMsgToCanKO + numMsgToCanOK
           << " = " << (float) (100.0 * numMsgToCanKO) / (numMsgToCanOK + numMsgToCanKO) << "% errors, avg. period="
           << (float) duration / numMsgToCanOK  << " msec" << ENDL;
  }
}

/* Check all data members, check timestamp is previous timestamp+1
   return true if ok */
bool checkRecord(GMiniRecordExample& rec) {
  bool dataOK = rec.checkValues(true);
  static unsigned long currentTS[5] = {0, 0, 0, 0}; // the timestamp expected in next record.

  if ((int) rec.sourceID > 3) {
    Serial << ENDL << myself << ": Error record.sourceID: got " << (int) rec.sourceID << " (ignored)." << ENDL;
    return false;
  }
  bool tsOK = false;
  if (currentTS[(int) rec.sourceID] == 0) {
    currentTS[(int)rec.sourceID] = rec.timestamp + 1;
    numRecordsOK[(int)rec.sourceID]++;
    tsOK = true;
  } else {
#ifdef DEBUG_TS
    Serial << ENDL << "sourceID=" << (int) rec.sourceID << ", rec.ts=" << rec.timestamp;
    Serial << ", currentTS[sourceID]=" << currentTS[(int) rec.sourceID] << ENDL;
#endif
    if (dataOK) numRecordsOK[(int)rec.sourceID]++;  // The record we received is ok. The lost ones are nok
    if (rec.timestamp == currentTS[(int) rec.sourceID]) {
      tsOK = true;
    } else {
      int lost = rec.timestamp - currentTS[(int)rec.sourceID];
      Serial << ENDL << myself << ": Error in timestamp: expected " << currentTS[(int)rec.sourceID] << ", got " << rec.timestamp << ENDL;
      if (lost > 0) {
        Serial << myself << ": lost " << lost << " record(s)." << ENDL;
        numRecordsKO[(int)rec.sourceID] += lost;
      } else {
        numRecordsKO[(int)rec.sourceID]++;
      }
      charCounter = 0;
    }
    currentTS[(int)rec.sourceID] = rec.timestamp + 1;
  }
  if (!tsOK) numErrors++;
  if (!dataOK) numErrors++;
  return (tsOK && dataOK);
}

void processCmdResponse(char* response) {
  Serial << 'R';
  charCounter++;
  if (charCounter > MaxLineLength) {
    Serial << ENDL;
    charCounter = 0;
  }
  if (strcmp(response, "99, dummy response to dummy message")) {
    Serial << myself << ": Received incorrect '" << response
           << "' CmdResponse " << ENDL;
  }
}

void processStatusMsg(char* msg) {
  Serial << 'S';
  charCounter++;
  if (charCounter > MaxLineLength) {
    Serial << ENDL;
    charCounter = 0;
  }
  if (strcmp(msg, "Dummy spontaneous status message from the can!")) {
    Serial << myself << ": Received incorrect '" << msg
           << "' status message " << ENDL;
  }
}

void checkRF() {
  bool gotRecord;
  char incomingString[xbc.MaxStringSize + 1];
  CansatFrameType stringType;
  uint8_t seqNbr;
  while (xbc.receive(incomingRecord, incomingString, stringType, seqNbr, gotRecord)) {
    if (gotRecord) {
      charCounter++;
      if (incomingRecord.sourceID == CansatCanID::MainCan) Serial << 'M';
      else Serial << (int) incomingRecord.sourceID;
      if (!checkRecord(incomingRecord)) {
        Serial << '*';
        charCounter++;
      }
      charCounter++;
      if (charCounter > MaxLineLength) {
        Serial << ENDL;
        charCounter = 0;
      }
    } else {
      switch (stringType) {
        case CansatFrameType::CmdResponse:
          processCmdResponse(incomingString);
          break;
        case CansatFrameType::StatusMsg:
          processStatusMsg(incomingString);
          break;
        case CansatFrameType::CmdRequest:
        case CansatFrameType::StringPart:
          Serial << ENDL << myself << "Received unexpected '" << incomingString
                 << "' CmdRequest (ignored)" << ENDL;
          charCounter = 0;
          break;
        default:
          Serial << ENDL <<  "*** " << myself << ": Error: unexpected string type: "
                 << (int) stringType << ENDL;
          charCounter = 0;
      }
    }
  } // While receive();
}


void checkSerialInput() {
  const char* Instructions = "'r'=interrupt/resume cmd to can, 'd'=detect nodes, 'z'=reset stats & resume, 's'=print stats, 'x'=print xbee config., 'p'=pause app. (+<return>)...";
  char c;
  if ((c = CSPU_Test::keyPressed()) != 0) {
    switch (c) {
      case 'd':
        xbc.printDiscoveredNodes();
        Serial << ENDL << myself << ": " << Instructions << ENDL;
        break;
      case 'p':
        Serial << myself << ": application paused" << ENDL;
        CSPU_Test::pressAnyKey();
        Serial << myself << ": " << Instructions << ENDL;
        return;
      case 'r':
        if (!cmdInterrupted) {
          Serial << ENDL << myself << ": cmd emission suspended. " << Instructions << ENDL;
        }
        else Serial << ENDL << myself << ": cmd emission resumed. " << Instructions << ENDL;
        cmdInterrupted = !cmdInterrupted;
        break;
      case 's':
        printStats(true);
        Serial << ENDL <<  myself << ": " << Instructions << ENDL;
        return;
      case 'x':
        xbc.printConfigurationSummary(Serial);
        Serial << ENDL <<  myself << ": " << Instructions << ENDL;
        return;
      case 'z':
        statsDuration = 0;
        statsElapsed = 0;
        for (int i = 0; i < 4 ; i++) {
          numRecordsKO[i] = numRecordsOK[i] = 0;
        }
        numMsgToCanKO = numMsgToCanOK = 0;
        printStats(true);
        Serial << ENDL <<  myself << ": stats reset. ";
        if (cmdInterrupted) Serial << Instructions << ENDL;
        else Serial << InterruptionInstructions << ENDL;
        return;
      default:
        Serial << ENDL << myself << ": ignored '" << c << "'. " << Instructions << ENDL;
    } // switch
  } // key pressed
} // checkSerial

void printCurrentChannel() {
  uint8_t channel;
  if (xbc.queryParameter("CH", channel) )
  {
    Serial << ENDL << "Current channel: " << channel << ENDL;
  } else {
    Serial << "*** Error querying channel" << ENDL;
  }
}

void sendCommand() {
  xbc.openStringMessage(CansatFrameType::CmdRequest);
  xbc << "99, dummy command message";
  xbc.closeStringMessage();
  Serial << 'C';
}

void setup() {
  DINIT_WITH_TIMEOUT(115200);

  pinMode(LED_BUILTIN, OUTPUT);
  Serial << ENDL << ENDL;
  Serial << "***** GROUND STATION SKETCH *****" << ENDL;
  Serial << "Initializing Serials and communications..." << ENDL;
  RF.begin(115200);
  xbc.begin(RF); // no return value
  xbc.printConfigurationSummary(Serial);
  auto component = xbc.getXBeeSystemComponent();
  strcpy(myself, xbc.getLabel(component));

  // Check we have the right RF module connected.
  if (component != GMiniXBeeClient::GMiniComponent::RF_Transceiver) {
    Serial << "*** Error: wrong XBee module detected: " << myself
           << " instead of " << xbc.getLabel(GMiniXBeeClient::GMiniComponent::RF_Transceiver) << ENDL;
    Serial << "Aborted." << ENDL;
    Serial.flush();
    exit(-1);
  }
  // Check the module is properly configured
  if (!xbc.checkXBeeModuleConfiguration()) {
    Serial << "*** Error: fix module configuration with XBee_ConfigureGMiniModule.ino  before running the test!" << ENDL;
    Serial << "Aborted." << ENDL;
    Serial.flush();
    exit(-1);
  }
  // At this stage, the RF module is ok!

  Serial << "Initialisation over." << ENDL << ENDL;

  Serial << "Sending commands to MainCan (ID=" << xbc.getXBeeModuleID(DefaultDestinationSH, DefaultDestinationSL) << ", ";
  PrintHexWithLeadingZeroes(Serial, DefaultDestinationSH);
  Serial << " - ";
  PrintHexWithLeadingZeroes(Serial, DefaultDestinationSL);
  Serial << ") every " << CommandEmissionPeriod / 1000 << "sec" << ENDL;
  Serial << "RF_Strategy: '" << xbc.getLabel(GMiniSelectedRF_Strategy) << "'" << ENDL;

  Serial << ENDL << "---------------------------------------" << ENDL;
  Serial << "Starting test.... " << InterruptionInstructions << ENDL;
  Serial << " - Sending records every " << CansatAcquisitionPeriod << " ms" << ENDL;
  Serial << " - Sending a command to the can every " << CommandEmissionPeriod / 1000.0 << " sec" << ENDL;
  Serial << " - Printing statistics every " << StatsPrintPeriod / 1000.0 << " sec" << ENDL;
  Serial << " 'M' = record from main can, '1', '2' = record from subcan 1/2 " << ENDL;
  Serial << " 'C'=command sent to main can, 'R'=response received, 'S'=status msg from can" << ENDL;
  Serial << ENDL;
  statsElapsed = 0; statsDuration = 0;
}

void loop() {
  if (!cmdInterrupted) {
    if (RegularlyDisplayChannel && (elapsedSinceChannelPrint > ChannelPrintPeriod)) {
      printCurrentChannel();
      elapsedSinceChannelPrint = 0;
    }
    if (elapsed > CommandEmissionPeriod) {
      elapsed = 0;
      sendCommand();
    }
  } // ! cmdInterrupted
  // Always receive
  checkRF();
  checkSerialInput();
  CSPU_Test::heartBeat();
  printStats();
}
