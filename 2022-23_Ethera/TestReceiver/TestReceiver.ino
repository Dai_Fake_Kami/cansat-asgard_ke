/*ON ARDUINO UNO:
 * SCK = PIN 13
 * MISO = PIN 12
 * MOSI = PIN 11
 * VIN = 5V
 * GROUND = GROUND
FOR MODULE SPECIFIC PIN (G0, CHIP SELECT AND RESET) SEE DOWNWARDS
*/

/*************** Configuration ************/
constexpr float RF69_Frequency = 434.0; // Mhz

constexpr bool UseEncryption = false;
constexpr bool UseAddressedPackets = false;

/***** End of configuration *************/
#include <SPI.h>
#include <RH_RF69.h>
#include <RHReliableDatagram.h>
#include "CSPU_Debug.h"
/************ Radio Setup ***************/

#if defined (ARDUINO_AVR_UNO)  // Feather 328P w/wing
#define RFM69_INT     3  // 
#define RFM69_CS      4  //
#define RFM69_RST     2  // "A"
#define LED           13


#elif defined(ADAFRUIT_FEATHER_M0_EXPRESS) // FEATHER_M0_EXPRESS w/ radio
// Feather M0 w/Radio
#define RFM69_CS      A3
#define RFM69_INT     5
#define RFM69_RST     A4
#define LED           13
#else
#error "This program only works for FeatherM0 Express and Arduino Uno"
#endif

// Singleton instance of the radio driver
RH_RF69 rf69(RFM69_CS, RFM69_INT);
bool receivedPacket;  // set to true when a packet is actually received.


void setup()
{
  DINIT(115200);
  

  pinMode(LED, OUTPUT);
  pinMode(RFM69_RST, OUTPUT);
  digitalWrite(RFM69_RST, LOW);

  Serial.println("Ethera test receiver RFM69");
  Serial.println();

  // manual reset
  digitalWrite(RFM69_RST, HIGH);
  delay(10);
  digitalWrite(RFM69_RST, LOW);
  delay(10);

  if (!rf69.init()) {
    Serial.println("RFM69 radio init failed. Aborted.");
    while (1);
  }
  Serial.println("RFM69 radio init OK!");
  // Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM (for low power module)
  // No encryption
  if (!rf69.setFrequency(RF69_Frequency)) {
    Serial.println("setFrequency failed. Aborted.");
    while(1);
  }

  // If you are using a high power RF69 eg RFM69HW, you *must* set a Tx power with the
  // ishighpowermodule flag set like this:
 

    // The encryption key has to be the same as the one in the server
  uint8_t key[16] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
                    0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,};
  if (UseEncryption == true) {
    Serial.print("encryption key is ");
    Serial.println(key[16]);
    rf69.setEncryptionKey(key);
  }
  Serial.print("RFM69 radio @");  Serial.print((int)RF69_Frequency);  Serial.println(" MHz");

  Serial.println("timestamp, received(true or false), buf, Rssi, encryption");
}



void loop() {
  uint8_t buf[RH_RF69_MAX_MESSAGE_LEN];
  uint8_t len = sizeof(buf);
  if (rf69.waitAvailableTimeout(500))  {
    // Should be a reply message for us now
    if (rf69.recv(buf, &len)) {
      receivedPacket = true;
    } 
    else {
      Serial.println("Receive failed");
      receivedPacket = false;
      buf[0] = '\0';
    }
  }  
  else {
    
    receivedPacket = false;
    buf[0] = '\0';
  }
  Serial << millis() << ", " << receivedPacket << ", '" <<  (char *)buf << "', " << (receivedPacket?rf69.lastRssi():rf69.rssiRead()) << ENDL; 
  //(receivedPacket?rf69.rssiRead():rf69.lastRssi()) means if received packet is true use this method if not use the methode after the double dots
}
