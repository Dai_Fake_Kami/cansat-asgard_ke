#include "EtheraTrackedCanSimulator.h"
#include "EtheraConfig.h"
#include "CSPU_Debug.h"
#include "elapsedMillis.h"

const uint8_t CanID = 0;

EtheraTrackedCanSimulator simulator;
elapsedMillis hb;


void setup() {
  DINIT_WITH_TIMEOUT(USB_SerialBaudRate);
  pinMode(LED_BUILTIN, OUTPUT);
  if (!simulator.begin(CanID)) {
    Serial << "Error in simulator initialization. Aborted." << ENDL;
    while(1) {
      // Blink LED rapidly. 
      digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
      delay(100);
    }
  }
  Serial << "Initialisation over." << ENDL << ENDL;
}

void loop() {
  simulator.run();
  if (hb > 1000) {
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
    hb = 0;
  }
}
