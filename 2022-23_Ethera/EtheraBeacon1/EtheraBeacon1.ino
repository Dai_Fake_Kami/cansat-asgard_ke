#include "EtheraBeacon.h"
#include "EtheraConfig.h"
#include "CSPU_Debug.h"
#include "elapsedMillis.h"

const uint8_t BeaconID = 1;

EtheraBeacon beacon;
elapsedMillis hb;


void setup() {
  DINIT_WITH_TIMEOUT(USB_SerialBaudRate);
  pinMode(LED_BUILTIN, OUTPUT);
  if (!beacon.begin(BeaconID)) {
    Serial << "Error in beacon initialization. Aborted." << ENDL;
    while(1) {
      // Blink LED rapidly. 
      digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
      delay(100);
    }
  }
  Serial << "Initialisation over." << ENDL << ENDL;
}

void loop() {
  beacon.run();
  if (hb > 1000) {
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
    hb = 0;
  }
}
