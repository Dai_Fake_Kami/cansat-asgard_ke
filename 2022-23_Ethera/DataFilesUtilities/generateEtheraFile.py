#!/usr/local/bin/python3.7
# !/usr/bin/env python3

# import sys  # for argv
import os.path
import sys
import DataRecord
from DataRecord import M_PER_DEGREE_LONGITUDE, M_PER_DEGREE_LATITUDE
from itertools import repeat

MYSELF = os.path.basename(sys.argv[0])
TITLE = '\nCanSat data record file generator\n' \
          '---------------------------------'
USAGE = 'Usage: ' + MYSELF + ' (no option, no argument)'
OUTPUT_FILE_NAME = "EtheraTestData.csv"
FORCE_OUTPUT_FILE_OVERWRITE = True  # for development only.
START_LATITUDE = 50.80482
START_LONGITUDE = 4.34046
START_TEMP = 19.4
START_PRESSURE = 1024.3
START_ALTITUDE = 98.2
START_TS = 153288
ADD_NOISE = True
DURATION_BEFORE_HANDOVER_IN_HOURS = 1
DURATION_BEFORE_TAKE_OFF_IN_HOURS = 2
DURATION_BEFORE_RETRIEVAL_IN_HOURS = 2
totalNumberOfRecords = 0
BEACONS = [ DataRecord.Beacon(430.2, START_LATITUDE+100/M_PER_DEGREE_LATITUDE, START_LONGITUDE-100/M_PER_DEGREE_LONGITUDE, -12.3, 100),
            DataRecord.Beacon(430.8, START_LATITUDE-100/M_PER_DEGREE_LATITUDE, START_LONGITUDE+100/M_PER_DEGREE_LONGITUDE, -12.3, 100)]
MONITORED_CANS = [ DataRecord.MonitoredCan(433.1, 1, -45, DataRecord.Variation(500, 500, True) ),
                   DataRecord.MonitoredCan(433.9, 1, -54, DataRecord.Variation(600, 700, True) ) ]

print(TITLE)
print()


def generate_records_down_to_ground(ground_altitude, number_of_records):
    while number_of_records > 0:
        generate_records(5)
        if record.altitude < ground_altitude:
            record.verticalVelocityVariation = None
            record.longitudeVariation = None
            record.latitudeVariation = None
            record.altitudeVariation = None
            record.thermistorVariation = None
            record.bmpTempVariation = None
        number_of_records -= 5


def generate_records(number):
    global totalNumberOfRecords
    global outputFile
    for _ in repeat(None, number):
        record.modify_record()
        if ADD_NOISE:
            outputFile.write(record.noisy_version().csv_line()+'\n')
        else:
            outputFile.write(record.csv_line() + '\n')
    totalNumberOfRecords += number


if len(sys.argv) != 1:
    print(MYSELF, ': Missing or extraneous argument (Aborted)')
    print(USAGE)
    sys.exit(1)

if os.path.exists(OUTPUT_FILE_NAME):
    if FORCE_OUTPUT_FILE_OVERWRITE:
        print('Overwriting data file \'' + OUTPUT_FILE_NAME + '\'...')
    else:
        print(MYSELF + ': ERROR: Output file \'' + OUTPUT_FILE_NAME + '\' exists (Aborted)')
        exit(3)
else:
    print('Generating data file \'' + OUTPUT_FILE_NAME + '\'...')
outputFile = open(OUTPUT_FILE_NAME, 'w')
record = DataRecord.EtheraRecord(START_TS, BEACONS, MONITORED_CANS)
record.altitude = START_ALTITUDE
record.pressure = START_PRESSURE
record.refAltitude = START_ALTITUDE
record.bmpTemperature = START_TEMP + 5.1
record.therm1_Temperature = START_TEMP

# 1. Generate records before launch
print('  Can startup...')
# 1.1 Without GPS data
for i in range(1, 3 * 60 * 5):
    record.modify_record()
    outputFile.write(record.noisy_version().csv_line() + '\n')
    totalNumberOfRecords += 1
generate_records(3 * 60 * 5 - 8)

# 1.2 With stable GPS data
print('  From startup to hand-over...')
record.gpsAltitude = START_ALTITUDE
record.gpsLatitude = START_LATITUDE
record.gpsLongitude = START_LONGITUDE
generate_records(DURATION_BEFORE_HANDOVER_IN_HOURS * 60 * 60 * 5)

# 1.3 With varying GPS data (transport to launch site)
print('  Transport to launch site...')
record.latitudeVariation = DataRecord.Variation(300/M_PER_DEGREE_LATITUDE, 40, False)
record.longitudeVariation = DataRecord.Variation(400/M_PER_DEGREE_LONGITUDE, 50, False)
generate_records(300)

# 2. Take-off and ascent
print('  Take-off and ascent...')
#    Ascent on 120 record
#    Accelerate on 10 records, ascent on 90 more, decelerate on 20 last.
record.latitudeVariation = DataRecord.Variation(500/M_PER_DEGREE_LATITUDE, 40, repeat=True)
record.longitudeVariation = DataRecord.Variation(500/M_PER_DEGREE_LONGITUDE, 50, repeat=True)
record.altitudeVariation = DataRecord.Variation(995, 120, repeat=False)
record.verticalVelocityVariation = DataRecord.Variation(-15, 10, repeat=False)
generate_records(50)

record.countSinceNewGPS = 2
record.latitudeVariation = DataRecord.Variation(500/M_PER_DEGREE_LATITUDE, 40, repeat=True)
record.longitudeVariation = DataRecord.Variation(500/M_PER_DEGREE_LONGITUDE, 50, repeat=True)
generate_records(50)

record.verticalVelocityVariation = DataRecord.Variation(15, 20, False)
generate_records(20)

# We are at the apex. Assume ejection from rocket
# 3. Descent: acceleration to 10 m/s in 3 sec = 15 rec then stable velocity
#             variation of lat/long until the end
#             temperature drops by 15°C
#             After 4 seconds = 20 records start modulating descent velocity with airbrake.
print("  Descent...")
record.thermistorVariation = DataRecord.Variation(-15, 20, False)
record.bmpTempVariation = DataRecord.Variation(-15, 350, False)
record.verticalVelocityVariation = DataRecord.Variation(10, 15, False)
generate_records(20)
print("  Assuming we're out of the rocket")
print("  Starting air-brakes...")

# Air-brakes: the can slows down, variations in lat/long
#           temperature drops by 15°C.
record.verticalVelocityVariation = DataRecord.Variation(-2, 15, False)
record.altitudeVariation = DataRecord.Variation(-1600, 1000, False)  # = 8 m/s
generate_records(25)

record.thermistorVariation = DataRecord.Variation(15, 500, False)

print('  Descent  down to 95 m...')
# When altitude reaches 95 m cans stop
generate_records_down_to_ground(95, DURATION_BEFORE_RETRIEVAL_IN_HOURS * 60 * 60 * 5)

outputFile.close()
print(MYSELF + ': Generated', totalNumberOfRecords, 'records. End of job.')
exit(0)
