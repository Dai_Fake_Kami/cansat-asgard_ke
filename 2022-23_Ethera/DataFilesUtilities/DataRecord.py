import copy
import math
import random
from math import log10, pow, sqrt, floor

# ref https://www.nhc.noaa.gov/gccalc.shtml
M_PER_DEGREE_LONGITUDE = 70000
M_PER_DEGREE_LATITUDE = 111000

class Variation:
    def __init__(self, max_delta, num_steps, repeat=False):
        """Initialise the requested variation
        Parameters
        ----------
        max_delta: integer or float, positive or negative
            The maximum variation to be applied.
        num_steps: integer
            The number of steps to use to vary from 0 to max_delta
        repeat: boolean
            If true, the variation is applied repetitively, alternately increasing and decreasing the value.
            If false, the variation stops after the max_delta is reached.
        """
        assert max_delta != 0
        assert num_steps > 0
        self.repeat = repeat
        self.current = 0
        self.maxDelta = max_delta
        self.increment = math.fabs(max_delta / num_steps)
        self.increasing = (max_delta > 0)
        self.done = False

    def __str__(self):
        return "max_delta={}, increment={}, increasing={}, repeat={}, done={}".format(self.maxDelta,
                                                                           self.increment,
                                                                           self.increasing,
                                                                           self.repeat,
                                                                           self.done)

    def get_increment(self):
        """Obtain the variation to apply in the current record"""
        if self.done:
            return 0

        if self.increasing:
            self.current += self.increment
            if ((self.maxDelta > 0) and (self.current > self.maxDelta)) or ((self.maxDelta < 0) and (self.current > 0)):
                if self.repeat:
                    self.current -= 2 * self.increment
                    self.increasing = False
                else:
                    self.done = True
        else:
            self.current -= self.increment
            if ((self.maxDelta < 0) and (self.current < self.maxDelta)) or ((self.maxDelta > 0) and (self.current < 0)):
                if self.repeat:
                    self.current += 2 * self.increment
                    self.increasing = True
                else:
                    self.done = True
        if self.done:
            return 0
        else:
            if self.increasing:
                return self.increment
            else:
                return -self.increment


class DataRecord:
    myRandom = random.Random()
    ACQUISITION_PERIOD = 200  # msec

    def __init__(self, start_ts):
        self.timestamp = start_ts
        self.newGPS_Data = 0
        self.gpsLatitude = 0.0
        self.gpsLongitude = 0.0
        self.gpsAltitude = 0.0
        self.pressure = 0.0
        self.altitude = 0.0
        self.refAltitude = 0.0
        self.verticalVelocity = 0.0
        self.bmpTemperature = 0.0
        self.therm1_Temperature = 0.0
        self.therm2_Temperature = 0.0

        self.countSinceNewGPS = 0
        self.latitudeVariation = None
        self.longitudeVariation = None
        self.altitudeVariation = None
        self.verticalVelocityVariation = None
        self.thermistorVariation = None
        self.bmpTempVariation = None

    def csv_line(self):
        return str(self.timestamp) + ',' \
            + str(self.newGPS_Data) + ',' \
            + '{:.5f},{:.5f},{:.1f},'.format(self.gpsLatitude, self.gpsLongitude, self.gpsAltitude) \
            + '{:.1f},{:.1f},{:.1f},{:.1f},'.format(self.bmpTemperature, self.pressure, self.altitude,
                                                       self.refAltitude) \
            + '{:.2f},{:.1f},'.format(self.verticalVelocity, self.therm1_Temperature)

    def noisy_version(self):
        noisy = copy.deepcopy(self)
        noisy.add_noise()
        return noisy

    def add_noise(self):
        if self.newGPS_Data == 1:
            if self.gpsLatitude != 0:
                self.gpsLatitude += self.myRandom.normalvariate(0, 0.00005)
            if self.gpsLongitude != 0:
                self.gpsLongitude += self.myRandom.normalvariate(0, 0.00005)
            if self.gpsAltitude != 0:
                self.gpsAltitude += self.myRandom.normalvariate(0, 0.6)
        self.bmpTemperature += self.myRandom.normalvariate(0, 0.1)
        self.pressure += self.myRandom.normalvariate(0, 0.5)
        self.altitude += self.myRandom.normalvariate(0, 0.5)
        self.verticalVelocity += self.myRandom.normalvariate(0, 0.3)
        self.therm1_Temperature += self.myRandom.normalvariate(0, 0.3)
        self.therm2_Temperature += self.myRandom.normalvariate(0, 0.4)

    def modify_record(self):
        """Modify the record according to the rules defined with the Variation objects."""
        self.timestamp += DataRecord.ACQUISITION_PERIOD
        self.countSinceNewGPS = (self.countSinceNewGPS + 1) % 3
        if self.countSinceNewGPS == 0 and self.gpsLatitude != 0:
            self.newGPS_Data = 1
            if self.latitudeVariation is not None:
                self.gpsLatitude += self.latitudeVariation.get_increment()
            if self.longitudeVariation is not None:
                self.gpsLongitude += self.longitudeVariation.get_increment()
                if self.gpsLongitude > 180:
                    print("ERROR: longitude={}".format(self.gpsLongitude))
                    exit(3)
        else:
            self.newGPS_Data = 0
        if self.altitudeVariation is not None:
            altitude_increment = self.altitudeVariation.get_increment()
            self.gpsAltitude += altitude_increment
            self.altitude += altitude_increment
            self.pressure -= altitude_increment / 10.0
        if self.verticalVelocityVariation is not None:
            self.verticalVelocity += self.verticalVelocityVariation.get_increment()
        if self.thermistorVariation is not None:
            temp_increment = self.thermistorVariation.get_increment()
            self.therm1_Temperature += temp_increment

        if self.bmpTempVariation is not None:
            self.bmpTemperature += self.bmpTempVariation.get_increment()

class Beacon:
    def __init__(self, frequency, latitude, longitude, rssi0, x0):
        self.frequency= frequency
        self.latitude = latitude
        self.longitude = longitude
        self.rssi0 = rssi0
        self.x0 = x0
        self.k0 = pow(10, (self.rssi0 + 20*log10(self.x0))/20)

    def distance_from_rssi(self, rssi):
        return floor(self.k0/pow(10, rssi/20))

    def get_distance_and_rssi(self, latitude, longitude):
        delta_latitude = latitude - self.latitude
        delta_longitude = longitude - self.longitude
        delta_x = delta_longitude*M_PER_DEGREE_LONGITUDE
        delta_y = delta_latitude * M_PER_DEGREE_LATITUDE
        distance = sqrt(delta_x*delta_x + delta_y*delta_y)
        #print(f"beacon lat={self.latitude}, long={self.longitude}")
        #print(f"delta lat={delta_latitude}, delta long={delta_longitude}")
        #print(f"delta x={delta_x}, y={delta_y}")
        #print(f"received lat={latitude}, long={longitude}, x0={self.x0}, dist={distance}")
        rssi = self.rssi0 + 20*log10(self.x0) - 20*log10(distance)
        rssi = math.floor(rssi)
        return self.distance_from_rssi(rssi), rssi


class MonitoredCan:
    def __init__(self, frequency, distance, rssi, distance_variation):
        self.frequency = frequency
        self.distance = distance
        self.rssi = rssi
        self.distanceVariation = distance_variation
class EtheraRecord(DataRecord):
    def __init__(self, start_ts, beacons: [], monitored_cans:[]):
        super().__init__(start_ts)
        self.brakePosition = 0
        self.flightPhase = 0
        self.frequency = 0.0
        self.rssi = 0
        self.distance = 0.0
        self.flightControlCode = 0
        self.beacons = beacons
        self.monitored_cans = monitored_cans
        self.currentBeaconOrCan = 0


    def modify_record(self):
        super().modify_record()
        num_beacons = len(self.beacons)
        num_cans = len(self.monitored_cans)
        for b in self.monitored_cans:
            if b.distanceVariation is not None:
                b.distance += b.distanceVariation.get_increment()

        #print(f"num_cans={num_cans}, num_beacons={num_beacons}, self.currentBeaconOrCan={self.currentBeaconOrCan}")
        self.frequency = 0
        self.distance = 0
        self.rssi = 0
        # No reception in half the records
        if random.randint(1, 2) != 1:
            if self.currentBeaconOrCan < num_beacons:
                if self.gpsLatitude != 0 and self.gpsLongitude != 0:
                    self.frequency = self.beacons[self.currentBeaconOrCan].frequency
                    self.distance, self.rssi = self.beacons[self.currentBeaconOrCan].\
                        get_distance_and_rssi(self.gpsLatitude, self.gpsLongitude)
            elif self.currentBeaconOrCan < ( num_beacons + num_cans):
                can_idx = self.currentBeaconOrCan - num_beacons
                #print(f"processing can {can_idx}")
                self.frequency = self.monitored_cans[can_idx].frequency
                self.distance = self.monitored_cans[can_idx].distance
                self.rssi = self.monitored_cans[can_idx].rssi

        self.currentBeaconOrCan += 1
        if self.currentBeaconOrCan >= ( num_beacons + num_cans):
            self.currentBeaconOrCan = 0

    def csv_line(self):
        return super().csv_line()  \
            + str(self.flightControlCode) + ',' \
            + str(self.brakePosition)+ ','  \
            + str(self.flightPhase)+ ',' \
            + '{:.1f},{:.1f},'.format(self.frequency, floor(self.rssi)) \
            + str(floor(self.distance))