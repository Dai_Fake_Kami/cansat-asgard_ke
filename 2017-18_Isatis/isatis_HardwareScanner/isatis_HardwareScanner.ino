/*
 * Isatis_HardwareScanner
 * 
 * This utility just creates an IsatisHardwareScanner and prints diagnostic on the Serial output.
 * Only information that can be automatically discovered is shown. 
 * 
 */

#include "IsatisHW_Scanner.h" // Include this first to have libraries linked in the right order. 
#include "IsatisConfig.h" 

 void setup() {
  Serial.begin(serialBaudRate);
  while (!Serial) ;
  
  IsatisHW_Scanner hw;
  hw.IsatisInit();
  hw.printFullDiagnostic(Serial);
}

void loop() {
  // put your main code here, to run repeatedly:

}
